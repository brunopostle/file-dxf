#!/usr/bin/perl

#Editor vim:syn=perl

use Test::More 'no_plan';
use strict;
use warnings;

use lib ('lib', '../lib');
use File::DXF::Csv qw(array2csv);

my $csv;
ok ($csv = File::DXF::Csv->new);

ok ($csv->Push (1, 'foo bar', ',', '"','" "', 8, 13, 21, 34, 55));

ok ($csv->Write ('_test.csv'));

unlink '_test.csv';

1;
