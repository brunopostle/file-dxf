#!/usr/bin/perl

#Editor vim:syn=perl

use Test::More 'no_plan';
use strict;
use warnings;

use lib ('lib', '../lib');
use File::DXF;
use File::DXF::Util qw /read_DXF/;

my $dxf = File::DXF->new;

my $data;
ok ($data = read_DXF ('t/data/circle-r12.dxf'), 'read r12 dxf');
ok ($dxf->Process ($data), 'process');
my @report = $dxf->{ENTITIES}->Report (('CIRCLE'));
is (ref $report[0], 'File::DXF::ENTITIES::CIRCLE', 'find circle inside');

like ($report[0]->Coordinates->[0], '/475\.0239/', 'Coordinates()');
like ($report[0]->Coordinates->[1], '/504\.6427/', 'Coordinates()');
is ($report[0]->Coordinates->[2] +0, 0, 'Coordinates()');
like ($report[0]->Radius, '/64.262/', 'Radius()');
like ($report[0]->Diameter, '/128\.5/', 'Diameter()');
like ($report[0]->Area, '/12973\.8/', 'Area()');
like ($report[0]->Circumference, '/403\.7/', 'Circumference()');

ok ($data = read_DXF ('t/data/circle-2004.dxf'), 'read 2004 dxf');
ok ($dxf->Process ($data), 'process');
@report = $dxf->{ENTITIES}->Report (('CIRCLE'));
is (ref $report[0], 'File::DXF::ENTITIES::CIRCLE', 'find circle inside');

ok ($data = read_DXF ('t/data/circle-2000.dxf'), 'read 2000 dxf');
ok ($dxf->Process ($data), 'process');
@report = $dxf->{ENTITIES}->Report (('CIRCLE'));
is (ref $report[0], 'File::DXF::ENTITIES::CIRCLE', 'find circle inside');

