#!/usr/bin/perl
#Editor vim:syn=perl

use lib 'lib';
use strict;
use warnings;
use 5.010;
use Data::Dumper;
use Test::More 'no_plan';

use File::DXF::Math;
use Math::MatrixReal;

# https://stackoverflow.com/questions/49769459/convert-points-on-a-3d-plane-to-2d-coordinates
my $matrix = File::DXF::Math::matrix_3d_to_2d([1,0,0], [1,1,0], [2,5,1]);

my $P = Math::MatrixReal->new_from_cols([[1,0.5,0,1]]);
$P = Math::MatrixReal->new_from_cols([[2,5,1,1]]);

ok(1);
say Dumper $matrix * $P;

1;
