#!/usr/bin/perl

#Editor vim:syn=perl

use Test::More 'no_plan';
use strict;
use warnings;

use lib ('lib', '../lib');
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::ENTITIES;

my $dxf = File::DXF->new;

my $data;
ok ($data = read_DXF ('t/data/mesh-r12.dxf'), 'read mesh-r12.dxf');
ok ($dxf->Process ($data), 'process');

my @report = $dxf->{ENTITIES}->Report (('POLYLINE','VERTEX', 'SEQEND'));
is (ref $report[0], 'File::DXF::ENTITIES::POLYLINE', 'find polyline inside');

is ($report[0]->Size_M, 3, 'Size_M');
is ($report[0]->Size_N, 3, 'Size_N');
ok ($report[0]->Rectangular, 'Rectangular()');
ok (!$report[0]->Triangular, 'Triangular()');
ok (!$report[0]->Closed_M, 'Closed_M()');
ok (!$report[0]->Closed_N, 'Closed_N()');
is ($report[0]->Nodes, 9, 'Nodes()');
is_deeply ([$report[0]->Index (0)], [0,0], 'Index()');
is ($report[0]->Triangles, 0, 'Triangles()');

is (ref $report[1], 'File::DXF::ENTITIES::VERTEX', 'find vertex inside');
is (ref $report[10], 'File::DXF::ENTITIES::SEQEND', 'find seqend inside');

ok ($data = read_DXF ('t/data/mesh-2004.dxf'), 'read mesh-2004.dxf');
ok ($dxf->Process ($data), 'process');

@report = $dxf->{ENTITIES}->Report (('POLYLINE','VERTEX', 'SEQEND'));
is (ref $report[0], 'File::DXF::ENTITIES::POLYLINE', 'find polyline inside');
is (ref $report[1], 'File::DXF::ENTITIES::VERTEX', 'find vertex inside');
is (ref $report[10], 'File::DXF::ENTITIES::SEQEND', 'find seqend inside');


ok ($data = read_DXF ('t/data/mesh-2000.dxf'), 'read mesh-2000.dxf');
ok ($dxf->Process ($data), 'process');

@report = $dxf->{ENTITIES}->Report (('POLYLINE','VERTEX', 'SEQEND'));
is (ref $report[0], 'File::DXF::ENTITIES::POLYLINE', 'find polyline inside');
is (ref $report[1], 'File::DXF::ENTITIES::VERTEX', 'find vertex inside');
is (ref $report[10], 'File::DXF::ENTITIES::SEQEND', 'find seqend inside');

ok (!$dxf->{ENTITIES}->Extract_Meshes, 'Extract_Meshes');

ok ($data = read_DXF ('t/data/unroll.dxf'), 'read unroll.dxf');
ok ($dxf->Process ($data), 'process');

ok (my @meshes = $dxf->{ENTITIES}->Extract_Meshes, 'Extract_Meshes()');

ok (scalar (@meshes), 'Extract_Meshes()');

ok ($data = read_DXF ('t/data/unroll.dxf'), 'read unroll.dxf');
ok ($dxf->Process ($data), 'process');

@report = $dxf->{ENTITIES}->Report (('POLYLINE','VERTEX', 'SEQEND'));
@meshes = File::DXF::ENTITIES::polyface_to_meshes (@report);

is (scalar (@meshes), 1, 'polyface_to_meshes');
is (scalar (@{$meshes[0]}), 3, 'polyface_to_meshes');
is ($meshes[0]->[2]->{layer}, 'Meshes', 'layer');
is ($meshes[0]->[2]->{colour}, 10, 'colour');

