#!/usr/bin/perl

#Editor vim:syn=perl

use Test::More 'no_plan';
use strict;
use warnings;

use lib ('lib', '../lib');
use File::DXF;
use File::DXF::Util qw /read_DXF/;

my $dxf = File::DXF->new;

my $data;
ok ($data = read_DXF ('t/data/text-r12.dxf'), 'read text-r12.dxf');
ok ($dxf->Process ($data), 'process');
my @report = $dxf->{ENTITIES}->Report (('TEXT'));
is (ref $report[0], 'File::DXF::ENTITIES::TEXT', 'find text inside');

is ($report[0]->Content, 'I wandered lonely', 'Content()');
is ($report[0]->Height +0, 2.5, 'Height()');
is ($report[0]->Rotation +0, 0, 'Rotation()');
is ($report[0]->Type, 'TEXT', 'Type()');

ok ($data = read_DXF ('t/data/text-2004.dxf'), 'read text-2004.dxf');
ok ($dxf->Process ($data), 'process');
@report = $dxf->{ENTITIES}->Report (('TEXT'));
is (ref $report[0], 'File::DXF::ENTITIES::TEXT', 'find text inside');

is ($report[0]->Content, 'I wandered lonely', 'Content()');
is ($report[0]->Height +0, 2.5, 'Height()');
is ($report[0]->Rotation +0, 0, 'Rotation()');
is ($report[0]->Type, 'TEXT', 'Type()');

ok ($data = read_DXF ('t/data/text-2000.dxf'), 'read text-2000.dxf');
ok ($dxf->Process ($data), 'process');
@report = $dxf->{ENTITIES}->Report (('TEXT'));
is (ref $report[0], 'File::DXF::ENTITIES::TEXT', 'find text inside');

is ($report[0]->Content, 'I wandered lonely', 'Content()');
is ($report[0]->Height +0, 2.5, 'Height()');
is ($report[0]->Rotation +0, 0, 'Rotation()');
is ($report[0]->Type, 'TEXT', 'Type()');

