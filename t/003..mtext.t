#!/usr/bin/perl

#Editor vim:syn=perl

use Test::More 'no_plan';
use strict;
use warnings;

use lib ('lib', '../lib');
use File::DXF;
use File::DXF::Util qw /read_DXF/;

my $dxf = File::DXF->new;

my $data;
ok ($data = read_DXF ('t/data/mtext-r12.dxf'), 'read text-r12.dxf');
ok ($dxf->Process ($data), 'process');
my @report = $dxf->{ENTITIES}->Report (('INSERT'));
TODO: { local $TODO = 'not implemented';
is (ref $report[0], 'File::DXF::ENTITIES::INSERT', 'find insert inside');
};

ok ($data = read_DXF ('t/data/mtext-2004.dxf'), 'read text-2004.dxf');
ok ($dxf->Process ($data), 'process');
@report = $dxf->{ENTITIES}->Report (('MTEXT'));
TODO: { local $TODO = 'not implemented';
is (ref $report[0], 'File::DXF::ENTITIES::MTEXT', 'find mtext inside');
};

ok ($data = read_DXF ('t/data/mtext-2000.dxf'), 'read text-2000.dxf');
ok ($dxf->Process ($data), 'process');
@report = $dxf->{ENTITIES}->Report (('MTEXT'));
TODO: { local $TODO = 'not implemented';
is (ref $report[0], 'File::DXF::ENTITIES::MTEXT', 'find mtext inside');
};

