#!/usr/bin/perl

#Editor vim:syn=perl

use Test::More 'no_plan';
use strict;
use warnings;

use lib ('lib', '../lib');
use File::DXF::Math qw/rotate_3d average_3d add_3d scale_3d distance_3d area_3d cog_3d green_theorem normalise_3d distance_3d normalise_3d x_product_3d dot_product_3d magnitude_3d subtract_3d triangle_normal line_plane_intersection angle_vectors_3d points_to_plane plane_distance/;

is_deeply (average_3d ([0,0,0], [2,0,0], [1,3,0]), [1,1,0]);
is_deeply (average_3d ([0,0,0], [2,0,0], [1,3,3]), [1,1,1]);
is_deeply (average_3d ([0,0,0], [-2,0,0], [-1,3,0]), [-1,1,0]);
is_deeply (average_3d ([0,0,0], [2,0,0]), [1,0,0]);
is_deeply (average_3d ([0,0,0], [2,0,0], [1,3,0], [5,5,0]), [2,2,0]);

is_deeply (add_3d ([1,2,3], [4,5,6]), [5,7,9]);
is_deeply (add_3d ([1,2,3], [4,5,6], [7,8,9]), [12,15,18]);
is_deeply (add_3d ([-1,-2,-3], [4,5,6]), [3,3,3]);
is_deeply (add_3d ([4,5,6]), [4,5,6]);

is_deeply (scale_3d ([1,2,3], 2), [2,4,6]);
is_deeply (scale_3d (2, [1,2,3]), [2,4,6]);
is_deeply (scale_3d ([-1,-2,-3], 2), [-2,-4,-6]);
is_deeply (scale_3d (-2, [1,2,3]), [-2,-4,-6]);

is (distance_3d ([0,0,0], [3,4,0]), 5);
is (distance_3d ([1,2,3], [4,6,3]), 5);
is (distance_3d ([-1,-2,-3], [2,2,-3]), 5);
is (distance_3d ([0,0,0], [0,4,3]), 5);
is (distance_3d ([0,0,0], [0,0,0]), 0);

is (area_3d ([0,0,0], [4,0,0], [4,4,0]), 8);
is (area_3d ([0,0,0], [0,4,0], [4,4,0]), 8);
is (area_3d ([0,0,0], [0,4,0], [0,4,4]), 8);
is (area_3d ([0,0,0], [4,4,0], [4,0,0]), 8);

is (green_theorem ([0,0,0], [4,0,0], [4,4,0]), 8);
is (green_theorem ([0,0,0], [0,4,0], [4,4,0]), -8);
is (green_theorem ([0,0,0], [0,4,0], [0,4,4]), 0);
is (green_theorem ([0,0,0], [4,4,0], [4,0,0]), -8);
is (green_theorem ([0,0,0], [-4,0,0], [-4,-4,0]), 8);
is (green_theorem ([-1,-2,-3], [3,-2,-3], [3,2,-3]), 8);

is_deeply (cog_3d ([0,0,1], [2,0,1], [1,3,1]), [1,1,0.5]);
is_deeply (cog_3d ([0,0,0], [2,0,0], [1,3,0]), [1,1,0]);
is_deeply (cog_3d ([0,0,0], [2,0,0], [0,2,2]), [0.5,1.0,0.5]);

is_deeply (normalise_3d ([3,4,0]), [3/5,4/5,0]);
is_deeply (normalise_3d ([0,8,6]), [0,4/5,3/5]);
is_deeply (normalise_3d ([0,-8,6]), [0,-4/5,3/5]);

is (magnitude_3d ([3,4,0]), 5);
is (magnitude_3d ([-3,4,0]), 5);
is (magnitude_3d ([-30,0,40]), 50);

is_deeply (subtract_3d ([4,5,6], [1,2,3]), [3,3,3]);

is (dot_product_3d ([1,3,-5], [4,-2,-1]), 3);

is_deeply (triangle_normal ([0,0,0], [1,0,0], [1,1,0]), [0,0,1]);
is_deeply (triangle_normal ([0,0,3], [1,0,3], [1,4,0]), [0,0.6,0.8]);

is (line_plane_intersection ([0,0,0], [2,0,0], [1,0,0], [1,1,0], [1,1,1]), 0.5);
is (line_plane_intersection ([-2,0,0], [1,0,0],   [1,0,0], [1,1,0], [1,1,1]), 1);
is (line_plane_intersection ([-200,0,0], [1,0,0],   [1,0,0], [1,1,0], [1,1,1]), 1);
is (line_plane_intersection ([1,0,0], [2,0,0],   [1,0,0], [1,1,0], [1,1,1]), 0);
is (line_plane_intersection ([2,0,0], [3,0,0],   [1,0,0], [1,1,0], [1,1,1]), -1);
is (line_plane_intersection ([0,2,0], [0,2,4],   [0,0,0], [1,0,0], [5,2,2]), 0.5);
is (line_plane_intersection ([0,2,0], [0,2,4],   [0,0,0], [1,0,0], [5,8,8]), 0.5);
is (line_plane_intersection ([0,2,0], [0,2,8],   [0,0,0], [1,0,0], [5,8,8]), 0.25);

rotate_3d (0.1, [1,0.01,0]);
rotate_3d (0.1, [0.01,1,0]);
rotate_3d (0.1, [-1,0.01,0]);
rotate_3d (0.1, [0.01,-1,0]);
rotate_3d (0.1, [1,0,0]);
rotate_3d (0.1, [0,1,0]);
rotate_3d (0.1, [-1,0,0]);
rotate_3d (0.1, [0,-1,0]);

is_deeply(points_to_plane([1,2,-2], [3,-2,1], [5,1,-4]),[11,16,14,-15]);

my $plane = points_to_plane([0,0,0],[0,1,0],[1,0,1]);

like(plane_distance($plane, [1,0,0]), '/^0\.707/');
like(plane_distance($plane, [0,0,1]), '/^-0\.707/');

like (angle_vectors_3d([1,0,0], [0,1,0]), '/^1.57/');
like (angle_vectors_3d([1,0,0], [-1,0,0]), '/^3.14/');

0;
