#!/usr/bin/perl

#Editor vim:syn=perl

use Test::More 'no_plan';
use strict;
use warnings;

use lib ('lib', '../lib');
use_ok ('File::DXF');
use File::DXF::Util qw /read_DXF/;

use_ok ('File::DXF::BLOCKS');
use_ok ('File::DXF::CLASSES');
use_ok ('File::DXF::ENTITIES');
use_ok ('File::DXF::HEADER');
use_ok ('File::DXF::OBJECTS');
use_ok ('File::DXF::TABLES');

ok (my $dxf = File::DXF->new, 'instantiate');

ok (my $data = read_DXF ('t/data/lines.dxf'), 'read lines.dxf');

ok ($dxf->Process ($data), 'process');

my @report = $dxf->{ENTITIES}->Report (('LINE'));
is (ref $report[0], 'File::DXF::ENTITIES::LINE', 'find line inside');
is ($report[0]->Type, 'LINE', 'Type()');
ok ($report[0]->Coordinates (0)->[0] ne $report[0]->Coordinates (0)->[1]);
ok ($report[0]->Coordinates (1)->[0] ne $report[0]->Coordinates (1)->[1]);
ok ($report[0]->Length > 0);

ok (my $text = $dxf->DXF, 'DXF()');

ok ($text =~ / *0\nSECTION\n *2\nENTITIES\n/, 'ENTITIES');
ok ($text =~ / *0\nSECTION\n *2\nHEADER\n/, 'HEADER');
ok ($text =~ / *0\nLINE\n/, 'LINE');

ok ($data = read_DXF ('t/data/3dface.dxf'), 'read 3dface.dxf');
ok ($dxf->Process ($data), 'Process');

@report = $dxf->{ENTITIES}->Report (('LINE'));

is (scalar @report, 0);

@report = $dxf->{ENTITIES}->Report (('3DFACE'));

is (ref $report[0], 'File::DXF::ENTITIES::3DFACE', 'find line inside');
is ($report[0]->Type, '3DFACE', 'Type()');
ok ($report[0]->Coordinates (0)->[0] ne $report[0]->Coordinates (0)->[1]);
ok ($report[0]->Coordinates (1)->[0] ne $report[0]->Coordinates (1)->[1]);

is ($report[0]->Colour, 1);
is ($report[1]->Colour, 3);
is ($report[2]->Colour, 5);
is ($report[3]->Colour, 0);

is ($report[0]->Packed, '        -51.566367:        -40.491034:          0.000000');

ok ($dxf->{BLOCKS} = File::DXF::BLOCKS->new);
ok ($dxf->{CLASSES} = File::DXF::CLASSES->new);
ok ($dxf->{ENTITIES} = File::DXF::ENTITIES->new);
ok ($dxf->{HEADER} = File::DXF::HEADER->new);
ok ($dxf->{OBJECTS} = File::DXF::OBJECTS->new);
ok ($dxf->{TABLES} = File::DXF::TABLES->new);

ok ($dxf->{BLOCKS}->Add_From_Disk ('t/data/point-2000.dxf', 't/data/torii.dxf'));

ok ($text = $dxf->DXF, 'DXF()');

1;
