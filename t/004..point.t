#!/usr/bin/perl

#Editor vim:syn=perl

use Test::More 'no_plan';
use strict;
use warnings;

use lib ('lib', '../lib');
use File::DXF;
use File::DXF::Util qw /read_DXF/;

use File::DXF::ENTITIES::LINE;
use File::DXF::ENTITIES::3DFACE;
use File::DXF::Csv;
use File::DXF::Math::Matrix;

my $dxf = File::DXF->new;

my $data;
ok ($data = read_DXF ('t/data/point-r12.dxf'), 'read r12 dxf');
ok ($dxf->Process ($data), 'process');
my @report = $dxf->{ENTITIES}->Report (('POINT'));
is (ref $report[0], 'File::DXF::ENTITIES::POINT', 'find point inside');

ok ($data = read_DXF ('t/data/point-2004.dxf'), 'read 2004 dxf');
ok ($dxf->Process ($data), 'process');
@report = $dxf->{ENTITIES}->Report (('POINT'));
is (ref $report[0], 'File::DXF::ENTITIES::POINT', 'find point inside');

ok ($data = read_DXF ('t/data/point-2000.dxf'), 'read 2000 dxf');
ok ($dxf->Process ($data), 'process');
@report = $dxf->{ENTITIES}->Report (('POINT'));
is (ref $report[0], 'File::DXF::ENTITIES::POINT', 'find point inside');

