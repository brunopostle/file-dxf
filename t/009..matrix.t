#!/usr/bin/perl

#Editor vim:syn=perl

use Test::More 'no_plan';
use strict;
use warnings;

use lib ('lib', '../lib');
use File::DXF::Math::Matrix qw(transpose multiply vekpro);

my $point  = [[4], [5], [6]];

  my $matrix = [[ 1, 0, 1 ],
                [ 0, 2, 0 ],
                [ 0, 0, 3 ]];


ok (my $matrix_transposed = transpose ($matrix));

ok (my $matrix_multiplied = multiply ($matrix, $point));

ok (my $scalar = vekpro ($matrix, $matrix_transposed));

1;
