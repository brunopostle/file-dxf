#!/usr/bin/perl

#Editor vim:syn=perl

use Test::More 'no_plan';
use strict;
use warnings;

use lib ('lib', '../lib');
use File::DXF::Mem;

my $mem = File::DXF::Mem->new;

$mem->{data}->[0]->[0] = [1000,1000,0,1];
$mem->{data}->[0]->[3] = [1000,4000,0,1];
$mem->{data}->[2]->[0] = [3000,1000,0,1];
$mem->{data}->[2]->[3] = [3000,4000,0,1];

$mem->Fixup;
#use Data::Dumper; die Dumper $mem->{data};

is ($mem->Mesh_Size_X, 3);
is ($mem->Mesh_Size_Y, 4);

ok ($mem->Write ('_temp.mem'), 'Write()');

ok ($mem->Read ('_temp.mem'), 'Read()');

unlink '_temp.mem';

is (File::DXF::Mem::FIXITY ,3, 'FIXITY');
is (File::DXF::Mem::FREE ,0, 'FREE');
is (File::DXF::Mem::FIXED ,1, 'FIXED');
is (File::DXF::Mem::STRAIGHT ,3, 'STRAIGHT');
