#!/usr/bin/perl

use strict;
use warnings;
use File::DXF::Util qw /dxf/;

# takes a CSV file and outputs a 3D DXF
# typical CSV format: X,Y,Z.RADIUS
# Usage: csv2points.pl < IN.CSV > OUT.DXF

binmode STDOUT, ":crlf";

print dxf (0, 'SECTION');
print dxf (2, 'ENTITIES');

my $id = 1;

for my $line (<STDIN>)
{
    next unless $line =~ /[0-9]/;
    $line =~ s/[\r\n\l]+//;
    $line =~ s/[[:space:]]*,[[:space:]]*/,/g;

    my @node = split ',', $line;
    next unless scalar @node == 4;

    print dxf (0, 'CIRCLE');   # Entity type
    print dxf (5, $id);       # Every entity has a unique ID
    print dxf (8, 'circles');  # Layer name
    print dxf (10, $node[0]); # X coordinate
    print dxf (20, $node[1]); # Y coordinate
    print dxf (30, $node[2]); # Z coordinate
    print dxf (40, $node[3]); # Radius

    $id++;
}

print dxf (0, 'ENDSEC');
print dxf (0, 'EOF');

0;
