#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /dxf read_DXF/;
use File::DXF;
use File::DXF::HEADER;
use File::DXF::ENTITIES;
use Carp;
use 5.010;

croak "usage: N=48 $0 input.dxf output.dxf" unless @ARGV == 2;
# takes multiple triangular meshes strips and splits into descrete meshes each
# of which contain paired triangles.
# Set $ENV{N} to increase the number of quads to rip.

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf_in = File::DXF->new;
   $dxf_in->Process ($data);

my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

for my $mesh ($dxf_in->{ENTITIES}->Extract_Meshes)
{
    # points: $mesh->[0]
    # polygons: $mesh->[1];
    my $polygons = $mesh->[1];
    my $points = [map {[split / /, $_]} @{$mesh->[0]}];

    while (scalar @{$polygons})
    {
        print scalar @{$polygons} ." triangles before\n";
        my ($snipped, $remainder);
  
        my @triangles;
        my $n = $ENV{N} || 1;
        for (1 .. ($n * 2))
        {
            ($snipped, $remainder) = _snip ($polygons);
            next unless scalar @{$snipped};
            $polygons = $remainder;
            push @triangles, @{$snipped};
        }
        
        my $triangles = [map {[split / /, $_]} @triangles];
        print scalar @{$polygons} .' triangles after' ."\n\n";
        push @{$dxf_out->{ENTITIES}}, _dxf_mesh ($points, $triangles);
    }
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

sub _dxf_mesh
{
    my @entities;
    my ($nodes, $triangles) = @_;
    my @nodes_in_use = map {@{$_}} @{$triangles};
    my %hash = map { $_ => 1 } @nodes_in_use;
    @nodes_in_use = sort keys %hash;

    push @entities, {type => 'POLYLINE', _data => [
    [8, 'mesh'],
    [62, 0],
    [66, 1],
    [10, 0.0],
    [20, 0.0],
    [30, 0.0],
    [70, 64],
    [71, scalar (@nodes_in_use)],
    [72, scalar (@{$triangles})] ]};

    # dump nodes
    for my $node_id (@nodes_in_use)
    {
        my $node = $nodes->[$node_id];
        push @entities, {type => 'VERTEX', _data => [
        [8, 'mesh'],
        [10, $node->[0]],
        [20, $node->[1]],
        [30, $node->[2]],
        [70, 192] ]};
    }

    my $lookup;
    for my $index (1 .. scalar @nodes_in_use)
    {
        $lookup->{$nodes_in_use[$index -1]} = $index;
    }

    for my $triangle (@{$triangles})
    {
        push @entities, {type => 'VERTEX', _data => [
        [8, 'mesh'],
        [62, 0],
        [10, 0.0],
        [20, 0.0],
        [30, 0.0],
        [70, 128],
        [71, $lookup->{$triangle->[0]}],
        [72, $lookup->{$triangle->[1]}],
        [73, $lookup->{$triangle->[2]}] ]};

        # some triangles have four sides
        push @{$entities[-1]->{_data}}, [74, $lookup->{$triangle->[3]}]
            if (scalar @{$triangle} == 4);
    }

    push @entities, {type => 'SEQEND', _data => []};

    return @entities;
}


sub _snip
{
    my $polygons = shift;
    my $lookup;
    for my $polygon (@{$polygons})
    {
        my @nodes = split / /, $polygon;
        for my $node (@nodes)
        {
            $lookup->{$node}++;
        }
    }
    my @singletons;
    for my $key (keys %{$lookup})
    {
        push (@singletons, $key) if $lookup->{$key} == 1;
    }
    # @singletons is list of nodes that are only used in one triangle...
    my @triangles_snip;

    for my $singleton (@singletons)
    {
        my ($triangle_snip) = grep {/(^|[[:space:]])$singleton([[:space:]]|$)/x} @{$polygons};
        if (defined $triangle_snip)
        {
            push @triangles_snip, $triangle_snip;
            $polygons = [grep {!/$triangle_snip/x} @{$polygons}];
        }
    }
    return [@triangles_snip], $polygons;
}

0;

