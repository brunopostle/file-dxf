#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::DXF::ENTITIES;
use Carp;
use 5.010;

# Reads a DXF POLYLINE 'triangle mesh' and generates Stanford triangle PLY
# polygon file format geometry.

croak "usage: $0 input.dxf > output.ply" unless @ARGV == 1;
binmode STDOUT, ":unix";

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

my ($mesh) = $dxf->{ENTITIES}->Extract_Meshes;

my @nodes = @{$mesh->[0]};
my @triangles = @{$mesh->[1]};

say 'ply';
say 'format ascii 1.0';
say 'comment Created by File::DXF';
say 'element vertex ' . scalar @nodes;
say 'property float x';
say 'property float y';
say 'property float z';
say 'element face '. scalar @triangles;
say 'property list uchar uint vertex_indices';
say 'end_header';

say $_ for (@nodes);

for my $face (@triangles)
{
    my @face = split / /, $face;
    say scalar (@face) .' '. $face;
}

0;
