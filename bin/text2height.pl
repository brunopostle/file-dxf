#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;

my $filename = $ARGV[0] || croak 'DXF file not specified';
my $data = read_DXF ($filename, 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my $index = 1;

print STDOUT dxf (0, 'SECTION');
print STDOUT dxf (2, 'ENTITIES');

my @report = $entities->Report (('TEXT'));

for my $item (@report)
{
    next unless $item->Content =~ /^[ 0-9.+-]+$/;

    print STDOUT dxf (0, 'TEXT');
    print STDOUT dxf (5, $index);
    print STDOUT dxf (8, $item->{layer});
    print STDOUT dxf (10, $item->Coordinates->[0]);
    print STDOUT dxf (20, $item->Coordinates->[1]);
    print STDOUT dxf (30, $item->Content +0.0);
    print STDOUT dxf (40, '1.0');
    print STDOUT dxf (62, $item->Colour);
    print STDOUT dxf (1, $item->Content);

    $index++;
}

print STDOUT dxf (0, 'ENDSEC');
print STDOUT dxf (0, 'EOF');

0;
