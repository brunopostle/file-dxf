#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;

# Reads a DXF and shifts XYZ coordinates by given distances.  Note, this
# transform doesn't make much sense for many ENTITIES types

croak "usage: $0 1.0 1.0 0.5 input.dxf output.dxf" unless @ARGV == 5;

my $x_shift = shift;
my $y_shift = shift;
my $z_shift = shift;

my $data = read_DXF ($ARGV[0], 'CP1252');

my ($x, $y, $z) = (0.0,0.0,0.0);
my $data_out = [];
my $ENTITIES = 0;

while (@{$data})
{
    my ($code, $string) = File::DXF::_next ($data);

    $ENTITIES = 1 if ($code == 2 and $string eq 'ENTITIES');

    # XYZ codes are '10, 20, 30', '11, 21, 31' etc...
    unless ($ENTITIES and $code =~ /^[123][0-9]$/)
    {
        push @{$data_out}, [$code, $string];
        next;
    }

    $x = $string + $x_shift if ($code =~ /^1[0-9]$/);
    $y = $string + $y_shift if ($code =~ /^2[0-9]$/);
    $z = $string + $z_shift if ($code =~ /^3[0-9]$/);

    # a Z code, capture the suffix
    if ($code =~ /^3([0-9])$/)
    {
        push @{$data_out}, ["1$1", $x], ["2$1", $y], ["3$1", $z];
    }
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";

for my $item (@{$data_out})
{
    print $DXF dxf (@{$item});
}

close $DXF;

0;

