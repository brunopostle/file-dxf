#!/usr/bin/perl

use strict;
use warnings;
use File::DXF::Util qw /dxf/;

# takes a CSV file representing 3D points and outputs a 3D DXF
# typical CSV format: LABEL,X,Y,Z
# Usage: csv2points.pl < IN.CSV > OUT.DXF

binmode STDOUT, ":crlf";

print dxf (0, 'SECTION');
print dxf (2, 'ENTITIES');

my $id = 1;

for my $line (<STDIN>)
{
    next unless $line =~ /[0-9]/;
    $line =~ s/[\r\n\l]+//;
    $line =~ s/[[:space:]]*,[[:space:]]*/,/g;

    my @node = split ',', $line;
    unshift @node, 'PLACEHOLDER' if scalar @node == 3;
    next unless scalar @node > 3;

    print dxf (0, 'POINT');   # Entity type
    print dxf (5, $id);       # Every entity has a unique ID
    print dxf (8, 'Points');  # Layer name
    print dxf (10, $node[1]); # X coordinate
    print dxf (20, $node[2]); # Y coordinate
    print dxf (30, $node[3]); # Z coordinate

    $id++;

    next if $node[0] eq 'PLACEHOLDER';

    print dxf (0, 'TEXT');    # Entity type
    print dxf (5, $id);       # Every entity has a unique ID
    print dxf (8, 'Labels');  # Layer name
    print dxf (10, $node[1]); # X coordinate
    print dxf (20, $node[2]); # Y coordinate
    print dxf (30, $node[3]); # Z coordinate
    print dxf (40, '1.0');    # Text height
    print dxf (1, $node[0]);  # Text string

    $id++;
}

print dxf (0, 'ENDSEC');
print dxf (0, 'EOF');

0;
