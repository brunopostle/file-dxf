#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw /add_3d scale_3d angle_2d distance_2d points_2line line_intersection/;
use File::DXF::ENTITIES::LINE;
use Math::Trig;
use Carp;
use 5.010;

croak "usage: $0 UNCUT.DXF CUT.DXF" unless @ARGV == 2;

my $dxf = File::DXF->new;
   $dxf->Process(read_DXF($ARGV[0], 'CP1252'));
my @LINES = $dxf->{ENTITIES}->Report('LINE');
my @CIRCLES = $dxf->{ENTITIES}->Report('CIRCLE');

my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;
open my $CSV, '>', $ARGV[1] .'.csv';
say $CSV join ', ', 'CIRCLE_ID', 'DIAMETER', 'ANGLE_DEG', 'LINK_ID';

my $coors = [];
my $inc = 0;
my $lookup = {};
# collect cutting coordinates
for my $line (sort {$a->Middle()->[0] <=> $b->Middle()->[0]} @LINES)
{
    $inc++;
    next unless $line->Length; # ignore zero length lines
    my $start = $line->Coordinates(0);
    my $end = $line->Coordinates(1);
    my $middle = $line->Middle();
    push @{$coors}, $start;
    push @{$coors}, $end;

    my $id_l = sprintf("L%03d", $inc);
    push @{$dxf_out->{ENTITIES}}, {type => 'LINE', _data => [
        [8, $id_l],
        [10, $start->[0]],
        [20, $start->[1]],
        [30, $start->[2]],
        [11, $end->[0]],
        [21, $end->[1]],
        [31, $end->[2]]]};
    push @{$dxf_out->{ENTITIES}}, {type => 'TEXT', _data => [
        [8, $id_l],
        [10, $middle->[0]],
        [20, $middle->[1]],
        [30, $middle->[2]],
        [40, 0.03],
        [1, $id_l]]};
    $lookup->{join '__', @{$start}} = $id_l;
    $lookup->{join '__', @{$end}} = $id_l;
}

$inc = 0;
for my $circle (sort {($a->Radius() *1000) + $a->Centre()->[0] <=>
                      ($b->Radius() *1000) + $b->Centre()->[0]} @CIRCLES)
{
    $inc++;
    say STDERR "Circle $inc/". scalar(@CIRCLES);
    my $centre = $circle->Centre();
    my $radius = $circle->Radius();
    my $id_c = sprintf("C%03d", $inc);
    my $colour = 10 * (1 + ($inc%24));
    push @{$dxf_out->{ENTITIES}}, {type => 'TEXT', _data => [
        [8, $id_c],
        [10, $centre->[0]],
        [20, $centre->[1]],
        [30, $centre->[2]],
        [40, 0.03],
        [62, $colour],
        [1, $id_c]]};

    my $cuts = {0.0 => 1};
    for my $coor (@{$coors})
    {
        my $distance = distance_2d($centre, $coor);
        if (abs($distance - $radius) < 0.001)
        {
            my $angle = angle_2d($centre, $coor);
            $cuts->{Math::Trig::rad2deg($angle)} = 1;
            say $CSV join ', ', $id_c, $radius*2000, Math::Trig::rad2deg($angle), $lookup->{join '__', @{$coor}};
        }
    }
    next unless scalar keys %{$cuts};
    my @angles = sort {$a <=> $b} keys %{$cuts};
    for my $index (0 .. scalar(@angles) -1)
    {
        my $start = $angles[$index -1];
        my $end = $angles[$index];

        push @{$dxf_out->{ENTITIES}}, {type => 'ARC', _data => [
            [8, $id_c],
            [10, $centre->[0]],
            [20, $centre->[1]],
            [30, $centre->[2]],
            [40, $radius],
            [50, $start],
            [51, $end],
            [62, $colour]]};
    }
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;
close $CSV;

0;
