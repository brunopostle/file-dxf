#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /dxf read_DXF/;
use File::DXF::Math qw /multiply_3d add_3d rotate_3d PI/;
use Storable qw/ dclone /;
use Carp;

croak "usage: $0 INPUT.DXF OUTPUT.DXF" unless @ARGV == 2;

my $dxf_in = File::DXF->new;
my $text = read_DXF ($ARGV[0], 'CP1252');
   $dxf_in->Process ($text);

my @report = $dxf_in->{ENTITIES}->Report ('INSERT');

print STDERR 'INSERT entities: '. scalar (@report) ."\n";

# write the DXF file

my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

my $data_out;

for my $insert (@report)
{
    my $shift = [$insert->{x}, $insert->{y}, $insert->{z}];
    my $rotation = PI() * $insert->Rotation / 180;
    my $scale = $insert->Scale;
    my $data = dclone $dxf_in->{BLOCKS}->{$insert->{name}};

    my ($x, $y, $z);

    while (@{$data})
    {
        my ($code, $string) = File::DXF::_next ($data);

        unless ($code =~ /^[123][0-9]$/)
        {
            push @{$data_out}, [$code, $string];
            next;
        }

        $x = $string if ($code =~ /^1[0-9]$/);
        $y = $string if ($code =~ /^2[0-9]$/);
        $z = $string if ($code =~ /^3[0-9]$/);

        # a Z code, capture the suffix
        if ($code =~ /^3([0-9])$/)
        {
            my $point = rotate_3d ($rotation, [$x, $y, $z]);
            $point = multiply_3d ($scale, $point);
            $point = add_3d ($point, $shift);

            push @{$data_out}, ["1$1", $point->[0]], ["2$1", $point->[1]], ["3$1", $point->[2]];
        }
    }
    $dxf_out->{ENTITIES}->Process ($data_out);
}

# write out original entities as-is except inserts
for my $entity (@{$dxf_in->{ENTITIES}})
{
    push @{$dxf_out->{ENTITIES}}, $entity unless $entity->{type} eq 'INSERT';
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;
