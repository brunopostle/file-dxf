#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw /add_3d scale_3d distance_2d distance_3d normalise_3d magnitude_3d add_2d subtract_2d subtract_3d points_2line line_intersection/;
use Carp;

croak "usage: $0 1.1 0.0 LINES_IN.DXF LINES_OUT.DXF" unless @ARGV == 4;

# creates a new DXF file from LINE ENTITIES, where each line is extended by a factor at each end.

my $factor = $ARGV[0];
my $add = $ARGV[1];

my $dxf_in = File::DXF->new;
   $dxf_in->Process (read_DXF ($ARGV[2], 'CP1252'));
my @lines_in = $dxf_in->{ENTITIES}->Report ('LINE');

my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

for my $line_in (@lines_in)
{
    next unless $line_in->Length; # ignore zero length lines
    my $a_in = $line_in->Coordinates (0);
    my $b_in = $line_in->Coordinates (1);
    my $colour = $line_in->{colour};

    my $a_b = scale_3d ($factor, subtract_3d ($b_in, $a_in));
    my $a_out = subtract_3d ($b_in, $a_b);
    my $b_out = add_3d ($a_in, $a_b);

    my $normalised = normalise_3d (subtract_3d ($b_in, $a_in));
    my $add_3d = scale_3d ($add, $normalised);
    $a_out = subtract_3d ($a_out, $add_3d);
    $b_out = add_3d ($b_out, $add_3d);

    push @{$dxf_out->{ENTITIES}}, {type => 'LINE', _data => [
        [8, 0],
        [10, $a_out->[0]],
        [20, $a_out->[1]],
        [30, $a_out->[2]],

        [11, $b_out->[0]],
        [21, $b_out->[1]],
        [31, $b_out->[2]],

        [62, $colour]]};
}

open my $DXF, '>', $ARGV[3];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;
