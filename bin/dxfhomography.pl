#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use File::DXF::Math qw /add_3d scale_3d/;
use File::DXF::Math::Matrix qw(multiply);
use Carp;

# Reads a DXF and multiplies all ENTITIES XYZ coordinates by a given homography
# matrix. Note, this transform doesn't make much sense for many ENTITIES types.

croak "usage: $0 input.dxf output.dxf" unless @ARGV == 2;

# http://jerome.jouvie.free.fr/opengl-tutorials/Lesson2.php

# identity matrix
my $matrix = [[   1,   0,   0,   0 ],
              [   0,   1,   0,   0 ],
              [   0,   0,   1,   0 ],
              [   0,   0,   0,   1 ]];

=cut
# translation matrix
$matrix = [[   1,   0,   0,   2 ],
           [   0,   1,   0,   3 ],
           [   0,   0,   1,   4 ],
           [   0,   0,   0,   1 ]];

# scale matrix
$matrix = [[   2,   0,   0,   0 ],
           [   0,   3,   0,   0 ],
           [   0,   0,   4,   0 ],
           [   0,   0,   0,   1 ]];

# rotation matrix
$matrix = [[0.87,   0, 0.5,   0 ],
           [   0,   1,   0,   0 ],
           [-0.5,   0,0.87,   0 ],
           [   0,   0,   0,   1 ]];

=cut
# perspective matrix
$matrix = [[   1,   0,   0,   0 ],
           [   0,   1,   0,   0 ],
           [   0,   0,   3,  -20 ],
           [   0,   0,  -1,   0 ]];

my $data = read_DXF ($ARGV[0], 'CP1252');

my ($x, $y, $z) = (0.0,0.0,0.0);
my $data_out = [];
my $ENTITIES = 0;

while (@{$data})
{
    my ($code, $string) = File::DXF::_next ($data);

    $ENTITIES = 1 if ($code == 2 and $string eq 'ENTITIES');

    # XYZ codes are '10, 20, 30', '11, 21, 31' etc...
    unless ($ENTITIES and $code =~ /^[123][0-9]$/)
    {
        push @{$data_out}, [$code, $string];
        next;
    }

    $x = $string if ($code =~ /^1[0-9]$/);
    $y = $string if ($code =~ /^2[0-9]$/);
    $z = $string if ($code =~ /^3[0-9]$/);

    # a Z code, capture the suffix
    if ($code =~ /^3([0-9])$/)
    {
        my $point  = [[$x], [$y], [$z], [1.0]];
        my $result = multiply ($matrix, $point);

        my $w = $result->[3]->[0] || 1;
        my $coor = [$result->[0]->[0] /$w, $result->[1]->[0] /$w, $result->[2]->[0] /$w];
        $coor = scale_3d (20, $coor);
        $coor = add_3d ([0,0,60], $coor);

        push @{$data_out}, ["1$1", $coor->[0]], ["2$1", $coor->[1]], ["3$1", $coor->[2]];
    }
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";

for my $item (@{$data_out})
{
    print $DXF dxf (@{$item});
}

close $DXF;

0;

