#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use YAML;
use File::DXF;
use Carp;

croak "usage: $0 local.walls 3dfaces.dxf" unless scalar @ARGV == 2;

my $yaml = YAML::LoadFile ($ARGV[0]);
my $dxf = File::DXF->new;

for my $wall (@{$yaml})
{
    push @{$dxf->{ENTITIES}}, {type => '3DFACE', _data => [
        [8, 0],
        [62, 0],
        [10, $wall->[0]->[0]],
        [20, $wall->[0]->[1]],
        [30, $wall->[0]->[2]],

        [11, $wall->[1]->[0]],
        [21, $wall->[1]->[1]],
        [31, $wall->[0]->[2]],

        [12, $wall->[1]->[0]],
        [22, $wall->[1]->[1]],
        [32, $wall->[1]->[2]],

        [13, $wall->[0]->[0]],
        [23, $wall->[0]->[1]],
        [33, $wall->[1]->[2]]]};
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf->DXF;
close $DXF;

0;
