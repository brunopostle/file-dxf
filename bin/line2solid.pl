#!/usr/bin/perl

use strict;
use warnings;
use lib ('lib', '../lib');
use Carp;
use 5.010;
  
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw /scale_3d subtract_3d add_3d triangle_normal/;

croak "usage: $0 0.2 INPUT.DXF OUTPUT.DXF" unless @ARGV == 3;

# thickens LINE entities, replacing them with a four sided 3DFACE cylinder with given radius

my $radius = $ARGV[0]/2;

my $dxf_in = File::DXF->new;
my $text = read_DXF ($ARGV[1], 'CP1252');
   $dxf_in->Process ($text);

my @lines = $dxf_in->{ENTITIES}->Report ('LINE');

my $dxf_out = File::DXF->new;

$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

for my $line (@lines)
{
    next unless $line->Length; # ignore zero length lines
    my $start = $line->Coordinates (0);
    my $end = $line->Coordinates (1);
    my $higher = [$start->[0], $start->[1], $start->[2]+1];

    # a horizontal vector orthogonal to the centreline
    my $horiz = scale_3d (triangle_normal ($start, $end, $higher), $radius);

    # another vector orthogonal to horiz
    my $other = scale_3d (triangle_normal ($start, $end, add_3d ($start, $horiz)), $radius);

    my $quad_a = [add_3d ($start, $horiz), add_3d ($start, $other), subtract_3d ($start, $horiz), subtract_3d ($start, $other)];
    my $quad_b = [add_3d ($end, $horiz), add_3d ($end, $other), subtract_3d ($end, $horiz), subtract_3d ($end, $other)];

    push @{$dxf_out->{ENTITIES}}, _3dface ($quad_a->[0], $quad_b->[0], $quad_b->[1], $quad_a->[1]);
    push @{$dxf_out->{ENTITIES}}, _3dface ($quad_a->[1], $quad_b->[1], $quad_b->[2], $quad_a->[2]);
    push @{$dxf_out->{ENTITIES}}, _3dface ($quad_a->[2], $quad_b->[2], $quad_b->[3], $quad_a->[3]);
    push @{$dxf_out->{ENTITIES}}, _3dface ($quad_a->[3], $quad_b->[3], $quad_b->[0], $quad_a->[0]);

    push @{$dxf_out->{ENTITIES}}, _3dface ($quad_a->[0], $quad_a->[1], $quad_a->[2], $quad_a->[3]);
    push @{$dxf_out->{ENTITIES}}, _3dface ($quad_b->[3], $quad_b->[2], $quad_b->[1], $quad_b->[0]);
}

open my $DXF, '>', $ARGV[2];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;

sub _3dface
{
    my ($a, $b, $c, $d) = @_;
    my @data;
    push @data, [8, 0];

    push @data, [10, $a->[0]];
    push @data, [20, $a->[1]];
    push @data, [30, $a->[2]];

    push @data, [11, $b->[0]];
    push @data, [21, $b->[1]];
    push @data, [31, $b->[2]];

    push @data, [12, $c->[0]];
    push @data, [22, $c->[1]];
    push @data, [32, $c->[2]];

    push @data, [13, $d->[0]];
    push @data, [23, $d->[1]];
    push @data, [33, $d->[2]];
    return {type => '3DFACE', _data => [@data]};
}

