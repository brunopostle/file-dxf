#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use File::DXF::Math qw /line_plane_intersection add_3d scale_3d/;
use Carp;
use 5.010;

croak "Converts a series of 3D curves into a surface mesh.  Curves are
defined by AutoCAD '3D Polyline' with 'Cubic', 'None' or 'Quadratic'
'Fit/Smooth'. Polylines can be closed or open.  Relies on interpolated
spline points being saved in the DXF file.

Tries to only create developable surfaces, will fail if splines are not suitable.
  Usage: $0 input.dxf output.dxf" unless @ARGV > 1;

my $data = read_DXF ($ARGV[0], 'CP1252');

open my $DXF, ">", $ARGV[1];
binmode $DXF, ":crlf";

print $DXF dxf (0, 'SECTION');
print $DXF dxf (2, 'ENTITIES');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my $vertex_index;
my $index = 0;
my $index_polyline = 0;
my @points;
my $patch;
my $open = undef;

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE')
    {
        $vertex_index = 0;
        next unless defined $item->{mode};
        $open = 1 if ($item->{mode} == 8    # straight open 3d polyline
                   or $item->{mode} == 12); # curved open 3d polyline
        $open = 0 if ($item->{mode} == 9    # straight closed 3d polyline
                   or $item->{mode} == 13); # curved closed 3d polyline

        next unless (defined $open);
        $state = 'POINTS';
        @points = ();
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX')
    {
        next unless defined $item->{mode};
        next unless ($item->{mode} == 32 or $item->{mode} == 40);
        push @points, [$item->{x}, $item->{y}, $item->{z}];
        $vertex_index++;
    }

    if ($item->Type eq 'SEQEND' and $state eq 'POINTS')
    {
        while (@points)
        { 
            my $node = shift @points;
            print $DXF dxf (0, 'POINT');
            print $DXF dxf (8, 0);
            print $DXF dxf (10, $node->[0]);
            print $DXF dxf (20, $node->[1]);
            print $DXF dxf (30, $node->[2]);
            push @{$patch->[$index_polyline]}, $node;
        }

        $state = 'NEXT';
        $index_polyline++;
    }
}

my $patch_new;
my $skip = 1;

for my $m (0 .. scalar @{$patch} -2)
{
    $patch_new->[$m +1]->[0] = [@{$patch->[$m +1]->[0]}];
    my $n2_last = 0;
    my $p_last = [0.0];
    for my $n (1 .. (scalar @{$patch->[0]} -1)/$skip)
    {
        $patch_new->[$m]->[$n] = [@{$patch->[$m]->[$n*$skip]}];
        my $a = $patch->[$m]->[($n*$skip) -1];
        my $b = $patch->[$m]->[($n*$skip)];
        my $c = $patch_new->[$m +1]->[$n -1];
        for my $n2 ($n2_last .. scalar @{$patch->[0]} -2)
        {
            my $p1 = $patch->[$m +1]->[$n2];
            my $p2 = $patch->[$m +1]->[$n2 +1];
            my $p = line_plane_intersection ($p1, $p2, $a, $b, $c);
            next if $p > 1.0000001;
            next if $p < -0.0000001;
            next if ($n2 == $n2_last and $p - 0.1 <= $p_last->[$n2_last]);
            # if we have a huge gap, assume a singularity
            if ($n2 > 16 + $n2_last)
            {
                $patch_new->[$m +1]->[$n] = [@{$patch_new->[$m +1]->[$n -1]}];
                last;
            }
            $patch_new->[$m +1]->[$n] = add_3d (scale_3d (1-$p, $p1), scale_3d ($p, $p2));
            $n2_last = $n2;
            $p_last->[$n2_last] = $p;
            last;
        }
        $patch_new->[$m +1]->[$n] = [@{$patch_new->[$m +1]->[$n -1]}]
            unless defined $patch_new->[$m +1]->[$n];
    }
    $patch->[$m +1] = [@{$patch_new->[$m +1]}];
}

print $DXF dxf (0, 'POLYLINE');
print $DXF dxf (8, 0);
print $DXF dxf (66, 1);
print $DXF dxf (70, 16) if $open;
print $DXF dxf (70, 48) unless $open;
print $DXF dxf (71, scalar @{$patch});
print $DXF dxf (72, scalar @{$patch->[0]});

for my $m (0 .. scalar @{$patch} -1)
{
    for my $n (0 .. scalar @{$patch->[0]} -1)
    {
        print $DXF dxf (0, 'VERTEX');
        print $DXF dxf (8, 0);
        print $DXF dxf (10, $patch->[$m]->[$n]->[0]);
        print $DXF dxf (20, $patch->[$m]->[$n]->[1]);
        print $DXF dxf (30, $patch->[$m]->[$n]->[2]);
        print $DXF dxf (70, 64);
    }
}

print $DXF dxf (0, 'SEQEND');
print $DXF dxf (8, 0);

print $DXF dxf (0, 'ENDSEC');
print $DXF dxf (0, 'EOF');

close $DXF;

exit 0;

