#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use 5.010;
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::DXF::ENTITIES;
use Carp;

# Reads a DXF and generates renderman RIB geometry.
# only handles POLYLINE 'polyface meshes'

croak "usage: $0 input.dxf output.rib" unless @ARGV == 2;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

my $polygon_geometries;

for my $mesh ($dxf->{ENTITIES}->Extract_Meshes)
{
    $polygon_geometries .= _rib_mesh ($mesh->[0], $mesh->[1]);
}

open my $RIB, '>', $ARGV[1];

my $tif = $ARGV[1];
$tif =~ s/\.rib$/.tif/;

say $RIB 'FrameBegin 1';

say $RIB '  Format 960 540 1.0';
say $RIB '  Display "'.$tif.'" "file" "rgb"';
say $RIB '  Projection "perspective" "fov" 50';
say $RIB '  Translate 2 8 31';
say $RIB '  Rotate -130 1 0 0';
say $RIB '  Rotate 20 0 0 1';

say $RIB '  WorldBegin';

say $RIB '    Attribute "visibility" "diffuse" 1  # make objects visible to eye';
say $RIB '    Attribute "trace" "bias" 0.005';
say $RIB '    Surface "occsurf2" "samples" 1024';
say $RIB '    AttributeBegin';
say $RIB '      Color [0.8 0.8 0.8]';
say $RIB '      PointsPolygons [4]';
say $RIB '      [0 1 2 3]';
say $RIB '      "P" [-50 -50 0 50 -50 0 50 50 0 -50 50 0]';
say $RIB '    AttributeEnd';

print $RIB $polygon_geometries;

say $RIB '  WorldEnd';

say $RIB 'FrameEnd';

close $RIB;

exit 0;

sub _rib_mesh
{
    my ($nodes, $triangles) = @_;
    say STDERR "Writing mesh. nodes, triangles: ".
        scalar @{$nodes}. ', ' .scalar @{$triangles};

    my $mesh = '';
    $mesh .= "    AttributeBegin\n"; 
    $mesh .= "      Color [ 0.8 0.8 0.8 ]\n";

    my @sizes = map {my @a = split (' ', $_); scalar @a} @{$triangles};
    $mesh .= join ' ', '      PointsPolygons', '[', @sizes, "]\n";

    $mesh .= join ' ', '      [', @{$triangles}, "]\n";

    $mesh .= join ' ', '      "P"', '[', @{$nodes}, "]\n";

    $mesh .= "    AttributeEnd\n"; 

    return $mesh;
}

