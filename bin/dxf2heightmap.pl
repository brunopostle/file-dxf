#!/usr/bin/perl

use strict;
use warnings;
use 5.010;

use lib 'lib';

use File::DXF::Util qw /read_DXF/;
use File::DXF;
use SVG;
use Carp;

croak "usage: $0 LANDSCAPE.DXF LANDSCAPE.SVG"
  unless @ARGV == 2;

# Convert a DXF with meshes into a SVG heightmap
# each face is colourised based in elevation
# scale etc... is hard-coded

my $min = [1022.0, 2060.0, 836.0];
my $max = [1332.0, 2218.0, 842.0];

my $scale = 10;
my $scale_z = 50;

my $width = ($max->[0] - $min->[0]) * $scale;
my $height= ($max->[1] - $min->[1]) * $scale;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

# size the output canvas
my $svg = SVG->new (width => $width, height => $height);

$svg->rectangle (x => 0, y => 0,
                 width => $svg->{-document}->{width}, height => $svg->{-document}->{height},
                 style => {fill => "rgb(212,212,212)"});

for my $mesh ($dxf->{ENTITIES}->Extract_Meshes)
{
    my $nodes = $mesh->[0];
    my $triangles = $mesh->[1];

    for my $triangle (@{$triangles})
    {
        my @face = split ' ', $triangle;

        my $a = [split ' ', $nodes->[$face[0]]];
        my $b = [split ' ', $nodes->[$face[1]]];
        my $c = [split ' ', $nodes->[$face[2]]];

        my $elev = ($a->[2] + $b->[2] + $c->[2]) / 3;
        my $foo = int (($elev - $min->[2]) * $scale_z);
        my $tone = "rgb($foo,$foo,$foo)";

        # try colours
        my $bar = ($elev - 838.7) * $scale_z;

        my ($red, $green, $blue) = (0,0,0);

        if    ($bar > -128 and $bar < -64)
        {
            $red   = 0;
            $green = int (($bar +128) *4);
            $blue  = int ((-64 - $bar) *4);
        }
        elsif ($bar >= -64 and $bar < 0)
        {
            $red   = int (($bar +64) *4);
            $green = 255;
            $blue  = int (($bar +64) *4);
        }
        elsif ($bar >= 0 and $bar < 64)
        {
            $red   = 255;
            $green = 255;
            $blue  = int ((64 - $bar) *4);
        }
        elsif ($bar >= 64 and $bar < 128)
        {
            $red   = 255;
            $green = int ((128 - $bar) *4);
            $blue  = 0;
        }
        else
        {
            $red = 0;
            $green = 0;
            $blue = 0;
        }

        # comment this out to get a greyscale image
        $tone = "rgb($red,$green,$blue)";

        my $A = _paperposition ($a);
        my $B = _paperposition ($b);
        my $C = _paperposition ($c);

        my $xv = [$A->[0],$B->[0],$C->[0]];
        my $yv = [$A->[1],$B->[1],$C->[1]];

        # draw the triangle on the canvas
        my $points = $svg->get_path (x => $xv, y => $yv, -type => 'polygon');
        $svg->polygon (%$points,
                       style => { 'stroke-width' => 1.0,
                                        'stroke' => $tone,
                                          'fill' => $tone }
                      );
    }
}

# iterate through all the triangles in the first group

sub _paperposition
{
    my $point = shift;

    # place mesh at the origin
    my $new = subtract_vector ($point, $min);
    # scale to canvas dimensions
    $new = scale_vector ($new, $scale);
    # flip vertically
    $new = subtract_vector ([$new->[0],$height,$new->[2]], [0,$new->[1],0]);
    return $new;
}

# dump the svg file
open my $SVGFILE, ">:encoding(UTF-8)", $ARGV[1];
print $SVGFILE $svg->xmlify;
close $SVGFILE;

exit 0;

sub scale_vector
{
    my $vector = shift;
    my $factor = shift;

    $vector->[0] *= $factor;
    $vector->[1] *= $factor;
    $vector->[2] *= $factor;
    return $vector;
}

sub subtract_vector
{
    my $vector = shift;
    carp 'no lhs in vector subtraction' unless defined $vector->[0];
    my $subtract = shift;
    carp 'no rhs in vector subtraction' unless defined $subtract->[0];
    my $answer = [];
    $answer->[0] = $vector->[0] - $subtract->[0];
    $answer->[1] = $vector->[1] - $subtract->[1];
    $answer->[2] = $vector->[2] - $subtract->[2];
    return $answer;
}

sub add_vector
{
    my $vector = shift;
    my $add = shift;
    my $answer = [];
    $answer->[0] = $vector->[0] + $add->[0];
    $answer->[1] = $vector->[1] + $add->[1];
    $answer->[2] = $vector->[2] + $add->[2];
    return $answer;
}

