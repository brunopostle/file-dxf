#!/usr/bin/perl

use strict;
use warnings;
use File::DXF;

open my $STL, '<', $ARGV[0];

my $dxf = File::DXF->new;
my $face = [];

for my $line (<$STL>)
{
    my @t = split ' ', $line;
    push @{$face}, [$t[1], $t[2], $t[3]] if ($t[0] eq 'vertex');
    next unless ($t[0] eq 'endloop');
    push @{$dxf->{ENTITIES}}, {type => '3DFACE', _data => [
        [8, 0],
        [62, 0],
        [10, $face->[0]->[0]],
        [20, $face->[0]->[1]],
        [30, $face->[0]->[2]],

        [11, $face->[1]->[0]],
        [21, $face->[1]->[1]],
        [31, $face->[1]->[2]],

        [12, $face->[2]->[0]],
        [22, $face->[2]->[1]],
        [32, $face->[2]->[2]],

        [13, $face->[2]->[0]],
        [23, $face->[2]->[1]],
        [33, $face->[2]->[2]]]};
    $face = [];
}

close $STL;

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf->DXF;
close $DXF;

0;
