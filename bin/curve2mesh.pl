#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use File::DXF::Math::Bezier;
use Carp;

croak "Converts a series of 3D curves into a surface mesh.
Curves are defined by a four-point AutoCAD '3D Polyline' with 'Cubic Fit/Smooth'.
or by a three-point AutoCAD '3D Polyline' with 'quadratic Fit/Smooth'.
  Usage: $0 input.dxf output.dxf [GRIDSIZE]" unless @ARGV > 1;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $N = 65;
$N = $ARGV[2] if defined $ARGV[2];

open my $DXF, ">", $ARGV[1];
binmode $DXF, ":crlf";

print $DXF dxf (0, 'SECTION');
print $DXF dxf (2, 'ENTITIES');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my $vertex_index;
my $index = 0;
my $index_polyline = 0;
my @points;
my $patch;
my $smooth;

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE')
    {
        $vertex_index = 0;
        next unless defined $item->{mode};
        next unless $item->{mode} == 12;
        $smooth = $item->{smooth};
        next unless ($smooth == 5 or $smooth == 6);
        $state = 'POINTS';
        @points = ();
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX')
    {
        next unless defined $item->{mode};
        next unless $item->{mode} == 48;
        push @points, [$item->{x}, $item->{y}, $item->{z}];
        $vertex_index++;
    }

    if ($item->Type eq 'SEQEND' and $state eq 'POINTS')
    {
        # bezier curve is equivalent to just
        # 3-point quadratic fit polylines and
        # 4-point cubic fit polylines
        next unless (($smooth == 5 and scalar @points == 3)
                  or ($smooth == 6 and scalar @points == 4));
        my $bezier = File::DXF::Math::Bezier->new (map {@{$_}} @points);
        my @curve = $bezier->curve ($N);

        while (@curve)
        { 
            my @node = splice (@curve, 0, 3);
            print $DXF dxf (0, 'POINT');
            print $DXF dxf (8, 0);
            print $DXF dxf (10, $node[0]);
            print $DXF dxf (20, $node[1]);
            print $DXF dxf (30, $node[2]);
            push @{$patch->[$index_polyline]}, [@node];
        }

        $state = 'NEXT';
        $index_polyline++;
    }
}

print $DXF dxf (0, 'POLYLINE');
print $DXF dxf (8, 0);
print $DXF dxf (66, 1);
print $DXF dxf (70, 16);
print $DXF dxf (71, scalar @{$patch});
print $DXF dxf (72, scalar @{$patch->[0]});

for my $m (0 .. scalar @{$patch} -1)
{
    for my $n (0 .. scalar @{$patch->[0]} -1)
    {
        print $DXF dxf (0, 'VERTEX');
        print $DXF dxf (8, 0);
        print $DXF dxf (10, $patch->[$m]->[$n]->[0]);
        print $DXF dxf (20, $patch->[$m]->[$n]->[1]);
        print $DXF dxf (30, $patch->[$m]->[$n]->[2]);
        print $DXF dxf (70, 64);
    }
}

print $DXF dxf (0, 'SEQEND');
print $DXF dxf (8, 0);

print $DXF dxf (0, 'ENDSEC');
print $DXF dxf (0, 'EOF');

close $DXF;

exit 0;

