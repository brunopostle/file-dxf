#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use File::DXF;

die "Parses a STAAD *.std file and extracts linear elements as
DXF LINE elements with layers assigned by member property.

Usage:
  $0 INPUT.STD OUTPUT.DXF\n" unless scalar (@ARGV) == 2;

open my $STAAD, '<', $ARGV[0];
binmode ($STAAD, ':crlf');

my $section = {};
my $SECTION = 'INIT';
for my $line (<$STAAD>)
{
    next if $line =~ /^\*/;
    if ($line =~ /^(START\sJOB\sINFORMATION|
        END\sJOB\sINFORMATION|
        JOINT\sCOORDINATES|
        MEMBER\sINCIDENCES|
        ELEMENT\sINCIDENCES\sSHELL|
        ELEMENT\sINCIDENCES\sSOLID|
        DEFINE\sPMEMBER|
        DEFINE\sMATERIAL\sSTART|
        END\sDEFINE\sMATERIAL|
        MEMBER\sPROPERTY\sBRITISH|
        MEMBER\sPROPERTY\sEUROPEAN|
        MEMBER\sPROPERTY\sCHINESE|
        MEMBER\sPROPERTY\sTIMBER\sCANADIAN|
        CONSTANTS|
        SUPPORTS|
        MEMBER\sRELEASE|
        MEMBER\sTENSION\s?|
        MEMBER\sLOAD|
        MEMBER\sTRUSS\s?|
        JOINT\sLOAD|
        LOAD\sCOMB\s(?:.*)|
        PERFORM\sANALYSIS|
        FINISH)$/x)
        {
            $SECTION = $1;
            while (defined $section->{$SECTION})
            {
                $SECTION .= '-X';
            }
            #say $SECTION;
            next;
        }

        $line =~ s/\n//;
    push @{$section->{$SECTION}}, $line;
}

close $STAAD;

my @items = split /; */, join '', @{$section->{'JOINT COORDINATES'}};
my $coors = {};
for my $item (@items)
{
    my @token = split ' ', $item;
    $coors->{$token[0]} = [$token[1], $token[2], $token[3]];
}

@items = split /; */, join '', @{$section->{'MEMBER INCIDENCES'}};
my $members = {};
for my $item (@items)
{
    my @token = split ' ', $item;
    $members->{$token[0]} = [$token[1], $token[2]];
}

my $properties = {};
my $text = '';
for my $member_property (keys %{$section})
{
    next unless $member_property =~ /MEMBER PROPERTY (.*)/;
    #say $member_property;
     
    for my $line (@{$section->{$member_property}})
    {
        if ($line =~ /(.* )-$/)
        {
            $text .= $1;
        }
        else
        {
            $text .= $line;
            my ($items, $key) = $text =~ /((?:[0-9 ]|TO)+) ((?:PRIS|TABLE).*)/;
            my @tokens = $items =~ /([0-9]+ TO [0-9]+|[0-9]+)/g;
            for my $token (@tokens)
            {
                if ($token =~ /^[0-9]+$/)
                {
                    push @{$properties->{$key}}, $token +0;
                }
                elsif ($token =~ /^([0-9]+) TO ([0-9]+)$/)
                {
                    push @{$properties->{$key}}, $1 .. $2;
                }
            }
            $text = '';
        }
    }
}
my $properties_rev = {};
for my $property_name (keys %{$properties})
{
    for my $member (@{$properties->{$property_name}})
    {
        $properties_rev->{$member} = $property_name;
    }
}

my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

for my $member_id (keys %{$members})
{
    my $member = $members->{$member_id};
    push @{$dxf_out->{ENTITIES}}, {type => 'LINE', _data => [
        [8, $properties_rev->{$member_id}],
        [10, $coors->{$member->[0]}->[0]],
        [20, $coors->{$member->[0]}->[2]],
        [30, $coors->{$member->[0]}->[1]],
        [11, $coors->{$member->[1]}->[0]],
        [21, $coors->{$member->[1]}->[2]],
        [31, $coors->{$member->[1]}->[1]]]};
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;
