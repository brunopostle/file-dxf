#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF::Math qw /point_is_in_triangle map_point map_point_quad/;
use File::DXF;
use Carp;
use 5.010;

# will map arbitary geometry superimposed on one mesh to another mesh with
# identical topology. e.g. unroll a mesh with unroll.pl, draw points, lines,
# 3dfaces on the unrolled mesh and use this tool to map them onto the original
# mesh. Similarly any objects drawn on a developable surface can be mapped onto
# the developed surface.

croak "usage: $0 ref_in.dxf ref_out.dxf input.dxf output.dxf" unless @ARGV == 4;

my $dxf_1 = File::DXF->new;
   $dxf_1->Process (read_DXF ($ARGV[0], 'CP1252'));
my ($mesh_1) = $dxf_1->{ENTITIES}->Extract_Meshes;

my $dxf_2 = File::DXF->new;
   $dxf_2->Process (read_DXF ($ARGV[1], 'CP1252'));
my ($mesh_2) = $dxf_2->{ENTITIES}->Extract_Meshes;

my $data = read_DXF ($ARGV[2], 'CP1252');

my ($x, $y, $z) = (0.0,0.0,0.0);
my $data_out = [];
my $ENTITIES = 0;

while (@{$data})
{
    my ($code, $string) = File::DXF::_next ($data);

    $ENTITIES = 1 if ($code == 2 and $string eq 'ENTITIES');

    # XYZ codes are '10, 20, 30', '11, 21, 31' etc...
    unless ($ENTITIES and $code =~ /^[123][0-9]$/)
    {
        push @{$data_out}, [$code, $string];
        next;
    }

    $x = $string if ($code =~ /^1[0-9]$/);
    $y = $string if ($code =~ /^2[0-9]$/);
    $z = $string if ($code =~ /^3[0-9]$/);

    # a Z code, capture the suffix
    if ($code =~ /^3([0-9])$/)
    {
        my $coor = [$x,$y,$z];
        my $newcoor = [$x,$y,$z];
        for my $triangle_id (0 .. scalar @{$mesh_1->[1]} -1)
        {
            my @node_ids = split ' ', $mesh_1->[1]->[$triangle_id];

            my $A1 = [split ' ', $mesh_1->[0]->[$node_ids[0]]];
            my $B1 = [split ' ', $mesh_1->[0]->[$node_ids[1]]];
            my $C1 = [split ' ', $mesh_1->[0]->[$node_ids[2]]];

            my $A2 = [split ' ', $mesh_2->[0]->[$node_ids[0]]];
            my $B2 = [split ' ', $mesh_2->[0]->[$node_ids[1]]];
            my $C2 = [split ' ', $mesh_2->[0]->[$node_ids[2]]];

            if (scalar @node_ids == 3)
            {
            if (point_is_in_triangle ($A1, $B1, $C1, $coor, 0.999))
            {
                $newcoor = map_point ($A1, $B1, $C1, $A2, $B2, $C2, $coor);
                last;
            }
            }
            if (scalar @node_ids == 4) # some triangles have four sides
            {
                my $D1 = [split ' ', $mesh_1->[0]->[$node_ids[3]]];
                my $D2 = [split ' ', $mesh_2->[0]->[$node_ids[3]]];

                if (point_is_in_triangle ($A1, $C1, $D1, $coor, 0.999) or point_is_in_triangle ($A1, $B1, $C1, $coor, 0.999))
                {
                    $newcoor = map_point_quad ($A1, $B1, $C1, $D1, $A2, $B2, $C2, $D2, $coor);
                    last;
                }
            }
        }
        push @{$data_out}, ["1$1", $newcoor->[0]], ["2$1", $newcoor->[1]], ["3$1", $newcoor->[2]];
    }
}

open my $DXF, '>', $ARGV[3];
binmode $DXF, ":crlf";

for my $item (@{$data_out})
{
    print $DXF dxf (@{$item});
}

close $DXF;

0;

