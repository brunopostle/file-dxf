#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use YAML;
use Carp;

croak "usage: $0 lines.dxf local.walls" unless scalar @ARGV == 2;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

my @report = $dxf->{ENTITIES}->Report (('LINE'));

my @walls;
for my $item (@report)
{
    my $a = $item->Coordinates (0);
    my $b = $item->Coordinates (1);
    $b->[2] = $a->[2] + $item->{thickness};
    my $type = lc $item->{layer};
    push @walls, [$a, $b, $type];
}

YAML::DumpFile ($ARGV[1], [@walls]);

0;
