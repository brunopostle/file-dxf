#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw /distance_3d/;
use File::DXF;
use File::DXF::ENTITIES;
use Carp;
use 5.010;

# Subdivide DXF polyface meshes into quads
# All triangles in the mesh(es) are written as three four-sided 3DFACE entities
# Quads are written as four four-sided 3DFACE entities

croak "usage: $0 input.dxf output.dxf" unless @ARGV == 2;
binmode STDOUT, ":unix";

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

my $dxf_out = File::DXF->new;

my @list;

for my $mesh ($dxf->{ENTITIES}->Extract_Meshes)
{
    my $nodes = $mesh->[0];
    my $triangles = $mesh->[1];

    for my $triangle (@{$triangles})
    {
        my @face = split ' ', $triangle;

        my $a = [split ' ', $nodes->[$face[0]]];
        my $b = [split ' ', $nodes->[$face[1]]];
        my $c = [split ' ', $nodes->[$face[2]]];

        if (scalar @face == 3)
        {
            push @list, [$a, _av($a,$b), _av($a,$b,$c), _av($a,$c)];
            push @list, [$b, _av($b,$c), _av($a,$b,$c), _av($a,$b)];
            push @list, [$c, _av($a,$c), _av($a,$b,$c), _av($b,$c)];
        }

        if (scalar @face == 4)
        {
            my $d = [split ' ', $nodes->[$face[3]]];

            push @list, [$a, _av($a,$b), _av($a,$b,$c,$d), _av($a,$d)];
            push @list, [$b, _av($b,$c), _av($a,$b,$c,$d), _av($a,$b)];
            push @list, [$c, _av($c,$d), _av($a,$b,$c,$d), _av($b,$c)];
            push @list, [$d, _av($d,$a), _av($a,$b,$c,$d), _av($c,$d)];
        }
    }
}

sub _av
{
    my $sum;
    for (@_)
    {
        $sum->[0] += $_->[0];
        $sum->[1] += $_->[1];
        $sum->[2] += $_->[2];
    }
    my $count = scalar @_;
    return [map {$_ / $count} @{$sum}];
}

my ($max, $min);

for my $face (@list)
{
    for my $id (0 .. 3)
    {
        my $length = distance_3d ($face->[$id-1], $face->[$id]);
        $max = $length unless defined $max;
        $min = $length unless defined $min;
        $max = $length if $length > $max;
        $min = $length if $length < $min;
        say join (' ', @{$face->[$id-1]}, '     ', @{$face->[$id]}) if $length < 0.001;
    }
    push @{$dxf_out->{ENTITIES}}, {type => '3DFACE', _data => [
        [8, 0],
        [62, 0],
        [10, $face->[0]->[0]],
        [20, $face->[0]->[1]],
        [30, $face->[0]->[2]],

        [11, $face->[1]->[0]],
        [21, $face->[1]->[1]],
        [31, $face->[1]->[2]],

        [12, $face->[2]->[0]],
        [22, $face->[2]->[1]],
        [32, $face->[2]->[2]],

        [13, $face->[3]->[0]],
        [23, $face->[3]->[1]],
        [33, $face->[3]->[2]]]};
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

say "Shortest edge: $min";
say "Longest edge: $max";

0;
