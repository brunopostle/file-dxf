#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util 'read_DXF';
use File::DXF;
use File::DXF::ENTITIES;
use Carp;

croak "usage: $0 foo.dxf" unless scalar @ARGV == 1;

# Takes one DXF file, extracts each layer as a separate DXF file
# Filenames are layer names with .dxf appended
# Block INSERT entities are all converted to POINT entities (sorry, it's what I needed)

my $data = read_DXF ($ARGV[0], 'CP1252');
my $dxf_in = File::DXF->new;
$dxf_in->Process ($data);

my $layers = {};

for my $entity (@{$dxf_in->{ENTITIES}})
{
    my $layer = Layer ($entity);

    if ($entity->{type} eq 'INSERT')
    {
       my @items;
       for my $item (@{$entity->{_data}})
       {
           push @items, $item if ($item->[0] =~ /^(5|8|6|10|20|30|50)$/);
       }

       push @{$layers->{$layer}}, {_data => [@items], type => 'POINT'};
    }
    else
    {
        push @{$layers->{$layer}}, $entity;
    }
}

for my $layer (keys %{$layers})
{
    my $dxf_out = File::DXF->new;
    $dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

    push @{$dxf_out->{ENTITIES}}, @{$layers->{$layer}};

    my $path_out = $layer .'.dxf';
    open my $DXF, '>', $path_out;
    binmode $DXF, ":crlf";
    print $DXF $dxf_out->DXF;
    close $DXF;
}

sub Layer
{
    my $self = shift;
    for my $item (@{$self->{_data}})
    {
        return $item->[1] if $item->[0] == 8;
    }
    return 0;
}

0;

