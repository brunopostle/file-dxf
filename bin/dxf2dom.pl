#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::Spec;
use YAML;
use Carp;
use 5.010;

croak "Usage: $0 /path/to/polylines.dxf" unless scalar @ARGV;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my $elevation;
my @points;
my $index = 0;

my ($volume,$directories,$name_project) = File::Spec->splitpath ($ARGV[0]);
$name_project =~ s/\.dxf$//i;
mkdir $name_project || croak "can't create folder '$name_project'";

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE')
    {
        next unless defined $item->{mode};
        next unless $item->{mode} == 1; # 2d closed polyline
        $elevation = $item->{z};
        $state = 'POINTS';
        @points = ();
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX')
    {
        push @points, [$item->{x}, $item->{y}];
    }

    if ($item->Type eq 'SEQEND' and $state eq 'POINTS')
    {
        next unless scalar @points == 4;

        my $yaml = { perimeter => {a => undef, b => undef, c => undef, d => undef},
                     elevation => $elevation, node => [@points],
                     height => 3.0, rotation => 0, type => 'O' };

        my $path_plot = 'plot-'.$index;
        mkdir (File::Spec->catdir ($name_project, $path_plot));
        YAML::DumpFile (File::Spec->catfile ($name_project, $path_plot, 'init.dom'), $yaml);
        say "Created '$name_project/$path_plot/init.dom'";
        $state = 'NEXT';
        $index++;
    }
}

exit 0;

