#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use 5.010;
use Carp;
use File::Slurp;

croak "Converts POLYLINE objects into separate DXF files.
  Usage: $0 input.dxf" unless @ARGV == 1;

my $dxf = read_file($ARGV[0]);

my (@polylines) = $dxf =~ m/( 0\r\n POLYLINE\r\n .*? 0\r\n SEQEND\r\n )/sgx;

say scalar @polylines;

my $index = 0;

for my $polyline (@polylines)
{

    my $path_out = '_tmp_'.sprintf("%08D", $index).'.dxf';
    open my $DXF, ">", $path_out;
    binmode $DXF, ":crlf";

    print $DXF "0\r\nSECTION\r\n";
    print $DXF "2\r\nENTITIES\r\n";

    print $DXF $polyline;
    print $DXF "8\r\n0\r\n";

    print $DXF "  0\r\nENDSEC\r\n";
    print $DXF "0\r\nEOF\r\n";

    close $DXF;
    system ('dos2unix', $path_out);
    system ('dos2unix', $path_out);
    system ('unix2dos', $path_out);

    $index++;
}

0;
