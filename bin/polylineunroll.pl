#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw (distance_3d point_is_in_triangle flatten_triangle);
use Carp;
use 5.010;

croak "Usage: $0 IN.DXF OUT.DXF"
    unless (scalar @ARGV == 2);

my @points_3d = ();
my @labels_3d = ();
my $elevation = 0.0;

my $dxf_in = File::DXF->new;
my $dxf_out = File::DXF->new;

$dxf_in->Process (read_DXF ($ARGV[0], 'CP1252'));
my $entities = $dxf_in->{ENTITIES};

my @mesh = $entities->Extract_Meshes;

for my $item ($entities->Report ('POINT'))
{
    push @points_3d, $item->Coordinates;
}

for my $item ($entities->Report ('TEXT'))
{
    push @labels_3d, [$item->Coordinates, $item->Content, $item->Colour, $item->{layer}];
}

for my $mesh (@mesh)
{

my @points_2d = ();
my @labels_2d = ();

my $nodes_3d = [map {[split / /, $_]} @{$mesh->[0]}];
my $triangles_3d = [map {[split / /, $_]} @{$mesh->[1]}];
my $triangles_3d_orig = [map {[split / /, $_]} @{$mesh->[1]}];
my $mesh_colour = $mesh->[2]->{colour};
my $mesh_layer = $mesh->[2]->{layer};

my $temp;
for my $t (@{$triangles_3d})
{
    push @{$temp}, $t if scalar @{$t} == 3;
    push @{$temp}, [$t->[0], $t->[1], $t->[2]], [$t->[2], $t->[3], $t->[0]] if scalar @{$t} ==4;
}
$triangles_3d = $temp;

# pick any two adjacent points

my $nodes = $triangles_3d->[0];

my $a_3d = $nodes_3d->[$nodes->[0]];
my $b_3d = $nodes_3d->[$nodes->[1]];

# draw first edge on y-axis as a seed

my $nodes_2d;
$nodes_2d->[$nodes->[0]] = [0.0, 0.0, 0.0];
$nodes_2d->[$nodes->[1]] = [0.0, distance_3d ($a_3d, $b_3d), 0.0];

# keep going until all nodes have been flattened

my $dirty = 1;
while ($dirty == 1)
{
    $dirty = 0;

    # iterate through all the triangles

    for my $triangle (0 .. scalar @{$triangles_3d} -1)
    {
        # get three node ids for triangle

        my $nodes = $triangles_3d->[$triangle];

        # we can only flatten this triangle if two nodes already flat

        my $done = 0;
        $done++ if $nodes_2d->[$nodes->[0]];
        $done++ if $nodes_2d->[$nodes->[1]];
        $done++ if $nodes_2d->[$nodes->[2]];
        next unless $done == 2;

        # figure out which node needs flattening

        my ($a_id, $b_id, $c_id);
        if (!$nodes_2d->[$nodes->[0]])
        {
            $a_id = $nodes->[1];
            $b_id = $nodes->[2];
            $c_id = $nodes->[0];
        }
        elsif (!$nodes_2d->[$nodes->[1]])
        {
            $a_id = $nodes->[2];
            $b_id = $nodes->[0];
            $c_id = $nodes->[1];
        }
        elsif (!$nodes_2d->[$nodes->[2]])
        {
            $a_id = $nodes->[0];
            $b_id = $nodes->[1];
            $c_id = $nodes->[2];
        }

        my $a_3d = $nodes_3d->[$a_id];
        my $b_3d = $nodes_3d->[$b_id];
        my $c_3d = $nodes_3d->[$c_id];

        my $a_2d = $nodes_2d->[$a_id];
        my $b_2d = $nodes_2d->[$b_id];

        my $error = distance_3d ($a_3d, $b_3d) - distance_3d ($a_2d, $b_2d); 
        say STDERR join ' ', 'Distortion:', sprintf ("%.4f", $error), "($a_id $b_id)"
            if (abs $error > 0.0001);

        for my $point (@points_3d)
        {
            next unless point_is_in_triangle ($a_3d, $b_3d, $c_3d, $point, 0.9999999);
            my $point_2d = flatten_triangle ($a_3d, $b_3d, $point, $a_2d, $b_2d);
            push @points_2d, $point_2d;
        }

        for my $label_3d (@labels_3d)
        {
            my ($point, $label, $colour, $layer) = @{$label_3d};
            next unless point_is_in_triangle ($a_3d, $b_3d, $c_3d, $point, 0.9999999);
            my $point_2d = flatten_triangle ($a_3d, $b_3d, $point, $a_2d, $b_2d);
            push @labels_2d, [$point_2d, $label, $colour, $layer];
        }

        my $c_2d = flatten_triangle ($a_3d, $b_3d, $c_3d, $a_2d, $b_2d);

        $nodes_2d->[$c_id] = $c_2d;
        $dirty = 1;
    }
}

for my $point_2d (@points_2d)
{
    push @{$dxf_out->{ENTITIES}},
        {type => 'POINT',
        _data => [[8, 0], [10, $point_2d->[0]], [20, $point_2d->[1]], [30, $elevation]]};
}

for my $label_2d (@labels_2d)
{
    my ($point_2d, $label, $colour, $layer) = @{$label_2d};
    push @{$dxf_out->{ENTITIES}},
        {type => 'TEXT',
        _data => [[8, $layer], [1, $label], [10, $point_2d->[0]], [20, $point_2d->[1]], [30, $elevation], [40, 0.1], [62, $colour]]};
}

for my $node_id (0 .. scalar @{$nodes_2d} -1)
{
    # unconnected nodes won't have got remapped, put them at the origin
    $nodes_2d->[$node_id] = [0,0] unless defined $nodes_2d->[$node_id]->[0];
    $nodes_2d->[$node_id]->[2] = $elevation;
}

my @faces = map {{node_id => $_, colour => $mesh_colour}} @{$triangles_3d_orig};

push @{$dxf_out->{ENTITIES}},
    $dxf_in->{ENTITIES}->Mesh ($nodes_2d, [@faces], {layer => $mesh_layer, colour => $mesh_colour});

$elevation++;

}

open my $DXF ,">", $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;

