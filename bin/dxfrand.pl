#!/usr/bin/perl

use strict;
use warnings;

# takes a dxf file, tries to randomise coordinates
# rotation and normals.  Only really suitable for block references, circles or text

my $dxf = shift;
my @out;

open my $DXF, '<', $dxf;
my @dxf = (<$DXF>);
close $DXF;

my $state = 'DEFAULT';
my $entities = 0;
my $scale;

for my $line (@dxf)
{
    chomp $line;
    $line =~ s/(\r|\l|\n)//g;
    $entities = 1 if $line =~ /^ENTITIES$/;
    $entities = 0 if $line =~ /^OBJECTS$/;

    if ($line =~ /^ *(10|20|30)$/ and $entities == 1 and $state eq 'DEFAULT')
    {
        push @out, $line; 
	$state = 'COOR';
    }
    elsif ($line =~ /^ *41$/ and $entities == 1 and $state eq 'DEFAULT')
    {
        push @out, $line; 
        $scale = rand (1) + 0.5;
	$state = 'SCALE';
    }
    elsif ($line =~ /^ *(42|43)$/ and $entities == 1 and $state eq 'DEFAULT')
    {
        push @out, $line; 
	$state = 'SCALE';
    }
    elsif ($line =~ /^ *50$/ and $entities == 1 and $state eq 'DEFAULT')
    {
        push @out, $line; 
	$state = 'ROTATION';
    }
    elsif ($line =~ /^ *(210|220|230)$/ and $entities == 1 and $state eq 'DEFAULT')
    {
        push @out, $line; 
	$state = 'NORMAL';
    }

    elsif ($line =~ /^[0-9.-]+/ and $state eq 'COOR')
    {
        push @out, rand (20000) - 10000;
	$state = 'DEFAULT';
    }
    elsif ($line =~ /^[0-9.-]+/ and $state eq 'SCALE')
    {
        push @out, $scale;
	$state = 'DEFAULT';
    }
    elsif ($line =~ /^[0-9.-]+/ and $state eq 'ROTATION')
    {
        push @out, rand (360);
	$state = 'DEFAULT';
    }
    elsif ($line =~ /^[0-9.-]+/ and $state eq 'NORMAL')
    {
        push @out, rand (2) - 1.0;
	$state = 'DEFAULT';
    }
    else
    {
        push @out, $line;
	$state = 'DEFAULT';
    }
}

binmode STDOUT, ":crlf";
print STDOUT join "\n", @out;

0;
