#!/usr/bin/perl

use strict;
use warnings;

# takes a triangular dxf mesh and draws lines between all adjacent triangles
# effectively drawing voronoi diagram assuming the triangle mesh is a delaunay
# diagram

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;

my $filename = $ARGV[0] || croak 'DXF file not specified';
my $data = read_DXF ($filename, 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'NEXT';
my $points = ['Doh, AutoCAD numbers nodes starting at 1'];
my $triangles = [];


for my $item (@report)
{
    if ($item->Type eq 'POLYLINE' and $item->Triangular)
    {
        print STDERR "Triangular mesh found\n";
        $state = 'MESH';
    }

    if ($state eq 'MESH' and $item->Type eq 'VERTEX' and $item->Triangle)
    {
        push @{$triangles}, [sort {$a <=> $b} ($item->{a}, $item->{b}, $item->{c})];
    }

    if ($state eq 'MESH' and $item->Type eq 'VERTEX' and $item->Point)
    {
        push @{$points}, $item->Coordinates;
    }

    if ($state eq 'MESH' and $item->Type eq 'SEQEND')
    {
        $state = 'NEXT';
        print STDERR "Points found: ". scalar @{$points} ."\n";
        print STDERR "Triangles found: ". scalar @{$triangles} ."\n";
    }
}

my $links = [];

for my $i (0 .. scalar @{$triangles} - 1)
{
    for my $j (0 .. scalar @{$triangles} - 1)
    {
        next if $i == $j;
        for my $m (0 .. 2)
        {
            for my $n (0 .. 2)
            {
                for my $o (0 .. 2)
                {
                    for my $p (0 .. 2)
                    {
                        next if $m == $o;
                        next if $n == $p;
                        if ($triangles->[$i]->[$m] == $triangles->[$j]->[$n]
                        and $triangles->[$i]->[$o] == $triangles->[$j]->[$p])
                        {
                            push @{$links}, [$i, $j];
                        }
                    }
                }
            }
        }
    }
}

print STDERR "Links found: ". scalar @{$links} ."\n";

open my $DXF, ">", $ARGV[0] . 'voronoi.dxf';
binmode $DXF, ":crlf";

print $DXF dxf (0, 'SECTION');
print $DXF dxf (2, 'ENTITIES');

my $index = 0;

for my $link (@{$links})
{

my $a = $triangles->[$link->[0]];
my $b = $triangles->[$link->[1]];

my $a_cent = [];
my $b_cent = [];

$a_cent->[0] = ($points->[$a->[0]]->[0] + $points->[$a->[1]]->[0] + $points->[$a->[2]]->[0]) / 3;
$a_cent->[1] = ($points->[$a->[0]]->[1] + $points->[$a->[1]]->[1] + $points->[$a->[2]]->[1]) / 3;
$a_cent->[2] = ($points->[$a->[0]]->[2] + $points->[$a->[1]]->[2] + $points->[$a->[2]]->[2]) / 3;

$b_cent->[0] = ($points->[$b->[0]]->[0] + $points->[$b->[1]]->[0] + $points->[$b->[2]]->[0]) / 3;
$b_cent->[1] = ($points->[$b->[0]]->[1] + $points->[$b->[1]]->[1] + $points->[$b->[2]]->[1]) / 3;
$b_cent->[2] = ($points->[$b->[0]]->[2] + $points->[$b->[1]]->[2] + $points->[$b->[2]]->[2]) / 3;

print $DXF dxf (0, 'LINE');
print $DXF dxf (5, $index);
print $DXF dxf (8, 'voronoi');
print $DXF dxf (62, 7);
print $DXF dxf (10, $a_cent->[0]);
print $DXF dxf (20, $a_cent->[1]);
print $DXF dxf (30, $a_cent->[2]);
print $DXF dxf (11, $b_cent->[0]);
print $DXF dxf (21, $b_cent->[1]);
print $DXF dxf (31, $b_cent->[2]);

$index++;

}

print $DXF dxf (0, 'ENDSEC');
print $DXF dxf (0, 'EOF');

close $DXF;

0;

