#!/usr/bin/perl

use strict;
use warnings;
use 5.010;
use File::DXF;

# takes an SDNF file representing a plate structure and outputs DXF 3FACE entities
# Usage: sdnf23dface.pl < IN.SDNF > OUT.DXF

my $state = 'INIT';
my $faces = [];
my $nodes = [];

=cut
"" "S355" 8.000000 4
414 18387.056 21643.03
414 19065.26 21235.527
414 19182.859 21423.124
414 18450.048 21881.031
200004 1 0 0 "plate"
"" "S355" 8.000000 4
 ...
=cut

for my $line (<STDIN>)
{
    $line =~ s/[\r\l\n]+//x;
    next unless $line =~ /[0-9]/x;

    if ($line =~ /^".*" ".+" [0-9.-]+ ([34])/)
    {
        $state = 'TRIANGLE' if $1 == 3;
        $state = 'QUAD' if $1 == 4;
    }
    next if $state eq 'INIT';

    if ($line =~ /^([0-9.-]+) ([0-9.-]+) ([0-9.-]+)$/)
    {
        push @{$nodes}, [$1,$2,$3];
    }

    if ($line =~ /^[0-9]+ [0-9]+ [0-9]+ [0-9]+ ".+"$/)
    {
        push @{$faces}, $nodes if scalar @{$nodes};
        $nodes = [];
    }
}
push @{$faces}, $nodes if scalar @{$nodes};

my $dxf = File::DXF->new;

for my $face (@{$faces})
{
    push (@{$face}, $face->[2]) if scalar @{$face} == 3;
    push @{$dxf->{ENTITIES}}, {type => '3DFACE', _data => [
        [8, 0],
        [62, 0],
        [10, $face->[0]->[0]],
        [20, $face->[0]->[1]],
        [30, $face->[0]->[2]],

        [11, $face->[1]->[0]],
        [21, $face->[1]->[1]],
        [31, $face->[1]->[2]],

        [12, $face->[2]->[0]],
        [22, $face->[2]->[1]],
        [32, $face->[2]->[2]],

        [13, $face->[3]->[0]],
        [23, $face->[3]->[1]],
        [33, $face->[3]->[2]]]};
}

binmode STDOUT, ":crlf";
print STDOUT $dxf->DXF;

0;
