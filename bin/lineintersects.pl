#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw /add_3d scale_3d distance_2d distance_3d normalise_3d magnitude_3d add_2d subtract_2d subtract_3d points_2line line_intersection/;
use Carp;

croak "usage: $0 LINES0.DXF LINES0.DXF POINTS.DXF" unless @ARGV == 3;

# takes two DXF files containing LINE entities and produces a DXF file
# containing POINT entities placed where where lines from the two files appear
# to intersect

my $dxf_0 = File::DXF->new;
   $dxf_0->Process (read_DXF ($ARGV[0], 'CP1252'));
my @LINES_0 = $dxf_0->{ENTITIES}->Report ('LINE');

my $dxf_1 = File::DXF->new;
   $dxf_1->Process (read_DXF ($ARGV[1], 'CP1252'));
my @LINES_1 = $dxf_1->{ENTITIES}->Report ('LINE');

my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

for my $LINE_0 (@LINES_0)
{
    next unless $LINE_0->Length; # ignore zero length lines
    my $a_0 = $LINE_0->Coordinates (0);
    my $b_0 = $LINE_0->Coordinates (1);
    my $line_0 = points_2line ($a_0, $b_0);

    for my $LINE_1 (@LINES_1)
    {
        next unless $LINE_1->Length; # ignore zero length lines
        my $a_1 = $LINE_1->Coordinates (0);
        my $b_1 = $LINE_1->Coordinates (1);
        my $line_1 = points_2line ($a_1, $b_1);

        my $intersection = line_intersection ($line_0, $line_1);
        next unless is_between_2d ($a_0, $b_0, $intersection);
        next unless is_between_2d ($a_1, $b_1, $intersection);

        my $along_0 = distance_2d ($a_0, $intersection) / distance_2d ($a_0, $b_0);
        my $coor_0 = add_3d (scale_3d ($a_0, 1-$along_0), scale_3d ($b_0, $along_0));
        push @{$dxf_out->{ENTITIES}}, {type => 'POINT', _data => [
            [8, 0],
            [10, $coor_0->[0]],
            [20, $coor_0->[1]],
            [30, $coor_0->[2]]]};

        my $along_1 = distance_2d ($a_1, $intersection) / distance_2d ($a_1, $b_1);
        my $coor_1 = add_3d (scale_3d ($a_1, 1-$along_1), scale_3d ($b_1, $along_1));
        push @{$dxf_out->{ENTITIES}}, {type => 'POINT', _data => [
            [8, 1],
            [10, $coor_1->[0]],
            [20, $coor_1->[1]],
            [30, $coor_1->[2]]]};
    }
}

sub is_between_2d
{
    my ($a, $b, $q) = @_;
    my $ab = distance_2d ($a, $b);
    my $aqb = distance_2d ($a, $q) + distance_2d ($q, $b);
    return 0 if ($aqb/$ab > 1.000000001);
    return 1;
}

open my $DXF, '>', $ARGV[2];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;
