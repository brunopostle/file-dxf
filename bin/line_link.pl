#!/usr/bin/perl

use strict;
use warnings;
use 5.010;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw /distance_3d shortest_link/;
use Carp;

# draws shortest perpendicular lines between all pairs of lines in the source file.

croak "usage: $0 LINES.DXF LINKS.DXF" unless @ARGV == 2;

my $dxf_in = File::DXF->new;
   $dxf_in->Process(read_DXF($ARGV[0], 'CP1252'));

my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

my @lines = $dxf_in->{ENTITIES}->Report('LINE');

for my $i (0 .. scalar(@lines)  -2)
{
    my $line_i = $lines[$i];
    next unless $line_i->Length; # ignore zero length lines
    my $P1 = $line_i->Coordinates(0);
    my $P2 = $line_i->Coordinates(1);

    for my $j ($i +1 .. scalar(@lines) -1)
    {
        my $line_j = $lines[$j];
        my $Q1 = $line_j->Coordinates(0);
        my $Q2 = $line_j->Coordinates(1);

        my ($R1, $R2) = shortest_link($P1, $P2, $Q1, $Q2);
        next unless $R1 and $R2;

        # lines can have a thickness attribute, use this to only draw link between 'touching' lines
        if ($line_i->{thickness} and $line_j->{thickness})
        {
            my $distance = distance_3d($R1, $R2);
            next if (abs($line_i->{thickness} - $distance) > 0.001
                 and abs($line_j->{thickness} - $distance) > 0.001
                 and abs((($line_i->{thickness} + $line_j->{thickness}) /2) - $distance) > 0.001);
        }

        push @{$dxf_out->{ENTITIES}}, {type => 'LINE', _data => [
            [8, 'LINK'],
            [10, $R1->[0]],
            [20, $R1->[1]],
            [30, $R1->[2]],
    
            [11, $R2->[0]],
            [21, $R2->[1]],
            [31, $R2->[2]]
             ]};
    }
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;
