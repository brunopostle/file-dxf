#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;
use 5.010;

croak "Converts a series of 3D curves into equivalent non-spline 3dpolylines.  Curves are
defined by AutoCAD '3D Polyline' with 'Cubic', 'None' or 'Quadratic'
'Fit/Smooth'. Polylines can be closed or open.  Relies on interpolated
spline points being saved in the DXF file.

  Usage: $0 input.dxf output.dxf" unless @ARGV > 1;

my $data = read_DXF ($ARGV[0], 'CP1252');

open my $DXF, ">", $ARGV[1];
binmode $DXF, ":crlf";

print $DXF dxf (0, 'SECTION');
print $DXF dxf (2, 'ENTITIES');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my $index = 0;
my @points;
my $patch;
my $open = undef;

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE')
    {
        next unless defined $item->{mode};
        $open = 1 if ($item->{mode} == 8    # straight open 3d polyline
                   or $item->{mode} == 12); # curved open 3d polyline
        $open = 0 if ($item->{mode} == 9    # straight closed 3d polyline
                   or $item->{mode} == 13); # curved closed 3d polyline

        next unless (defined $open);
        $state = 'POINTS';
        @points = ();
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX')
    {
        next unless defined $item->{mode};
        next unless ($item->{mode} == 32 or $item->{mode} == 40);
        push @points, [$item->{x}, $item->{y}, $item->{z}];
    }

    if ($item->Type eq 'SEQEND' and $state eq 'POINTS')
    {
        print $DXF dxf (0, 'POLYLINE');
        print $DXF dxf (8, 0);
        print $DXF dxf (66, 1);
        print $DXF dxf (70, 8) if $open;
        print $DXF dxf (70, 9) unless $open;

        while (@points)
        { 
            my $node = shift @points;
            print $DXF dxf (0, 'VERTEX');
            print $DXF dxf (8, 0);
            print $DXF dxf (10, $node->[0]);
            print $DXF dxf (20, $node->[1]);
            print $DXF dxf (30, $node->[2]);
        }

        print $DXF dxf (0, 'SEQEND');
        print $DXF dxf (8, 0);

        $state = 'NEXT';
    }
}

print $DXF dxf (0, 'ENDSEC');
print $DXF dxf (0, 'EOF');

close $DXF;

exit 0;

