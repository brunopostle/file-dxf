#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use SVG;
use Carp;

my $filename = $ARGV[0] || croak 'DXF file not specified';
my $data = read_DXF ($filename, 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('LINE'));

my $svg = SVG->new (width => 1600,
                  height => 1200);

my $width = 2;
my $colour = 'rgb(0,0,0)';

for my $item (@report)
{
    my $a = $item->Coordinates (0);
    my $b = $item->Coordinates (1);
    $svg->line (x1 => $a->[0], y1 => $a->[1],
                x2 => $b->[0], y2 => $b->[1],
                style => { 'stroke-width' => $width,
                                 'stroke' => $colour });
}

open my $SVGFILE, ">:encoding(UTF-8)", $ARGV[1];
print $SVGFILE $svg->xmlify;
close $SVGFILE;

0;
