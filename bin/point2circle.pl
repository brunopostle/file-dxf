#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;

my $filename = $ARGV[0] || croak 'DXF file not specified';
my $data = read_DXF ($filename, 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my $index = 1;

print STDOUT dxf (0, 'SECTION');
print STDOUT dxf (2, 'ENTITIES');

my @report = $entities->Report (('POINT'));

for my $item (@report)
{
    print STDOUT dxf (0, 'CIRCLE');
    print STDOUT dxf (5, $index);
    print STDOUT dxf (8, 0);
    print STDOUT dxf (10, $item->Coordinates->[0]);
    print STDOUT dxf (20, $item->Coordinates->[1]);
    print STDOUT dxf (30, $item->Coordinates->[2]);
    print STDOUT dxf (40, '6.0');

    $index++;
}

print STDOUT dxf (0, 'ENDSEC');
print STDOUT dxf (0, 'EOF');

0;
