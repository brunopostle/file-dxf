#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::DXF::Mem;
use Carp;

my $filename = $ARGV[0] || croak 'DXF file not specified';
my $data = read_DXF ($filename, 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my $mem;
my $mesh;
my $vertex_index;

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE' and $item->Rectangular)
    {
        $mesh = $item;
        $mem = File::DXF::Mem->new;
        $vertex_index = 0;
        $state = 'MESH';
    }

    if ($state eq 'MESH' and $item->Type eq 'VERTEX')
    {
        my ($i, $j) = $mesh->Index ($vertex_index);
        $mem->{data}->[$i]->[$j] = [@{$item->Coordinates}, 0];
        $vertex_index++;
    }

    if ($state eq 'MESH' and $item->Type eq 'SEQEND')
    {

        if ($mesh->Closed_M)
        {
             for my $j (0 .. $mesh->Size_N - 1)
             {
                  $mem->{data}->[$mesh->Size_M]->[$j] = $mem->{data}->[0]->[$j];
             }
        }
        if ($mesh->Closed_N)
        {
             for my $i (0 .. $mesh->Size_M - 1)
             {
                  $mem->{data}->[$i]->[$mesh->Size_N] = $mem->{data}->[$i]->[0];
             }
 
        }
        if ($mesh->Closed_M and $mesh->Closed_N)
        {
             $mem->{data}->[$mesh->Size_M]->[$mesh->Size_N] = $mem->{data}->[0]->[0];
        }

        $mem->Fixup;
        my $filename = $ARGV[0];
        $filename =~ s/\.dxf$/\.mem/i;
        $mem->Write ($filename);
        $state = 'NEXT';
        exit 0;
    }
}

exit 1;

