#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use 5.010;
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;

croak "Converts a series of 3D curves into a surface mesh.  Curves are
defined by AutoCAD '3D Polyline' with 'Cubic', 'None' or 'Quadratic'
'Fit/Smooth'. Polylines can be closed or open.  Relies on interpolated
spline points being saved in the DXF file.
  Usage: $0 input.dxf output.dxf" unless @ARGV > 1;

my $data = read_DXF ($ARGV[0], 'CP1252');

open my $DXF, ">", $ARGV[1];
binmode $DXF, ":crlf";

print $DXF dxf (0, 'SECTION');
print $DXF dxf (2, 'ENTITIES');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my $vertex_index;
my $index = 0;
my $index_polyline = 0;
my @points;
my $patch;
my $open = undef;

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE')
    {
        say "POLYLINE $index_polyline";
        $vertex_index = 0;
        next unless defined $item->{mode};
        $open = 1 if ($item->{mode} == 8    # straight open 3d polyline
                   or $item->{mode} == 12); # curved open 3d polyline
        $open = 0 if ($item->{mode} == 9    # straight closed 3d polyline
                   or $item->{mode} == 13); # curved closed 3d polyline

        next unless (defined $open);
        $state = 'POINTS';
        @points = ();
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX')
    {
        #say 'VERTEX';
        next unless defined $item->{mode};
        next unless ($item->{mode} == 32 or $item->{mode} == 40);
        push @points, [$item->{x}, $item->{y}, $item->{z}];
        $vertex_index++;
    }

    if ($item->Type eq 'SEQEND' and $state eq 'POINTS')
    {
        say "SEQEND $index_polyline, ". scalar @points;
        while (@points)
        { 
            my $node = shift @points;
            print $DXF dxf (0, 'POINT');
            print $DXF dxf (8, 0);
            print $DXF dxf (10, $node->[0]);
            print $DXF dxf (20, $node->[1]);
            print $DXF dxf (30, $node->[2]);
            push @{$patch->[$index_polyline]}, $node;
        }

        $state = 'NEXT';
        $index_polyline++;
    }
}

print $DXF dxf (0, 'POLYLINE');
print $DXF dxf (8, 0);
print $DXF dxf (66, 1);
print $DXF dxf (70, 17) if $open;
print $DXF dxf (70, 48) unless $open;
print $DXF dxf (71, scalar @{$patch});
print $DXF dxf (72, scalar @{$patch->[0]});

for my $m (0 .. scalar @{$patch} -1)
{
    for my $n (0 .. scalar @{$patch->[0]} -1)
    {
        print $DXF dxf (0, 'VERTEX');
        print $DXF dxf (8, 0);
        print $DXF dxf (10, $patch->[$m]->[$n]->[0]);
        print $DXF dxf (20, $patch->[$m]->[$n]->[1]);
        print $DXF dxf (30, $patch->[$m]->[$n]->[2]);
        print $DXF dxf (70, 64);
    }
}

print $DXF dxf (0, 'SEQEND');
print $DXF dxf (8, 0);

print $DXF dxf (0, 'ENDSEC');
print $DXF dxf (0, 'EOF');

close $DXF;

exit 0;

