#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw /add_3d scale_3d distance_2d points_2line line_intersection/;
use File::DXF::ENTITIES::LINE;
use Carp;

croak "usage: $0 LINES.DXF KNIVES.DXF CUT.DXF 0.15" unless @ARGV == 4;

# takes two DXF files containing LINE entities and produces a DXF file with all
# the LINE entities in the first DXF file split where they intersect LINE
# entities in the second DXF file.

my $dxf_0 = File::DXF->new;
   $dxf_0->Process (read_DXF ($ARGV[0], 'CP1252'));
my @LINES_0 = $dxf_0->{ENTITIES}->Report ('LINE');

my $dxf_1 = File::DXF->new;
   $dxf_1->Process (read_DXF ($ARGV[1], 'CP1252'));
my @LINES_1 = $dxf_1->{ENTITIES}->Report ('LINE');

my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

while (scalar @LINES_0)
{
    my $LINE_0 = shift @LINES_0;
    next unless $LINE_0->Length; # ignore zero length lines
    my $a_0 = $LINE_0->Coordinates (0);
    my $b_0 = $LINE_0->Coordinates (1);
    my $line_0 = points_2line ($a_0, $b_0);

    my $is_cut = 0;
    for my $LINE_1 (@LINES_1)
    {
        next unless $LINE_1->Length; # ignore zero length lines
        my $a_1 = $LINE_1->Coordinates (0);
        my $b_1 = $LINE_1->Coordinates (1);
        next unless abs ($a_1->[2] - $a_0->[2]) < 0.000000001;
        my $line_1 = points_2line ($a_1, $b_1);

        my $intersection = line_intersection ($line_0, $line_1);
        next unless defined $intersection->[0];
        next unless is_between_2d ($a_0, $b_0, $intersection);
        next unless is_between_2d ($a_1, $b_1, $intersection);

        my $along_0 = distance_2d ($a_0, $intersection) / distance_2d ($a_0, $b_0);
        next if $along_0 < 0.000000001;
        next if $along_0 > 0.999999999 ;
        my $coor_0 = add_3d (scale_3d ($a_0, 1-$along_0), scale_3d ($b_0, $along_0));

        my $line_a = File::DXF::ENTITIES::LINE->new;
        my $line_b = File::DXF::ENTITIES::LINE->new;

        $line_a->Coordinates (0, $a_0);
        $line_a->Coordinates (1, $coor_0);

        $line_b->Coordinates (0, $b_0);
        $line_b->Coordinates (1, $coor_0);

        push @LINES_0, $line_a;
        push @LINES_0, $line_b;

        push @{$dxf_out->{ENTITIES}}, {type => 'LINE', _data => [
            [8, 4],
            [10, $coor_0->[0]],
            [20, $coor_0->[1]],
            [30, $coor_0->[2]],
            [11, $coor_0->[0]],
            [21, $coor_0->[1]],
            [31, $coor_0->[2] +$ARGV[3]]]};

        $is_cut = 1;
        last;
    }
    next if $is_cut;
    
    push @{$dxf_out->{ENTITIES}}, {type => 'LINE', _data => [
        [8, 0],
        [10, $a_0->[0]],
        [20, $a_0->[1]],
        [30, $a_0->[2]],
        [11, $b_0->[0]],
        [21, $b_0->[1]],
        [31, $b_0->[2]]]};
}

sub is_between_2d
{
    my ($a, $b, $q) = @_;
    my $ab = distance_2d ($a, $b);
    my $aqb = distance_2d ($a, $q) + distance_2d ($q, $b);
    return 0 if ($aqb/$ab > 1.000000001);
    return 1;
}

open my $DXF, '>', $ARGV[2];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;
