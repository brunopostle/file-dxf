#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util 'read_DXF';
use File::DXF;
use File::DXF::HEADER;
use File::DXF::ENTITIES;
use File::DXF::BLOCKS;
use Carp;

# Merges two or more DXF files into a new DXF file, identically named blocks in
# earlier DXF files will be clobbered by those in later DXF files.
# Expect to lose data such as layers, geometry should be preserved

croak "usage: $0 foo.dxf bar.dxf [...] merged.dxf" unless scalar @ARGV > 1;

my @paths = @ARGV;

my $path_out = pop @paths;
my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;
$dxf_out->{BLOCKS} = File::DXF::BLOCKS->new;

for my $path (@paths)
{
    next unless -e $path;
    my $data = read_DXF ($path, 'CP1252');
    my $dxf_in = File::DXF->new;
    $dxf_in->Process ($data);
    for my $key (keys %{$dxf_in->{BLOCKS}})
    {
        $dxf_out->{BLOCKS}->{$key} = $dxf_in->{BLOCKS}->{$key};
    }
    push @{$dxf_out->{ENTITIES}}, @{$dxf_in->{ENTITIES}};
}

open my $DXF, '>', $path_out;
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;

