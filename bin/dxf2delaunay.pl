#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use Carp;

# takes the x and y coordinates of TEXT objects in a DXF file
# and outputs a list of points suitable for Delaunay triangulation
# with qhull <http://www.qhull.org/>
# Typical qhull command, outputs triangle list to result.txt:
# 
#   qdelaunay Qj s i TO result.txt

my $filename = $ARGV[0] || croak 'DXF file not specified';
my $data = read_DXF ($filename, 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

print "2 # created by $0\n";

my @report = $entities->Report (('TEXT', 'INSERT', 'POINT'));

my $number = scalar @report;
print "$number\n";

for my $item (@report)
{
    print $item->Coordinates->[0] ." ";
    print $item->Coordinates->[1] ."\n";
}

0;
