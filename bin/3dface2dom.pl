#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::Spec;
use YAML;
use Carp;
use 5.010;

croak "Usage: $0 /path/to/3dfaces.dxf

Creates a tree of empty Dom projects from 3DFACE entities in a DXF file.
3DFACE entities need to be four sided and drawn anti-clockwise (i.e. normals up).
Units are assumed to be metres.
Indicate a 'private' boundary with a visible edge, and a 'street' edge with a hidden edge.
Non-horizontal non-planar 3DFACES are rejected.
" unless scalar @ARGV;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my @report = $dxf->{ENTITIES}->Report (('3DFACE'));

my $state = 'INIT';
my $index = 0;

my ($volume,$directories,$name_project) = File::Spec->splitpath ($ARGV[0]);
$name_project =~ s/\.dxf$//i;
mkdir $name_project || croak "can't create folder '$name_project'";

for my $face (@report)
{
    my $nodes = $face->{nodes};
    next unless scalar @{$nodes} == 4; # ignore 3 sided faces
    my @elevations = sort {$a <=> $b} map {$_->[2]} @{$nodes};
    next if abs ($elevations[0] - $elevations[3]) > 0.001;

    my $elevation = $nodes->[0]->[2];

    my @points = map {[$_->[0],$_->[1]]} @{$nodes};

    my $perimeter = ['private', 'private', 'private', 'private'];
    map {$perimeter->[$_] = undef if $face->{hidden}->[$_]} (0 .. 3); 

    my $yaml = { perimeter => {a => $perimeter->[0], b => $perimeter->[1], c => $perimeter->[2], d => $perimeter->[3]},
                 elevation => $elevation, node => [@points],
                 height => 3.0, rotation => 0, type => 'O' };

    my $path_plot = 'plot-'.$index;
    mkdir (File::Spec->catdir ($name_project, $path_plot));
    YAML::DumpFile (File::Spec->catfile ($name_project, $path_plot, 'init.dom'), $yaml);
    say "Created '$name_project/$path_plot/init.dom'";
    $state = 'NEXT';
    $index++;
}

exit 0;

