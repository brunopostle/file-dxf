#!/usr/bin/perl

use strict;
use warnings;
use 5.010;

use lib ('lib');
use File::DXF::Mem;

# COnverts a Patterner MEM mesh into a Collada DAE mesh

for my $path_mem (@ARGV)
{
next unless $path_mem =~ m/\.mem$/i;

my $path_dae = $path_mem;
$path_dae =~ s/\.mem$/.dae/i;

my $mem = File::DXF::Mem->new;
$mem->Read ($path_mem);

my $id = 'Mem';

my $nodes;
my $triangles;

for my $m (1 .. $mem->Mesh_Size_X)
{
    for my $n (1 .. $mem->Mesh_Size_Y)
    {
        my $coor = $mem->{data}->[$m-1]->[$n-1];
        push @{$nodes}, join ' ', $coor->[0]/1000, $coor->[1]/1000, $coor->[2]/1000;
    }
}

my $size_x = $mem->Mesh_Size_X;
my $size_y = $mem->Mesh_Size_Y;

for my $m (0 .. $size_x -2)
{
    for my $n (0 .. $size_y -2)
    {
        push @{$triangles}, join ' ', ($m*$size_y)+$n, ($m*$size_y)+$n+$size_y,
                                      ($m*$size_y)+$n+$size_y+1, ($m*$size_y)+$n+1;
    }
}

open my $DAE, '>', $path_dae;
binmode $DAE, ":crlf";

say $DAE '<?xml version="1.0" encoding="utf-8"?>';
say $DAE '<COLLADA version="1.4.0" xmlns="http://www.collada.org/2005/11/COLLADASchema">';

say $DAE '<asset>';
say $DAE ' <contributor>';
say $DAE '  <author></author>';
say $DAE '  <authoring_tool>File::DXF</authoring_tool>';
say $DAE '  <comments></comments>';
say $DAE '  <copyright></copyright>';
say $DAE '  <source_data></source_data>';
say $DAE ' </contributor>';
say $DAE ' <created>2012-01-03T16:37:29.985000</created>';
say $DAE ' <modified>2012-01-03T16:37:29.985000</modified>';
say $DAE ' <unit meter="1.00" name="meter"/>';
say $DAE ' <up_axis>Z_UP</up_axis>';
say $DAE '</asset>';

say $DAE '<library_geometries>';

say $DAE '
 <geometry id="'.$id.'-Geometry" name="'.$id.'-Geometry">
  <mesh>
   <source id="'.$id.'-Geometry-Position">
    <float_array count="'. (scalar (@{$nodes}) * 3) .'" id="'.$id.'-Geometry-Position-array">
'.
    join (' ', @{$nodes}) .'
    </float_array>
    <technique_common>
     <accessor count="'. scalar (@{$nodes}) .'" source="#'.$id.'-Geometry-Position-array" stride="3">
      <param type="float" name="X"></param>
      <param type="float" name="Y"></param>
      <param type="float" name="Z"></param>
     </accessor>
    </technique_common>
   </source>
   <vertices id="'.$id.'-Geometry-Vertex">
    <input semantic="POSITION" source="#'.$id.'-Geometry-Position"/>
   </vertices>
   <polygons count="'. scalar (@{$triangles}) .'">
    <input offset="0" semantic="VERTEX" source="#'.$id.'-Geometry-Vertex"/><p>'.
    join ('</p><p>', @{$triangles}).'
   </p></polygons>
  </mesh>
 </geometry>';

say $DAE '</library_geometries>';

say $DAE '<library_visual_scenes>';
say $DAE ' <visual_scene id="Scene" name="Scene">';

say $DAE '<node layer="L1" id="'.$id.'" name="'.$id.'">
   <translate sid="translate">0.00 0.00 0.00</translate>
   <rotate sid="rotateZ">0 0 1 0.00000</rotate>
   <rotate sid="rotateY">0 1 0 0.00000</rotate>
   <rotate sid="rotateX">1 0 0 0.00000</rotate>
   <scale sid="scale">1.00000 1.00000 1.00000</scale>
   <instance_geometry url="#'.$id.'-Geometry"/>
  </node>';

say $DAE ' </visual_scene>';
say $DAE '</library_visual_scenes>';
say $DAE '<scene>';
say $DAE ' <instance_visual_scene url="#Scene"/>';
say $DAE '</scene>';
say $DAE '</COLLADA>';

close $DAE;

}

0;
