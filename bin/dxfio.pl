#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util 'read_DXF';
use File::DXF;
use Carp;

my $filename = $ARGV[0] || croak 'DXF file not specified';
my $data = read_DXF ($filename, 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

print STDOUT $dxf->DXF;

0;
