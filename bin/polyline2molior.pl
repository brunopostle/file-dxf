#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use 5.010;
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use YAML;
use Carp;

croak "Converts 2D POLYLINE objects into molior extrusion objects.
  Usage: $0 input.dxf" unless @ARGV == 1;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my @points;
my $increment = 0;

#my $diameter = 0.22;
my $diameter = 0.15;

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE')
    {
        $state = 'POINTS';
        @points = ();
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX')
    {
        push @points, [$item->{x}, $item->{y}, $item->{z}];
    }

    if ($item->Type eq 'SEQEND' and $state eq 'POINTS')
    {
        $diameter = ((int rand(16) +15)/100);
        my $rad = $diameter/2;
        my $dar = 0-$rad;
        my $profile = [[$dar,$dar], [$rad,$dar], [$rad,$rad], [$dar,$rad]];
        my $molior = {type => 'molior-extrusion', profile => [],
            extension => 0.0, elevation => 0.0, closed => 1, cap => [],
            profile => $profile};
        $molior->{height} = $points[0]->[2]; # assume a 2d polyline in xy plane
        $molior->{path} = [map {[$_->[0],$_->[1]]} @points];

        YAML::DumpFile ("_extrusion_$increment.molior", $molior);
        $increment++;

        $state = 'NEXT';
    }
}

exit 0;

