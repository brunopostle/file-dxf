#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::DXF::ENTITIES;
use Carp;
use 5.010;

# Subdivide DXF polyface (triangular) meshes
# All triangles in the mesh(es) are written as four triangular 3DFACE entities
# Quads are assumed to be two triangles, so become eight triangular 3DFACE entities

croak "usage: $0 input.dxf output.dxf" unless @ARGV == 2;
binmode STDOUT, ":unix";

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

my $dxf_out = File::DXF->new;

my @list;

for my $mesh ($dxf->{ENTITIES}->Extract_Meshes)
{
    my $nodes = $mesh->[0];
    my $triangles = $mesh->[1];

    for my $triangle (@{$triangles})
    {
        my @face = split ' ', $triangle;

        my $a = [split ' ', $nodes->[$face[0]]];
        my $b = [split ' ', $nodes->[$face[1]]];
        my $c = [split ' ', $nodes->[$face[2]]];

        #push @list, [$a, $b, $c];
        push @list, [$a, _av($a,$b), _av($a,$c)];
        push @list, [_av($a,$b), $b, _av($b,$c)];
        push @list, [_av($a,$b), _av($b,$c), _av($a,$c)];
        push @list, [_av($b,$c), $c, _av($a,$c)];

        if (scalar @face == 4)
        {
        my $d = [split ' ', $nodes->[$face[3]]];
        #push @list, [$c, $d, $a];
        push @list, [$a, _av($a,$c), _av($a,$d)];
        push @list, [_av($a,$c), $c, _av($c,$d)];
        push @list, [_av($a,$c), _av($c,$d), _av($a,$d)];
        push @list, [_av($c,$d), $d, _av($a,$d)];
        }
    }
}

sub _av
{
    my ($a, $b) = @_;
    return [($a->[0] + $b->[0])/2, ($a->[1] + $b->[1])/2, ($a->[2] + $b->[2])/2];
}

for my $face (@list)
{
    push @{$dxf_out->{ENTITIES}}, {type => '3DFACE', _data => [
        [8, 0],
        [62, 0],
        [10, $face->[0]->[0]],
        [20, $face->[0]->[1]],
        [30, $face->[0]->[2]],

        [11, $face->[1]->[0]],
        [21, $face->[1]->[1]],
        [31, $face->[1]->[2]],

        [12, $face->[2]->[0]],
        [22, $face->[2]->[1]],
        [32, $face->[2]->[2]],

        [13, $face->[2]->[0]],
        [23, $face->[2]->[1]],
        [33, $face->[2]->[2]]]};
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;
