#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;

# Reads a DXF and rotates XYZ coordinates 90 degrees around either the X, Y or Z axis.  Note, this
# transform doesn't make much sense for many ENTITIES types

croak "usage: $0 z input.dxf output.dxf" unless @ARGV == 3;

my $axis = shift;

my $data = read_DXF ($ARGV[0], 'CP1252');

my ($x, $y, $z) = (0.0,0.0,0.0);
my ($x_old, $y_old, $z_old) = (0.0,0.0,0.0);
my $data_out = [];
my $ENTITIES = 0;

while (@{$data})
{
    my ($code, $string) = File::DXF::_next ($data);

    $ENTITIES = 1 if ($code == 2 and $string eq 'ENTITIES');

    # XYZ codes are '10, 20, 30', '11, 21, 31' etc...
    unless ($ENTITIES and $code =~ /^[123][0-9]$/)
    {
        push @{$data_out}, [$code, $string];
        next;
    }

    $x_old = $string if ($code =~ /^1[0-9]$/);
    $y_old = $string if ($code =~ /^2[0-9]$/);
    $z_old = $string if ($code =~ /^3[0-9]$/);

    if ($axis =~ /x/xi)
    {
        $x = $x_old;
        $y = 0-$z_old;
        $z = $y_old;
    } 
    if ($axis =~ /y/xi)
    {
        $x = $z_old;
        $y = $y_old;
        $z = 0-$x_old;
    } 
    if ($axis =~ /z/xi)
    {
        $x = 0-$y_old;
        $y = $x_old;
        $z = $z_old;
    } 

    # a Z code, capture the suffix
    if ($code =~ /^3([0-9])$/)
    {
        push @{$data_out}, ["1$1", $x], ["2$1", $y], ["3$1", $z];
    }
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";

for my $item (@{$data_out})
{
    print $DXF dxf (@{$item});
}

close $DXF;

0;

