#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::DXF::ENTITIES;
use File::DXF::Math::Matrix qw(multiply);
use Carp;

# Reads a DXF and multiplies 3d polyface meshes by a given homography matrix.
# only handles POLYLINE 'polyface meshes'

croak "usage: $0 input.dxf output.dxf" unless @ARGV == 2;

# http://jerome.jouvie.free.fr/opengl-tutorials/Lesson2.php

# identity matrix
my $matrix = [[   1,   0,   0,   0 ],
              [   0,   1,   0,   0 ],
              [   0,   0,   1,   0 ],
              [   0,   0,   0,   1 ]];

=cut
# translation matrix
$matrix = [[   1,   0,   0,   2 ],
           [   0,   1,   0,   3 ],
           [   0,   0,   1,   4 ],
           [   0,   0,   0,   1 ]];

# scale matrix
$matrix = [[   2,   0,   0,   0 ],
           [   0,   3,   0,   0 ],
           [   0,   0,   4,   0 ],
           [   0,   0,   0,   1 ]];

# rotation matrix
$matrix = [[0.87,   0, 0.5,   0 ],
           [   0,   1,   0,   0 ],
           [-0.5,   0,0.87,   0 ],
           [   0,   0,   0,   1 ]];

=cut
# perspective matrix
$matrix = [[   1,   0,   0,   0 ],
           [   0,   1,   0,   0 ],
           [   0,   0,   3,  -20 ],
           [   0,   0,  -1,   0 ]];

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

my $dxf_out = File::DXF->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

for my $mesh ($dxf->{ENTITIES}->Extract_Meshes)
{
    my $polyline = {type => 'POLYLINE', _data => [
        [8, 'mesh'],
        [62, 0],
        [66, 1],
        [10, 0.0],
        [20, 0.0],
        [30, 0.0],
        [70, 64],
        [71, scalar (@{$mesh->[0]})],
        [72, scalar (@{$mesh->[1]})]
    ]};

    push @{$dxf_out->{ENTITIES}}, $polyline;

    # dump nodes
    for my $packed (@{$mesh->[0]})
    {
        my $node = [split ' ', $packed];
        my $point  = [[$node->[0]], [$node->[1]], [$node->[2]], [1.0]];

        my $result = multiply ($matrix, $point);

        my $w = $result->[3]->[0] || 1;
        $node = [$result->[0]->[0] /$w, $result->[1]->[0] /$w, $result->[2]->[0] /$w];

        my $vertex = {type => 'VERTEX', _data => [
            [8, 'mesh'],
            [10, $node->[0]],
            [20, $node->[1]],
            [30, $node->[2]],
            [70, 192]
        ]};

        push @{$dxf_out->{ENTITIES}}, $vertex;
    }

    # dump triangles
    for my $packed (@{$mesh->[1]})
    {
        my $nodes = [split ' ', $packed];

        my $vertex = {type => 'VERTEX', _data => [
            [8, 'mesh'],
            [62, 0],
            [10, 0.0],
            [20, 0.0],
            [30, 0.0],
            [70, 128],
            [71, $nodes->[0] +1],
            [72, $nodes->[1] +1],
            [73, $nodes->[2] +1],
        ]};
        push @{$vertex->{_data}}, [74, $nodes->[3] +1] if defined $nodes->[3];

        push @{$dxf_out->{ENTITIES}}, $vertex;
    }

    push @{$dxf_out->{ENTITIES}}, {type => 'SEQEND', _data => []};
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;
