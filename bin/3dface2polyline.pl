#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::ENTITIES;
use File::DXF::Util qw /dxf read_DXF/;
use Carp;

# Takes DXF 3DFACE entities and joins into a POLYLINE polyface mesh

croak "usage: $0 INPUT.DXF OUTPUT.DXF" unless @ARGV == 2;

my $dxf_in = File::DXF->new;
my $text = read_DXF ($ARGV[0], 'CP1252');
   $dxf_in->Process ($text);

# write the DXF file

my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;
$dxf_out->{BLOCKS} = File::DXF::BLOCKS->new;

# write out original block table as-is
for my $key (keys %{$dxf_in->{BLOCKS}})
{
    $dxf_out->{BLOCKS}->{$key} = $dxf_in->{BLOCKS}->{$key};
}

my @report = $dxf_in->{ENTITIES}->Report ('3DFACE');
print STDERR '3DFACE entities: '. scalar (@report) ."\n";
my $faces = {};

for my $face (@report)
{
    $faces->{$face->{layer}} = [] unless $faces->{$face->{layer}};
    push @{$faces->{$face->{layer}}}, $face;
}

for my $layer (keys %{$faces})
{
    my ($nodes, $faces, $metadata) = File::DXF::ENTITIES::_faces_to_mesh(@{$faces->{$layer}});
    push @{$dxf_out->{ENTITIES}}, $dxf_in->{ENTITIES}->Mesh ($nodes, $faces, $metadata);
}

# write out original entities as-is except 3dfaces
for my $entity (@{$dxf_in->{ENTITIES}})
{
    push @{$dxf_out->{ENTITIES}}, $entity unless $entity->{type} eq '3DFACE';
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;
