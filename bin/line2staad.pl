#!/usr/bin/perl

use strict;
use warnings;
use 5.010;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use Carp;

# Reads DXF LINE elements and generates STAAD geometry.
# Layers are used to apply separate material/section definitions

croak "usage: $0 input.dxf output.std" unless @ARGV == 2;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report ('LINE');

open my $OUT, '>', $ARGV[1];
binmode $OUT, 'crlf';

my $nodes = {};
my $links = [];

for my $item (@report)
{
    my $c0 = $item->Coordinates (0);
    my $c1 = $item->Coordinates (1);
    my $start = [$c0->[0], $c0->[2], 0-$c0->[1]];
    my $end = [$c1->[0], $c1->[2], 0-$c1->[1]];
    my $layer = $item->{layer};

    my $start_txt = join ' ', map {sprintf ("%.4f", $_)} @{$start};
    my $end_txt   = join ' ', map {sprintf ("%.4f", $_)} @{$end};

    $nodes->{$start_txt} = undef;
    $nodes->{$end_txt}   = undef;

    push @{$links}, [$start_txt, $end_txt, $layer];
}

say $OUT 'STAAD SPACE';
say $OUT 'START JOB INFORMATION';
say $OUT 'ENGINEER DATE 11-Dec-14';
say $OUT 'END JOB INFORMATION';

say $OUT 'INPUT WIDTH 79';
say $OUT 'UNIT METER KN';

say $OUT 'JOINT COORDINATES';

my $id = 1;
for my $key (keys %{$nodes})
{
    $nodes->{$key} = $id;
    say $OUT "$id $key; ";

    $id++;
}

say $OUT 'MEMBER INCIDENCES';

$id = 1;
my $section = {};

@{$links} = sort {$a->[2] cmp $b->[2]} @{$links};

for my $link (@{$links})
{
    say $OUT $id .' '. $nodes->{$link->[0]} .' '. $nodes->{$link->[1]} .'; ';
    $section->{$link->[2]}->{start} = $id unless defined $section->{$link->[2]}->{start};
    $section->{$link->[2]}->{end} = $id;
    $id++;
}

say $OUT 'DEFINE MATERIAL START';
say $OUT 'ISOTROPIC STEEL';
say $OUT 'E 2.05e+008';
say $OUT 'POISSON 0.3';
say $OUT 'DENSITY 76.8195';
say $OUT 'ALPHA 1.2e-005';
say $OUT 'DAMP 0.03';
say $OUT 'TYPE STEEL';
say $OUT 'STRENGTH FY 253200 FU 407800 RY 1.5 RT 1.2';
say $OUT 'END DEFINE MATERIAL';

for my $layer (sort keys %{$section})
{
    say $OUT 'MEMBER PROPERTY EUROPEAN';
    say $OUT $section->{$layer}->{start} ." TO ". $section->{$layer}->{end} ." TABLE ST 150x100x12.5RHS";
}

say $OUT 'CONSTANTS';
say $OUT 'MATERIAL STEEL ALL';

say $OUT 'FINISH';

close $OUT;

0;
