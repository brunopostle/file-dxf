#!/usr/bin/perl

use strict;
use warnings;
use File::DXF::Util 'dxf';

# adapted from http://people.bath.ac.uk/abscjkw/ComputerPrograms/C++programs/SunFlower.cpp

my $PI = atan2 (0,-1);
my $lastnode = 10000;
my $noderadius = 10;
my $rmax = 1.8 * $noderadius * sqrt ($lastnode);
my $goldenratio = (1 + sqrt(5)) / 2;
my $anglefactor = 1.0;
my $deltatheta = $anglefactor * $PI / $goldenratio;
my $rootthetamax = sqrt($lastnode * $deltatheta);

print STDOUT dxf (0, 'SECTION');
print STDOUT dxf (2, 'ENTITIES');

for my $clockwise (0 .. 1)
{
    for my $node (0 .. $lastnode)
    {
        my $theta  = $node * $deltatheta;
        my $r = $rmax * sqrt ($theta) / $rootthetamax;
        $r = (0 - $r) if $clockwise;
        my $x = $r * cos ($theta);
        my $y = $r * sin ($theta);
        print STDOUT dxf (0, 'CIRCLE');
        print STDOUT dxf (8, 'SunFlower');
        print STDOUT dxf (62, 3) if $clockwise;
        print STDOUT dxf (62, 6) unless $clockwise;
        print STDOUT dxf (10, $x);
        print STDOUT dxf (20, $y);
        print STDOUT dxf (40, $noderadius);
    }
}

print STDOUT dxf (0, 'ENDSEC');
print STDOUT dxf (0, 'EOF');

0;

