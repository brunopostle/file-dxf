#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use Carp;

my $filename = $ARGV[0] || croak 'DXF file not specified';
my $data = read_DXF ($filename, 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my $dir = $filename .'.ENTITIES';
mkdir $dir;

my $index = 0;

for my $entity (@{$entities})
{
    my $file = "$dir/". (sprintf "%09d", $index) .'-'. $entity->{type} .'.txt';
    open my $FILE, ">", $file or croak "cannot write-open $filename";
    binmode $FILE, ':crlf';
#    print STDERR "$file\n";
    print $FILE pretty_print ($entity->{_data});
    close $FILE;
    $index++;
}


sub pretty_print
{
    my $data = shift;
    my $out;
    for my $item (@{$data})
    {
        $out .= (join ': ', @{$item}) ."\n";
    }
    return $out;
}

0;
