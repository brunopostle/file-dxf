#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use Carp;

# Reads a DXF POLYLINE 'triangle mesh' and generates STAAD geometry.

croak "usage: $0 input.dxf > output.std" unless @ARGV == 1;
binmode STDOUT, ":crlf";

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my $id_node;
my $id_triangle;
my @nodes;
my @triangles;

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE' and $item->Triangular)
    {
        print STDERR "found triangular mesh polyline\n";
        print STDERR "nodes: ". $item->Nodes ."\n";
        print STDERR "triangles: ". $item->Triangles ."\n";
        $state = 'POINTS';
        $id_node = 1;
        $id_triangle = 1;
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Point)
    {
        my $new_x = $item->{x};
        my $new_y = $item->{z};
        my $new_z = 0-$item->{y};
        # STAAD has a maximum line length limit
        $item->{x} = sprintf ("%.9f", $new_x);
        $item->{y} = sprintf ("%.9f", $new_y);
        $item->{z} = sprintf ("%.9f", $new_z);

        push @nodes, join (' ', $id_node, $item->{x}, $item->{y}, $item->{z});
        $id_node++;
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Triangle)
    {
	$item->{a} = 0 - $item->{a} if ($item->{a} < 0);
	$item->{b} = 0 - $item->{b} if ($item->{b} < 0);
	$item->{c} = 0 - $item->{c} if ($item->{c} < 0);

        # some triangles have four sides
        if (defined $item->{d})
        {
	    $item->{d} = 0 - $item->{d} if ($item->{d} < 0);

            push @triangles, join (' ', $id_triangle,
                                        $item->{a}, $item->{b}, $item->{c}, $item->{d});
        }
        else
        {
            push @triangles, join (' ', $id_triangle,
                                        $item->{a}, $item->{b}, $item->{c});
        }

        $id_triangle++;
    }

    if ($item->Type eq 'SEQEND')
    {
        printn ('STAAD SPACE');
        printn ('START JOB INFORMATION');
        printn ('ENGINEER DATE 07-Dec-11');
        printn ('END JOB INFORMATION');
        printn ('INPUT WIDTH 79');
        printn ('UNIT METER KN');
        printn ('JOINT COORDINATES');
        printn (join '; ', @nodes, '');
        printn ('ELEMENT INCIDENCES SHELL');
        printn (join '; ', @triangles, '');
        printn ('FINISH');

        exit 0;
    }
}

sub printn
{
    my $text = shift;
    my @split = $text =~ m/(.{0,77};[[:space:]]*|^[^;]+$)/gx;
    print STDOUT join ("\n", @split) . "\n";
    #print STDOUT $text . "\n";
    return;
}

exit 1;
