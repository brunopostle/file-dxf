#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::DXF::ENTITIES;
use File::DXF::Math qw /PI line_plane_intersection point_is_in_triangle_2/;
use Carp;
use 5.010;

# Reads a DXF polyface and intersects circles to generate tubes

croak "usage: $0 polyface.dxf circles.dxf output.dxf [MAX_CHORD_LENGTH]" unless @ARGV > 2;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf_mesh = File::DXF->new;
   $dxf_mesh->Process($data);

my @meshes = $dxf_mesh->{ENTITIES}->Extract_Meshes;

my $mesh = shift @meshes;

my $nodes = $mesh->[0];
my $triangles = $mesh->[1];
my $minz;
my $maxz;

my $triangle_list= [];

for my $triangle (@{$triangles})
{
    my @face = split ' ', $triangle;

    my @A = split ' ', $nodes->[$face[0]];
    my @B = split ' ', $nodes->[$face[1]];
    my @C = split ' ', $nodes->[$face[2]];

    my ($min, $max);
    for my $point ([@A],[@B],[@C])
    {
        $min->[0] = $point->[0] unless defined $min->[0];
        $min->[1] = $point->[1] unless defined $min->[1];
        $min->[0] = $point->[0] if $min->[0] > $point->[0];
        $min->[1] = $point->[1] if $min->[1] > $point->[1];
        $max->[0] = $point->[0] unless defined $max->[0];
        $max->[1] = $point->[1] unless defined $max->[1];
        $max->[0] = $point->[0] if $max->[0] < $point->[0];
        $max->[1] = $point->[1] if $max->[1] < $point->[1];

        $minz = $point->[2] unless defined $minz;
        $minz = $point->[2] if $minz > $point->[2];
        $maxz = $point->[2] unless defined $maxz;
        $maxz = $point->[2] if $maxz < $point->[2];
    }
    push @{$triangle_list}, [[@A],[@B],[@C],$min,$max];

    if (scalar @face == 4)
    {
        my @D = split ' ', $nodes->[$face[3]];
        my ($min, $max);
        for my $point ([@C],[@D],[@A])
        {
            $min->[0] = $point->[0] unless defined $min->[0];
            $min->[1] = $point->[1] unless defined $min->[1];
            $min->[0] = $point->[0] if $min->[0] > $point->[0];
            $min->[1] = $point->[1] if $min->[1] > $point->[1];
            $max->[0] = $point->[0] unless defined $max->[0];
            $max->[1] = $point->[1] unless defined $max->[1];
            $max->[0] = $point->[0] if $max->[0] < $point->[0];
            $max->[1] = $point->[1] if $max->[1] < $point->[1];
        }
        $minz = $D[2] unless defined $minz;
        $minz = $D[2] if $minz > $D[2];
        $maxz = $D[2] unless defined $maxz;
        $maxz = $D[2] if $maxz < $D[2];

        push @{$triangle_list}, [[@C],[@D],[@A],$min,$max];
    }
}

my $chord_max = $ARGV[3] || 0.75;

my $data_circles = read_DXF ($ARGV[1], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data_circles);
my $entities = $dxf->{ENTITIES};

my @circles = $entities->Report (('CIRCLE', 'ARC'));
my @lines = $entities->Report (('LINE'));

my $dxf_out = File::DXF->new;

$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

open my $CSV, '>', $ARGV[2] .'.csv';
say $CSV join ', ', 'CIRCLE_ID', 'DIAMETER', 'ANGLE_DEG', 'HEIGHT';
open my $CSV2, '>', $ARGV[2] .'.botedges.csv';
say $CSV2 join ', ', 'CIRCLE_ID', 'DIAMETER', 'X', 'Y', 'Z', 'A_X', 'A_Y', 'A_Z', 'B_X', 'B_Y', 'B_Z';
open my $CSV3, '>', $ARGV[2] .'.topedges.csv';
say $CSV3 join ', ', 'CIRCLE_ID', 'DIAMETER', 'X', 'Y', 'Z', 'A_X', 'A_Y', 'A_Z', 'B_X', 'B_Y', 'B_Z';

my $id_circle = 0;
for my $circle (@circles)
{
    say STDERR $id_circle+1 .'/'. scalar @circles;
    $id_circle++;
    my $stacks = [];

    my $segments = 1 + int($circle->Length/$chord_max);
    #$segments = 12 if $segments < 12;
    my $layer = $circle->{layer};
    my $colour = $circle->{colour};

    for my $inc (0 .. $segments)
    {
        my $radian = deg2rad($circle->Start) + ($inc * deg2rad($circle->Angle) / $segments);
        my $degree = rad2deg($radian);
        my $x = ($circle->Radius * cos($radian)) + $circle->Coordinates->[0];
        my $y = ($circle->Radius * sin($radian)) + $circle->Coordinates->[1];

        my $stack = [];
        for my $tri (@{$triangle_list})
        {
            # filter by max/min of triangle
            next unless ($x >= $tri->[3]->[0] and $x <= $tri->[4]->[0]);
            next unless ($y >= $tri->[3]->[1] and $y <= $tri->[4]->[1]);
            # filter fully
            next unless point_is_in_triangle_2($tri->[0], $tri->[1], $tri->[2], [$x, $y]);

            my $z = line_plane_intersection([$x, $y, 0.0], [$x, $y, 1.0],
                                             $tri->[0], $tri->[1], $tri->[2]);

            push @{$stack}, [$x, $y, $z];
        }
        say STDERR "Warning odd number of intersections! $x, $y" if scalar(@{$stack}) %2;

        push @{$stacks}, [sort {$a->[2] <=> $b->[2]} @{$stack}];
        for my $point (@{$stacks->[-1]})
        {
            say $CSV join ', ', $layer, $circle->Diameter(), $degree, $point->[2];
        }
    }

    for my $inc (1 .. $segments)
    {
        my $left = [@{$stacks->[$inc-1]}];
        my $right = [@{$stacks->[$inc]}];
        next unless scalar @{$left};
        next unless scalar @{$right};

        while (scalar @{$left} > scalar @{$right})
        {
            splice @{$left}, _worst($left,$right), 1;
        }
        while (scalar @{$right} > scalar @{$left})
        {
            splice @{$right}, _worst($right,$left), 1;
        }

        while (scalar @{$left})
        {
            my $a = shift @{$left};
            my $b = shift @{$right};
            my $c = shift @{$right};
            my $d = shift @{$left};
            push @{$dxf_out->{ENTITIES}}, _3dface($layer, $colour, $a, $b, $c, $d);
            say $CSV2 join ', ', $layer, $circle->Diameter(), @{$circle->Coordinates()}, @{$a}, @{$b};
            say $CSV3 join ', ', $layer, $circle->Diameter(), @{$circle->Coordinates()}, @{$d}, @{$c};
        }
    }
}

# reduce the triangle list
my @ends;
for my $line (@lines)
{
    my $A = $line->Coordinates(0);
    my $B = $line->Coordinates(1);
    push @ends, $A, $B;
}

my $triangle_list_filtered = [];
for my $tri (@{$triangle_list})
{
    my $interesting = 0;
    for my $end (@ends)
    {
        my $x = $end->[0];
        my $y = $end->[1];

        # filter by max/min of triangle
        next unless ($x >= $tri->[3]->[0] and $x <= $tri->[4]->[0]);
        next unless ($y >= $tri->[3]->[1] and $y <= $tri->[4]->[1]);
        # filter fully
        next unless point_is_in_triangle_2($tri->[0], $tri->[1], $tri->[2], [$x, $y]);
        $interesting = 1;
    }
    if ($interesting)
    {
        push @{$triangle_list_filtered}, $tri;
    }
}

my $id_line = 0;
for my $line (@lines)
{
    $id_line++;
    say STDERR $id_line .'/'. scalar @lines;
    my $A = $line->Coordinates(0);
    my $B = $line->Coordinates(1);
    my $layer = $line->{layer};
    my $colour = $line->{colour};
    my $interval = 0.05;
    my $results = [];
    for my $z (int($minz / $interval) .. int($maxz / $interval))
    {
        $z *= $interval;
        my @A_status = _point_inside_status([$A->[0], $A->[1], $z],
                           $triangle_list_filtered, 0.1, 0.3);
        my @B_status = _point_inside_status([$B->[0], $B->[1], $z],
                           $triangle_list_filtered, 0.1, 0.3);

        if ($A_status[0] and $B_status[0]) # both ends inside manifold
        {
            if (!$A_status[1] and !$B_status[1]) # neither end too close
            {
                if (not ($A_status[2] or $B_status[2])) # not both ends too far
                {
                    push @{$results}, $z;
                }
            }
        }
    }
    my $todo = 1;
    while ($todo)
    {
        $todo = 0;
        for my $index (1 .. scalar(@{$results}) -2)
        {
            if (scalar @{$results} <= 2)
            {
                $todo = 0;
                last;
            }
            elsif (($results->[$index +1] - $results->[$index] <= 0.15)
            and ($results->[$index] - $results->[$index -1] <= 0.15))
            {
                splice (@{$results}, $index, 1);
                $todo = 1;
                last;
            }
        }
    }
    for my $z (@{$results})
    {
        push @{$dxf_out->{ENTITIES}}, _line($layer, $colour, [$A->[0], $A->[1], $z],
                                            [$B->[0], $B->[1], $z]);
    }
}

# point is inside if there are odd number of faces directly above
sub _point_inside_status
{
    my ($P, $triangle_list, $r0, $r1) = @_;
    my $x = $P->[0];
    my $y = $P->[1];

    my $stack = [];
    for my $tri (@{$triangle_list})
    {
        # filter by max/min of triangle
        next unless ($x >= $tri->[3]->[0] and $x <= $tri->[4]->[0]);
        next unless ($y >= $tri->[3]->[1] and $y <= $tri->[4]->[1]);
        # filter fully
        next unless point_is_in_triangle_2($tri->[0], $tri->[1], $tri->[2], [$x, $y]);

        my $z = line_plane_intersection([$x, $y, 0.0], [$x, $y, 1.0],
                                         $tri->[0], $tri->[1], $tri->[2]);

        push @{$stack}, [$x, $y, $z];
    }
    say STDERR "Warning odd number of intersections! $x, $y" if scalar(@{$stack}) %2;

    $stack = [sort {$a->[2] <=> $b->[2]} @{$stack}];
    my $count_above = 0;
    my $too_close = 0;
    my $too_far = 1;
    for my $item (@{$stack})
    {
        $count_above++ if ($item->[2] > $P->[2]);
        my $distance = abs($item->[2] - $P->[2]);
        $too_close = 1 if $distance < $r0;
        $too_far = 0 if $distance < $r1;
    }
    my $inside = 0;
    $inside = 1 if $count_above %2;
    return ($inside, $too_close, $too_far);
}

sub _worst
{
    my $distances = [];
    my ($stack_long, $stack_short) = @_;
    for my $id_long (0 .. scalar @{$stack_long}-1)
    {
        for my $id_short (0 .. scalar @{$stack_short}-1)
        {
            my $distance = abs($stack_long->[$id_long]->[2] - $stack_short->[$id_short]->[2]);
            $distances->[$id_long] = $distance unless defined $distances->[$id_long];
            $distances->[$id_long] = $distance if $distance < $distances->[$id_long];
        }
    }
    my $id_furthest = 0;
    for my $id_long (0 .. scalar @{$stack_long}-1)
    {
        $id_furthest = $id_long if $distances->[$id_furthest] < $distances->[$id_long];
    }
    return $id_furthest;
}

open my $DXF, '>', $ARGV[2];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;
close $CSV;
close $CSV2;
close $CSV3;

sub _3dface
{
    my ($layer, $colour, $a, $b, $c, $d) = @_;
    my @data;
    push @data, [8, $layer];
    push @data, [62, $colour];

    push @data, [10, $a->[0]];
    push @data, [20, $a->[1]];
    push @data, [30, $a->[2]];

    push @data, [11, $b->[0]];
    push @data, [21, $b->[1]];
    push @data, [31, $b->[2]];

    push @data, [12, $c->[0]];
    push @data, [22, $c->[1]];
    push @data, [32, $c->[2]];

    push @data, [13, $d->[0]];
    push @data, [23, $d->[1]];
    push @data, [33, $d->[2]];
    return {type => '3DFACE', _data => [@data]};
}

sub _line
{
    my ($layer, $colour, $a, $b) = @_;
    my @data;
    push @data, [8, $layer];
    push @data, [62, $colour];

    push @data, [10, $a->[0]];
    push @data, [20, $a->[1]];
    push @data, [30, $a->[2]];

    push @data, [11, $b->[0]];
    push @data, [21, $b->[1]];
    push @data, [31, $b->[2]];
    return {type => 'LINE', _data => [@data]};
}

sub deg2rad
{
    my $deg = shift;
    return PI() * ($deg / 180);
}

sub rad2deg
{
    my $rad = shift;
    return 180 * ($rad / PI());
}

0;

