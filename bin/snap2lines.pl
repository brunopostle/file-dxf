#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw /add_3d scale_3d distance_2d points_2line line_intersection perpendicular_distance perpendicular_line/;
use File::DXF::ENTITIES::LINE;
use Carp;

croak "usage: $0 LINES.DXF KNIVES.DXF CUT.DXF" unless @ARGV == 3;

# takes two DXF files containing LINE entities and produces a DXF file with all
# the LINE entities in the first DXF file with the ends snapped to the LINE
# entities in the second DXF file if the perpendicular distance is < 0.001

my $dxf_0 = File::DXF->new;
   $dxf_0->Process (read_DXF ($ARGV[0], 'CP1252'));
my @LINES_0 = $dxf_0->{ENTITIES}->Report ('LINE');

my $dxf_1 = File::DXF->new;
   $dxf_1->Process (read_DXF ($ARGV[1], 'CP1252'));
my @LINES_1 = $dxf_1->{ENTITIES}->Report ('LINE');

my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

while (scalar @LINES_0)
{
    my $LINE_0 = shift @LINES_0;
    next unless $LINE_0->Length; # ignore zero length lines
    my $a_0 = $LINE_0->Coordinates (0);
    my $b_0 = $LINE_0->Coordinates (1);
    my $line_0 = points_2line ($a_0, $b_0);

    my $is_cut = 0;
    for my $LINE_1 (@LINES_1)
    {
        next unless $LINE_1->Length; # ignore zero length lines
        my $a_1 = $LINE_1->Coordinates (0);
        my $b_1 = $LINE_1->Coordinates (1);
        my $line_1 = points_2line ($a_1, $b_1);

        my $a_perpline = perpendicular_line ($line_1, $a_0);
        my $a_intersection = line_intersection ($line_1, $a_perpline);

        if (defined $a_intersection->[0]
            and is_between_2d ($a_1, $b_1, $a_intersection)
            and perpendicular_distance ($line_1, $a_0) < 0.004)
        {
            $a_0 = [@{$a_intersection},0];
        }

        my $b_perpline = perpendicular_line ($line_1, $b_0);
        my $b_intersection = line_intersection ($line_1, $b_perpline);

        if (defined $b_intersection->[0]
            and is_between_2d ($a_1, $b_1, $b_intersection)
            and perpendicular_distance ($line_1, $b_0) < 0.004)
        {
            $b_0 = [@{$b_intersection},0];
        }
        
    }
    
    push @{$dxf_out->{ENTITIES}}, {type => 'LINE', _data => [
        [8, 0],
        [10, $a_0->[0]],
        [20, $a_0->[1]],
        [30, $a_0->[2]],
        [11, $b_0->[0]],
        [21, $b_0->[1]],
        [31, $b_0->[2]]]};
}

sub is_between_2d
{
    my ($a, $b, $q) = @_;
    my $ab = distance_2d ($a, $b);
    my $aqb = distance_2d ($a, $q) + distance_2d ($q, $b);
    return 0 if ($aqb/$ab > 1.000000001);
    return 1;
}

open my $DXF, '>', $ARGV[2];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;
