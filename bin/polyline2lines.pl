#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;

# Reads a DXF POLYLINE 'triangle mesh' and generates simple DXF lines.

croak "usage: $0 input.dxf output.dxf" unless @ARGV == 2;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

open my $OUT, '>', $ARGV[1];
binmode $OUT, ':crlf';

print $OUT dxf (0, 'SECTION');
print $OUT dxf (2, 'ENTITIES');

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my $id_node;
my $id_triangle;
my $nodes;
my @triangles;

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE' and $item->Triangular)
    {
        print STDERR "found triangular mesh polyline\n";
        print STDERR "nodes: ". $item->Nodes ."\n";
        print STDERR "triangles: ". $item->Triangles ."\n";
        $state = 'POINTS';
        $id_node = 1;
        $id_triangle = 1;
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Point)
    {
        $nodes->{$id_node} = [$item->{x}, $item->{y}, $item->{z}];
        $id_node++;
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Triangle)
    {
	$item->{a} = 0 - $item->{a} if ($item->{a} < 0);
	$item->{b} = 0 - $item->{b} if ($item->{b} < 0);
	$item->{c} = 0 - $item->{c} if ($item->{c} < 0);

        # some triangles have four sides
        if (defined $item->{d})
        {
	    $item->{d} = 0 - $item->{d} if ($item->{d} < 0);

            print $OUT _dxfline ($nodes->{$item->{a}}, $nodes->{$item->{b}});
            print $OUT _dxfline ($nodes->{$item->{b}}, $nodes->{$item->{c}});
            print $OUT _dxfline ($nodes->{$item->{c}}, $nodes->{$item->{d}});
            print $OUT _dxfline ($nodes->{$item->{d}}, $nodes->{$item->{a}});
        }
        else
        {
            print $OUT _dxfline ($nodes->{$item->{a}}, $nodes->{$item->{b}});
            print $OUT _dxfline ($nodes->{$item->{b}}, $nodes->{$item->{c}});
            print $OUT _dxfline ($nodes->{$item->{c}}, $nodes->{$item->{a}});
        }

        $id_triangle++;
    }

    if ($item->Type eq 'SEQEND')
    {
        $nodes = {};
        @triangles = ();
    }
}

print $OUT dxf (0, 'ENDSEC');
print $OUT dxf (0, 'EOF');


close $OUT;

exit 0;

sub _dxfline
{
     my ($a, $b) = @_;
     return dxf (0, 'LINE') .
     dxf (8, 0) .
     dxf (10, $a->[0]) .
     dxf (20, $a->[1]) .
     dxf (30, $a->[2]) .
     dxf (11, $b->[0]) .
     dxf (21, $b->[1]) .
     dxf (31, $b->[2]);
}
