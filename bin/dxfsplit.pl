#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util 'read_DXF';
use File::DXF;
use File::DXF::HEADER;
use File::DXF::ENTITIES;
use File::DXF::BLOCKS;
use Carp;

croak "usage: $0 foo.dxf [...]" unless scalar @ARGV > 0;

# Takes one or more DXF files, extracts the block entities and writes each as a
# separate DXF file. Filenames are block names with .dxf appended

my @paths = @ARGV;

for my $path (@paths)
{
    my $data = read_DXF ($path, 'CP1252');
    my $dxf_in = File::DXF->new;
    $dxf_in->Process ($data);
    for my $key (keys %{$dxf_in->{BLOCKS}})
    {
        my $dxf_out = File::DXF->new;
        $dxf_out->{HEADER} = File::DXF::HEADER->new;
        $dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

        $dxf_out->{ENTITIES}->Process ($dxf_in->{BLOCKS}->{$key});

        my $path_out = $key .'.dxf';
        open my $DXF, '>', $path_out;
        binmode $DXF, ":crlf";
        print $DXF $dxf_out->DXF;
        close $DXF;
    }
}

0;

