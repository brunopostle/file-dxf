#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use 5.010;
use File::DXF::Util qw /dxf/;
use File::DXF;
use Carp;

croak "plots a catastrophe cusp
  Usage: $0 output.dxf" unless @ARGV == 1;

open my $DXF, ">", $ARGV[0];
binmode $DXF, ":crlf";

my $patch = [];

for my $x (-200 .. 200)
{
    for my $u (-200 .. 200)
    {
        my $x_r = $x/100;
        my $u_r = $u/100;
        my $v = (-4*$x_r*$x_r*$x_r) - (2*$u_r*$x_r);
        $patch->[$x +200]->[$u +200] = [$x, $u, $v*100];
        say $v;
    }
}

print $DXF dxf (0, 'SECTION');
print $DXF dxf (2, 'ENTITIES');

print $DXF dxf (0, 'POLYLINE');
print $DXF dxf (8, 0);
print $DXF dxf (66, 1);
print $DXF dxf (70, 16);
print $DXF dxf (71, scalar @{$patch});
print $DXF dxf (72, scalar @{$patch->[0]});

for my $m (0 .. scalar @{$patch} -1)
{
    for my $n (0 .. scalar @{$patch->[0]} -1)
    {
        print $DXF dxf (0, 'VERTEX');
        print $DXF dxf (8, 0);
        print $DXF dxf (10, $patch->[$m]->[$n]->[0]);
        print $DXF dxf (20, $patch->[$m]->[$n]->[1]);
        print $DXF dxf (30, $patch->[$m]->[$n]->[2]);
        print $DXF dxf (70, 64);
    }
}

print $DXF dxf (0, 'SEQEND');
print $DXF dxf (8, 0);

print $DXF dxf (0, 'ENDSEC');
print $DXF dxf (0, 'EOF');

close $DXF;

exit 0;

