#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use Carp;
use 5.010;

croak "Usage: $0 IN.DXF OUT.DXF"
    unless (scalar @ARGV == 2);

my @nodes;
my @triangles;
my $node_counter = 0;

my $dxf_in = File::DXF->new;
$dxf_in->Process (read_DXF ($ARGV[0], 'CP1252'));

for my $mesh ($dxf_in->{ENTITIES}->Extract_Meshes)
{
    my @nodes_in = map {[split / /, $_]} @{$mesh->[0]};
    my @triangles_in = map {[split / /, $_]} @{$mesh->[1]};

    push @nodes, @nodes_in;
    push @triangles, map {[map {$_ + $node_counter} @{$_}]} @triangles_in;
    $node_counter = scalar @nodes;
}

# write DXF

my $dxf_out = File::DXF->new;

# mesh header
my $polyline = {type => 'POLYLINE', _data => [
    [8, 'mesh'],
    [62, 0],
    [66, 1],
    [10, 0.0],
    [20, 0.0],
    [30, 0.0],
    [70, 64],
    [71, scalar (@nodes)],
    [72, scalar (@triangles)]
]};

push @{$dxf_out->{ENTITIES}}, $polyline;

# mesh points
for my $node (@nodes)
{
    my $vertex = {type => 'VERTEX', _data => [
        [8, 'mesh'],
        [10, $node->[0]],
        [20, $node->[1]],
        [30, $node->[2]],
        [70, 192]
    ]};

    push @{$dxf_out->{ENTITIES}}, $vertex;
}

# mesh triangles
for my $triangle (@triangles)
{
    my $vertex = {type => 'VERTEX', _data => [
        [8, 'mesh'],
        [62, 0],
        [10, 0.0],
        [20, 0.0],
        [30, 0.0],
        [70, 128],
        [71, $triangle->[0] +1],
        [72, $triangle->[1] +1],
        [73, $triangle->[2] +1],
    ]};

    push @{$vertex->{_data}}, [74, $triangle->[3] +1] if defined $triangle->[3];

    push @{$dxf_out->{ENTITIES}}, $vertex;
}

# mesh footer
push @{$dxf_out->{ENTITIES}}, {type => 'SEQEND', _data => []};

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;

