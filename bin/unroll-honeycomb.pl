#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw (distance_3d point_is_in_triangle angle_triangle PI);
use Math::Trig;
use Carp;
use 5.010;

croak "Usage: $0 IN.DXF OUT.DXF"
    unless (scalar @ARGV == 2);

# unrolls a mesh, but assumes that all triangles are equilateral, useful for
# extracting just the topology rather than the proportions.

my $elevation = 0.0;

my $dxf_in = File::DXF->new;
my $dxf_out = File::DXF->new;

$dxf_in->Process (read_DXF ($ARGV[0], 'CP1252'));
my $entities = $dxf_in->{ENTITIES};

my @mesh = $entities->Extract_Meshes;

for my $mesh (@mesh)
{

my @points_2d = ();
my @labels_2d = ();

my $nodes_3d = [map {[split / /, $_]} @{$mesh->[0]}];
my $triangles_3d = [map {[split / /, $_]} @{$mesh->[1]}];
my $triangles_3d_orig = [map {[split / /, $_]} @{$mesh->[1]}];
my $mesh_colour = $mesh->[2]->{colour};
my $mesh_layer = $mesh->[2]->{layer};

my $temp;
for my $t (@{$triangles_3d})
{
    push @{$temp}, $t if scalar @{$t} == 3;
    push @{$temp}, [$t->[0], $t->[1], $t->[2]], [$t->[2], $t->[3], $t->[0]] if scalar @{$t} ==4;
}
$triangles_3d = $temp;

# pick any two adjacent points

my $nodes = $triangles_3d->[0];

my $a_3d = $nodes_3d->[$nodes->[0]];
my $b_3d = $nodes_3d->[$nodes->[1]];

# draw first edge on y-axis as a seed

my $nodes_2d;
$nodes_2d->[$nodes->[0]] = [0.0, 0.0, 0.0];
#$nodes_2d->[$nodes->[1]] = [0.0, distance_3d ($a_3d, $b_3d), 0.0];
$nodes_2d->[$nodes->[1]] = [0.0, 1.0, 0.0];

# keep going until all nodes have been flattened

my $dirty = 1;
while ($dirty == 1)
{
    $dirty = 0;

    # iterate through all the triangles

    for my $triangle (0 .. scalar @{$triangles_3d} -1)
    {
        # get three node ids for triangle

        my $nodes = $triangles_3d->[$triangle];

        # we can only flatten this triangle if two nodes already flat

        my $done = 0;
        $done++ if $nodes_2d->[$nodes->[0]];
        $done++ if $nodes_2d->[$nodes->[1]];
        $done++ if $nodes_2d->[$nodes->[2]];
        next unless $done == 2;

        # figure out which node needs flattening

        my ($a_id, $b_id, $c_id);
        if (!$nodes_2d->[$nodes->[0]])
        {
            $a_id = $nodes->[1];
            $b_id = $nodes->[2];
            $c_id = $nodes->[0];
        }
        elsif (!$nodes_2d->[$nodes->[1]])
        {
            $a_id = $nodes->[2];
            $b_id = $nodes->[0];
            $c_id = $nodes->[1];
        }
        elsif (!$nodes_2d->[$nodes->[2]])
        {
            $a_id = $nodes->[0];
            $b_id = $nodes->[1];
            $c_id = $nodes->[2];
        }

        my $a_3d = $nodes_3d->[$a_id];
        my $b_3d = $nodes_3d->[$b_id];
        my $c_3d = $nodes_3d->[$c_id];

        my $a_2d = $nodes_2d->[$a_id];
        my $b_2d = $nodes_2d->[$b_id];

        my $c_2d = flatten_triangle ($a_3d, $b_3d, $c_3d, $a_2d, $b_2d);

        $nodes_2d->[$c_id] = $c_2d;
        $dirty = 1;
    }
}

for my $point_2d (@points_2d)
{
    push @{$dxf_out->{ENTITIES}},
        {type => 'POINT',
        _data => [[8, 0], [10, $point_2d->[0]], [20, $point_2d->[1]], [30, $elevation]]};
}

for my $label_2d (@labels_2d)
{
    my ($point_2d, $label, $colour) = @{$label_2d};
    push @{$dxf_out->{ENTITIES}},
        {type => 'TEXT',
        _data => [[8, 0], [1, $label], [10, $point_2d->[0]], [20, $point_2d->[1]], [30, $elevation], [40, 0.1], [62, $colour]]};
}

for my $node_id (0 .. scalar @{$nodes_2d} -1)
{
    # unconnected nodes won't have got remapped, put them at the origin
    $nodes_2d->[$node_id] = [0,0] unless defined $nodes_2d->[$node_id]->[0];
    $nodes_2d->[$node_id]->[2] = $elevation;
}

my @faces = map {{node_id => $_, colour => $mesh_colour}} @{$triangles_3d_orig};

push @{$dxf_out->{ENTITIES}},
    $dxf_in->{ENTITIES}->Mesh ($nodes_2d, [@faces], {layer => $mesh_layer, colour => $mesh_colour});

$elevation++;

}

open my $DXF ,">", $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

sub flatten_triangle
{
    my ($a_3d, $b_3d, $c_3d, $a_2d, $b_2d) = @_;

    # define everything needed for unplaced point

    my $distance_A = 1.0;
    my $distance_B = 1.0;
    my $distance_C = 1.0;

    print STDERR join ' ', 'Distances ABC:', $distance_A, $distance_B, $distance_C, "\n"
        if $ENV{DEBUG};

    # find angle_a from a_dis, b_dis and c_dis
    # this is relative to the base line, not y=constant

    #my $angle_a = _angle ($distance_A, $distance_B, $distance_C);
    my $angle_a = angle_triangle ($distance_A, $distance_B, $distance_C);

    print STDERR join ' ', 'Angle A:', $angle_a, "\n" if $ENV{DEBUG};

    my $adjacent = ($b_2d->[0] - $a_2d->[0]) || 0.00000000001;

    my $base_angle = atan (($b_2d->[1] - $a_2d->[1]) / $adjacent);

    # correct the arctangent

    $base_angle += PI() if ($b_2d->[0] < $a_2d->[0]);

    # find the angle to c relative to y=constant
    my $new_angle = $angle_a + $base_angle;

    my $c_2d = undef;

    $c_2d->[0] = $a_2d->[0] + ($distance_B * cos ($new_angle));
    $c_2d->[1] = $a_2d->[1] + ($distance_B * sin ($new_angle));
    $c_2d->[2] = 0.0;

    return $c_2d;
}

0;

