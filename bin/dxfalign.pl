#!/usr/bin/perl

use strict;
use warnings;

# takes a dxf file, tries to locate coordinate references
# and moves them to the nearest integer unit. i.e this
# coordinate: 2.123, -1.1, 23.90 becomes 2, -1, 24

my $dxf = shift;
my @out;

open my $DXF, '<', $dxf;
my @dxf = (<$DXF>);
close $DXF;

my $state = 'NORM';

for my $line (@dxf)
{
    chomp $line;
    if ($line =~ /^([0-9]+)\.[0-9]+/ and $state eq 'COOR')
    {
        push @out, int ($1 + 0.5);
    }
    elsif ($line =~ /^(-[0-9]+)\.[0-9]+/ and $state eq 'COOR')
    {
        push @out, int ($1 - 0.5); 
    }
    elsif ($line =~ /^ +(10|20|30|11|21|31)/)
    {
        push @out, $line; 
	$state = 'COOR';
    }
    else
    {
        push @out, $line;
	$state = 'NORM';
    }
}

print join "\n", @out;

0;
