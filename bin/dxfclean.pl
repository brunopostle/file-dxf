#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;

# cleans some floating-point -> decimal artefacts

croak "usage: $0 drawing1.dxf [drawing2.dxf ...]" unless scalar @ARGV;

for my $path_dxf (@ARGV)
{
    my $data = read_DXF ($path_dxf, 'CP1252');
    my $data_out;
    my $ENTITIES;

    while (@{$data})
    {
        my ($code, $string) = File::DXF::_next ($data);

        $ENTITIES = 1 if ($code == 2 and $string eq 'ENTITIES');

        # XYZ codes are '10, 20, 30', '11, 21, 31' etc...
        unless ($ENTITIES and $code =~ /^[123][0-9]$/)
        {
            push @{$data_out}, [$code, $string];
            next;
        }

        my $num = $string;
        $num =~ s/0000*[0-9]+$//;

        $num =~ s/09999*[0-9]+$/1/;
        $num =~ s/19999*[0-9]+$/2/;
        $num =~ s/29999*[0-9]+$/3/;
        $num =~ s/39999*[0-9]+$/4/;
        $num =~ s/49999*[0-9]+$/5/;
        $num =~ s/59999*[0-9]+$/6/;
        $num =~ s/69999*[0-9]+$/7/;
        $num =~ s/79999*[0-9]+$/8/;
        $num =~ s/89999*[0-9]+$/9/;
        $num =~ s/^[1-9]\.[0-9]+e-[0-9][0-9]?$/0.0/;

        push @{$data_out}, [$code, $num];
    }

    open my $DXF, '>', $path_dxf;
    binmode $DXF, ":crlf";

    for my $item (@{$data_out})
    {
        print $DXF dxf (@{$item});
    }

    close $DXF;
}

0;

