#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::DXF::Csv;
use Carp;

my $filename = $ARGV[0] || croak 'DXF file not specified';
my $data = read_DXF ($filename, 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

{
    my @report = $entities->Report (('CIRCLE'));

    my $csv = File::DXF::Csv->new;
    $csv->Push ('X', 'Y', 'Z', 'Layer', 'Radius', 'Diameter', 'Circumference', 'Area');

    for my $item (@report)
    {
        $csv->Push (@{$item->Coordinates},
                      $item->Layer,
                      $item->Radius,
                      $item->Diameter,
                      $item->Circumference,
                      $item->Area);
    }

    $csv->Write ("$filename.CIRCLE.csv");
}

{
    my @report = $entities->Report (('ARC'));

    my $csv = File::DXF::Csv->new;
    $csv->Push ('X', 'Y', 'Z', 'Layer', 'Radius');

    for my $item (@report)
    {
        $csv->Push (@{$item->Coordinates},
                      $item->Layer,
                      $item->Radius)
    }

    $csv->Write ("$filename.ARC.csv");
}

{
    my @report = $entities->Report (('POINT'));

    my $csv = File::DXF::Csv->new;
    $csv->Push ('X', 'Y', 'Z', 'Layer');

    for my $item (@report)
    {
        $csv->Push (@{$item->Coordinates},
                      $item->Layer);
    }

    $csv->Write ("$filename.POINT.csv");
}

{
    my @report = $entities->Report (('TEXT', 'MTEXT'));

    my $csv = File::DXF::Csv->new;
    $csv->Push ('X', 'Y', 'Z', 'Layer', 'Content', 'Height', 'Rotation');

    for my $item (@report)
    {
        $csv->Push (@{$item->Coordinates},
                      $item->Content,
                      $item->Layer,
                      $item->Height,
                      $item->Rotation);
    }

    $csv->Write ("$filename.TEXT.csv");
}

0;
