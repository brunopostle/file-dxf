#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;

croak "usage: $0 input.dxf output.dxf" unless @ARGV == 2;

my $data = read_DXF ($ARGV[0], 'CP1252');

my ($x, $y, $z) = (0.0,0.0,0.0);
my $ENTITIES = 0;

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";

print $DXF dxf (0, 'SECTION');
print $DXF dxf (2, 'ENTITIES');

while (@{$data})
{
    my ($code, $string) = File::DXF::_next ($data);

    $ENTITIES = 1 if ($code == 2 and $string eq 'ENTITIES');

    # XYZ codes are '10, 20, 30', '11, 21, 31' etc...
    next unless ($ENTITIES and $code =~ /^[123][0-9]$/);

    $x = $string if ($code =~ /^1[0-9]$/);
    $y = $string if ($code =~ /^2[0-9]$/);
    $z = $string if ($code =~ /^3[0-9]$/);

    # a Z code, capture the suffix
    if ($code =~ /^3([0-9])$/)
    {
        print $DXF dxf (0, 'POINT');   # Entity type
        print $DXF dxf (8, 'Points');  # Layer name
        print $DXF dxf (10, $x); # X coordinate
        print $DXF dxf (20, $y); # Y coordinate
        print $DXF dxf (30, $z); # Z coordinate
    }
}

print $DXF dxf (0, 'ENDSEC');
print $DXF dxf (0, 'EOF');

close $DXF;

0;

