#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /dxf read_DXF/;
use Data::Dumper;
use Carp;

# Takes vertical DXF 3DFACE entities and writes equivalent lines with thickness
# The 2D direction of the ,line is taken from the two lowest points in the 3DFACE
# The elevation is taken from the lowest point in the 3DFACE
# The thickness is taken from the highest point in the 3D face

croak "usage: $0 INPUT.DXF OUTPUT.DXF" unless @ARGV == 2;

my $dxf_in = File::DXF->new;
my $text = read_DXF ($ARGV[0], 'CP1252');
   $dxf_in->Process ($text);

# write the DXF file

my $dxf_out = File::DXF->new;
$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

my @faces = $dxf_in->{ENTITIES}->Report ('3DFACE');

for my $face (@faces)
{
    my $nodes = [sort {$a->[2] <=> $b->[2]} @{$face->{nodes}}];

    push @{$dxf_out->{ENTITIES}}, {type => 'LINE', _data => [
        [8, 0],
        [10, $nodes->[0]->[0]],
        [20, $nodes->[0]->[1]],
        [30, $nodes->[0]->[2]],

        [11, $nodes->[1]->[0]],
        [21, $nodes->[1]->[1]],
        [31, $nodes->[0]->[2]],
        [39, $nodes->[3]->[2] - $nodes->[0]->[2]] ]};
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;

0;
