#!/usr/bin/perl
use strict;
use warnings;
use lib ('lib', '../lib');
use Math::Trig;
use File::Spec;
use File::DXF;
use File::DXF::Math qw/add_2d subtract_2d distance_3d normalise_2d scale_2d/;
use File::DXF::Util qw /read_DXF/;
use Carp;
use 5.010;

croak "usage: $0 INPUT.DXF OUTPUT.CSV" unless @ARGV == 2;

my $dxf_in = File::DXF->new;
my $text = read_DXF ($ARGV[0], 'CP1252');
   $dxf_in->Process ($text);

my @lines = $dxf_in->{ENTITIES}->Report ('LINE');

open my $CSV, '>', $ARGV[1];
binmode $CSV, ':crlf';
say $CSV 'Ref, Elevation, Length';

for my $line (@lines)
{
    next unless $line->Length; # ignore zero length lines
    my $start = $line->Coordinates (0);
    my $end = $line->Coordinates (1);
    my $layer = $line->{layer};
    my $dz = abs ($start->[2] - $end->[2]);
    next unless $dz < 0.001;
    say $CSV join ', ', $layer, $start->[2], distance_3d ($start, $end);
}

close $CSV;

0;
