#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use 5.010;
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;

croak "Converts POLYLINE objects into LINE objects.
  Usage: $0 input.dxf output.dxf" unless @ARGV > 1;

my $data = read_DXF ($ARGV[0], 'CP1252');

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";

print $DXF dxf (0, 'SECTION');
print $DXF dxf (2, 'ENTITIES');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my @points;

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE')
    {
        $state = 'POINTS';
        @points = ();
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX')
    {
        push @points, [$item->{x}, $item->{y}, $item->{z}];
    }

    if ($item->Type eq 'SEQEND' and $state eq 'POINTS')
    {
        for my $id (0 .. scalar (@points) -2)
        { 
            my $node_0 = $points[$id];
            my $node_1 = $points[$id+1];
            print $DXF dxf (0, 'LINE');
            print $DXF dxf (8, 0);
            print $DXF dxf (10, $node_0->[0]);
            print $DXF dxf (20, $node_0->[1]);
            print $DXF dxf (30, $node_0->[2]);
            print $DXF dxf (11, $node_1->[0]);
            print $DXF dxf (21, $node_1->[1]);
            print $DXF dxf (31, $node_1->[2]);
        }

        $state = 'NEXT';
    }
}

print $DXF dxf (0, 'ENDSEC');
print $DXF dxf (0, 'EOF');

close $DXF;

exit 0;

