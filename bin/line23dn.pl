#!/usr/bin/perl

use strict;
use warnings;

use lib 'lib';
use File::DXF;
use File::DXF::Util qw /read_DXF/;
use Data::Dumper;
use 5.010;
use Carp;

croak "usage: $0 INPUT.DXF OUTPUT.3dn [< members.csv]" unless @ARGV == 2;

# Convert a DXF file containing LINE elements into a simple '3D+ Neutral file' (.3dn)
# suitable for importing into 'Fastrak Building Designer'.
# Vertical lines are converted to 'columns'.
# All other lines are converted to 'beams'.
# Units are assumed to be millimetres.
# All attributes other than coordinates are garbage defaults and will need to be checked/fixed.

# supply a CSV file on STDIN:
#layername, section definition ($ indicates a custom section defined in USER DEFINED SECTIONS)
#FSB01,00000;$PB 600x400/80x640.56
#R5B04,00606;RSC(P) 300x100x46
#HMB06,00607;RHS 260x140x12.5
#SLB01,00608;SHS 100x100x10.0
#GFB06,00613;UEA 100x100x10
#VB01,00617;Flat 8x100
#GFB01,00694;UKB 203x102x23
#GFB02,00695;UKC 203x203x46

my $dxf_in = File::DXF->new;
my $text = read_DXF ($ARGV[0], 'CP1252');
   $dxf_in->Process ($text);

my @lines = $dxf_in->{ENTITIES}->Report ('LINE');

my $nodes = [];
my $beams = [];
my $columns = [];
my $grids = [];

my $members = {0 => '00000;$FBD_AUTODESIGN'};
for (<STDIN>)
{
    $_ =~ s/(\r|\n|\l)//g;
    my ($key, $value) = split ',', $_;
    $value =~ s/^ +//;
    $members->{$key} = $value;
}
my @layers = map {"  $_ 2"} keys %{$members};

for my $line (@lines)
{
    next unless $line->Length; # ignore zero length lines
    my $a = $line->Coordinates (0);
    my $b = $line->Coordinates (1);

    my $id_a = scalar @{$nodes};
    my $id_b = $id_a +1;
    push @{$nodes}, $a, $b;

    if (abs($a->[0] - $b->[0]) < 0.1 and abs($a->[1] - $b->[1]) < 0.1) # line is vertical, create a column
    {
        push @{$columns}, [$id_a +1, $id_b +1, $line->{layer}];
    }
    else # line is horizontal or inclined, create a beam
    {
         push @{$beams}, [$id_a +1, $id_b +1, $line->{layer}];
    }
}

for my $lwpolyline ($dxf_in->{ENTITIES}->Report('LWPOLYLINE'))
{
	#for (@{$lwpolyline->{nodes}})
    for (0 .. @{$lwpolyline->{nodes}} -1)
    {
        my $id_a = scalar @{$nodes};
        my $id_b = $id_a +1;
        push @{$nodes}, [@{$lwpolyline->{nodes}->[$_]}, $lwpolyline->{z}];
	push @{$grids}, [$id_a +1, $id_b +1] unless $_ == scalar @{$lwpolyline->{nodes}} -1;
    }
}

open my $OUT, '>', $ARGV[1];

#  0    1    2     3     4    5     6     7     8
# ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)
my @time = localtime;
my $year = $time[5] + 1900; 

print $OUT "VERSION
  7.10 3D+ Neutral File
MODEL DESCRIPTION
  NAME =
  DESC =
  CLIENT =
  CONTRACT NO =imported_3dn
  ISSUE NO =0
  LOCATION =
  DESIGNER =
  MODEL UNIT =1
  DISPLAY FORMAT =1111
  DATE =$time[3]\-$time[4]\-$year
  TIME =$time[2]\:$time[1]\:$time[0]
  COUNTRY =0
USER DEFINED SECTIONS
*Section name,profile=ub,par1=100,...
  \"PB 600x400/80x640.56\",profile=pg,,B1=400,B2=400,D=600,T1=80,T2=80,t=40
  \"PB 450x200/35x169.56\",profile=pg,,B1=200,B2=200,D=450,T1=35,T2=35,t=20
  \"PB 600x300/30x226.08\",profile=pg,,B1=300,B2=300,D=600,T1=30,T2=30,t=20
  \"FBD_AUTODESIGN\",profile=ub,,B=60,D=100,T=5,r=5,t=5
LAYERS
* Name Colour
  Columns 1
  Beams 3
  FBD_DESIGN 4\n".
join("\n", @layers)."\n".
"JOINT COORDINATES
* ID    X    Y    Z\n";

my $id = 1;
for my $node (@{$nodes})
{
    print $OUT "  $id $node->[0] $node->[1] $node->[2]\n";
    #  1 9852.26363798 17518.83266067 0.00000000
    $id++;
}

print $OUT "GRID LINES\n" if scalar (@{$grids});
my $id_grid = 1;
for my $grid (@{$grids})
{
    print $OUT join ' ', $id_grid, $grid->[0], $grid->[1], 0, "2000;2000;1000\n";
    $id_grid++;
}

$id = 1;

for my $beam (@{$beams})
{
my $uuid = _uuid();
my $layer = $beam->[2];
my $section = $members->{$layer} ||'00000;$FBD_AUTODESIGN';
print $OUT "LINEAR MEMBER
  MID = $id
  UUID = $uuid
  SIZE = $section
  GRADE = S355
  DISPLAY = 1023
  TOSIDE = 12
  MROT = 0.0000
  XYOFF = -0.000;0.000
  ADXYZ = 0.000;0.000;0.000
  BDXYZ = 0.000;0.000;0.000
  START/END = $beam->[0] $beam->[1]
  USERATT = FBD_TYPE,PART_MARK,SEL_GROUP,PART_NAME;SIMPLE_BEAM,SB 2/1/3-2/1/4,Beam Attr 1,BEAM
  CLSGRPFLAGS = beam;;111100
  LAYER = $layer
  WEIGHT% = 7.500
  ELEGEN = 0;1;000011;000011;0.0;\n";
$id++;
}

for my $column (@{$columns})
{
my $uuid = _uuid();
my $layer = $column->[2];
my $section = $members->{$layer} ||'00000;$FBD_AUTODESIGN';
print $OUT "LINEAR MEMBER
  MID = $id
  UUID = $uuid
  SIZE = $section
  GRADE = S355
  DISPLAY = 1023
  TOSIDE = 22
  MROT = 0.0000
  XYOFF = 0.000;0.000
  ADXYZ = 0.000;0.000;0.000
  BDXYZ = 0.000;0.000;0.000
  START/END = $column->[0] $column->[1]
  USERATT = FBD_TYPE,PART_MARK,SEL_GROUP,PART_NAME;SIMPLE_COLUMN,SSC 2/3,Column Attr 1,COLUMN
  CLSGRPFLAGS = column;;111100
  LAYER = $layer
  WEIGHT% = 7.500
  ELEGEN = 0;1;000000;000000;0.0;\n";
$id++;
}

close $OUT;

sub _uuid
{
    #UUID = BF625F2E-D928-47A5-ADA3-570070B7DE6A
    join '-',
    sprintf("%08d", rand(99999999)),
    sprintf("%04d", rand(9999)),
    sprintf("%04d", rand(9999)),
    sprintf("%04d", rand(9999)),
    sprintf("%012d", rand(999999999999));
}

0;
