#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw /scale_3d add_3d area_3d cog_3d green_theorem average_3d/;
use File::DXF;
use Carp;
use 5.010;

# Reads a DXF POLYLINE 'triangle mesh' and calculates the surface area, volume,
# CoG of surface and CoG of volume.  Volume calculations are wrong unless mesh
# is closed manifold with unified normals.

croak "usage: $0 input.dxf" unless @ARGV == 1;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $bad = File::DXF->new;

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $state = 'INIT';
my $area_mesh = 0.0;
my $volume_mesh = 0.0;
my $id_triangle;
my $points = [];
my $cog_area = [];
my $cog_volume = [];
my $minmax;
my $minmax_obj;

my $area_total = 0.0;
my $volume_total = 0.0;
my $cog_area_total = [];
my $cog_volume_total = [];

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE' and $item->Triangular)
    {
        say "found mesh, ". $item->Layer .", ". $item->Nodes ." nodes, ". $item->Triangles ." triangles";
        $state = 'POINTS';
        $area_mesh = 0.0;
        $volume_mesh = 0.0;
        $cog_area = [0.0, 0.0, 0.0];
        $cog_volume = [0.0, 0.0, 0.0];
        $id_triangle = 0;
        $points = ['This space intentionally left blank'];
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Point)
    {
        push @{$points}, [$item->{x}, $item->{y}, $item->{z}, 'UNUSED'];

        $minmax = [[$item->{x},$item->{y},$item->{z}],[$item->{x},$item->{y},$item->{z}]]
            unless defined $minmax;
        $minmax->[0]->[0] = $item->{x} if $item->{x} < $minmax->[0]->[0];
        $minmax->[0]->[1] = $item->{y} if $item->{y} < $minmax->[0]->[1];
        $minmax->[0]->[2] = $item->{z} if $item->{z} < $minmax->[0]->[2];
        $minmax->[1]->[0] = $item->{x} if $item->{x} > $minmax->[1]->[0];
        $minmax->[1]->[1] = $item->{y} if $item->{y} > $minmax->[1]->[1];
        $minmax->[1]->[2] = $item->{z} if $item->{z} > $minmax->[1]->[2];

        $minmax_obj = [[$item->{x},$item->{y},$item->{z}],[$item->{x},$item->{y},$item->{z}]]
            unless defined $minmax_obj;
        $minmax_obj->[0]->[0] = $item->{x} if $item->{x} < $minmax_obj->[0]->[0];
        $minmax_obj->[0]->[1] = $item->{y} if $item->{y} < $minmax_obj->[0]->[1];
        $minmax_obj->[0]->[2] = $item->{z} if $item->{z} < $minmax_obj->[0]->[2];
        $minmax_obj->[1]->[0] = $item->{x} if $item->{x} > $minmax_obj->[1]->[0];
        $minmax_obj->[1]->[1] = $item->{y} if $item->{y} > $minmax_obj->[1]->[1];
        $minmax_obj->[1]->[2] = $item->{z} if $item->{z} > $minmax_obj->[1]->[2];
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Triangle)
    {

	$item->{a} = 0 - $item->{a} if ($item->{a} < 0);
	$item->{b} = 0 - $item->{b} if ($item->{b} < 0);
	$item->{c} = 0 - $item->{c} if ($item->{c} < 0);

        my $a = $points->[$item->{a}];
        my $b = $points->[$item->{b}];
        my $c = $points->[$item->{c}];
        my $centre = average_3d ($a, $b, $c);

        $points->[$item->{a}]->[3] = '';
        $points->[$item->{b}]->[3] = '';
        $points->[$item->{c}]->[3] = '';

        my $area = area_3d ($a, $b, $c);
        $area_mesh += $area;
        if ($area < 0.000001)
        {
            say STDOUT join ', ', '    BAD face', @{$centre};
            push @{$bad->{ENTITIES}}, _3dface ([$a,$b,$c,$c]);
        }

        my $volume = $centre->[2] * green_theorem ($a, $b, $c);
        $volume_mesh += $volume;

        $cog_area = add_3d ($cog_area, scale_3d ($area, $centre));

        $cog_volume = add_3d ($cog_volume, scale_3d ($volume, cog_3d ($a, $b, $c)));

        # some triangles have four sides
        if (defined $item->{d})
        {
	    $item->{d} = 0 - $item->{d} if ($item->{d} < 0);

            my $d = $points->[$item->{d}];
            my $centre = average_3d ($a, $c, $d);

            $points->[$item->{d}]->[3] = '';

            my $area_0 = area_3d ($a, $c, $d);
            $area_mesh += $area_0;
            my $area_1 = area_3d ($a, $b, $d);
            my $area_2 = area_3d ($b, $c, $d);
            if ($area_0 < 0.000001 or $area_1 < 0.000001 or $area_2 < 0.000001
                                     or (abs ($area + $area_0 - $area_1 - $area_2) > 0.00000001))
            {
                say STDOUT join ', ', '    BAD face', @{$centre};
                push @{$bad->{ENTITIES}}, _3dface ([$a,$b,$c,$d]);
            }

            my $volume = $centre->[2] * green_theorem ($a, $c, $d);
            $volume_mesh += $volume;

            $cog_area = add_3d ($cog_area, scale_3d ($area_0, $centre));

            $cog_volume = add_3d ($cog_volume, scale_3d ($volume, cog_3d ($a, $c, $d)));
        }
        $id_triangle++;
        print STDERR "\r$id_triangle";
    }

    if ($item->Type eq 'SEQEND')
    {
        for my $point (@{$points})
        {
            next unless ref $point;
            next unless $point->[3] eq 'UNUSED';
            say join ', ', @{$point};
        }
        $cog_area_total = add_3d ($cog_area_total, $cog_area);

        print STDERR "\r";
        say STDOUT "area: $area_mesh";
        $area_mesh = 1 unless $area_mesh;
        $cog_area = scale_3d ($cog_area, 1/$area_mesh);
        say STDOUT 'CoG of surface: ' . join (', ', @{$cog_area});

        $cog_volume_total = add_3d ($cog_volume_total, $cog_volume);

        say STDOUT "volume: $volume_mesh";
        $volume_mesh = 1 unless $volume_mesh;
        $cog_volume = scale_3d ($cog_volume, 1/$volume_mesh);
        say STDOUT 'CoG of volume: ' . join (', ', @{$cog_volume});
        say STDOUT 'Bounding box: '. join (',', @{$minmax_obj->[0]}) .' '. join (',', @{$minmax_obj->[1]});
        undef $minmax_obj;
        say STDOUT '';

        $area_total += $area_mesh;
        $volume_total += $volume_mesh;

        $state = 'NEXT';
    }
}

croak "No meshes found!" unless $area_total;

say STDOUT "Total area: $area_total";
$cog_area_total = scale_3d ($cog_area_total, 1/$area_total);
say STDOUT 'CoG of Total surface: ' . join (', ', @{$cog_area_total});

say STDOUT "Total volume: $volume_total";
$cog_volume_total = scale_3d ($cog_volume_total, 1/$volume_total);
say STDOUT 'CoG of Total volume: ' . join (', ', @{$cog_volume_total});

say STDOUT 'Bounding box: '. join (',', @{$minmax->[0]}) .' '. join (',', @{$minmax->[1]});

if (scalar @{$bad->{ENTITIES}})
{
    open my $DXF, '>', '_bad.dxf';
    binmode $DXF, ":crlf";
    print $DXF $bad->DXF;
    close $DXF;
}

sub _3dface
{
    my $face = shift;
    return {type => '3DFACE', _data => [
        [8, 0],
        [62, 0],
        [10, $face->[0]->[0]],
        [20, $face->[0]->[1]],
        [30, $face->[0]->[2]],

        [11, $face->[1]->[0]],
        [21, $face->[1]->[1]],
        [31, $face->[1]->[2]],

        [12, $face->[2]->[0]],
        [22, $face->[2]->[1]],
        [32, $face->[2]->[2]],

        [13, $face->[2]->[0]],
        [23, $face->[2]->[1]],
        [33, $face->[2]->[2]]]};
}

0;
