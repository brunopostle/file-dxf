#!/usr/bin/perl
use strict;
use warnings;
use lib ('lib', '../lib');
use Math::Trig;
use File::Spec;
use File::DXF;
use File::DXF::Math qw/add_2d subtract_2d normalise_2d scale_2d/;
use File::DXF::Util qw /read_DXF/;
use Carp;
use 5.010;

croak "usage: $0 0.2 INPUT.DXF OUTPUT.DXF" unless @ARGV == 3;

my $width = $ARGV[0];

my $in = $width/-2;
my $out = $width/2;
my $height = $width;

my $dxf_in = File::DXF->new;
my $text = read_DXF ($ARGV[1], 'CP1252');
   $dxf_in->Process ($text);

my @lines = $dxf_in->{ENTITIES}->Report ('LINE');

my $dxf_out = File::DXF->new;

$dxf_out->{HEADER} = File::DXF::HEADER->new;
$dxf_out->{ENTITIES} = File::DXF::ENTITIES->new;

for my $line (@lines)
{
    next unless $line->Length; # ignore zero length lines
    my $start = $line->Coordinates (0);
    my $end = $line->Coordinates (1);
    my $A = [$start->[0], $start->[1]];
    my $B = [$end->[0], $end->[1]];

    my $vector = normalise_2d (subtract_2d ($B, $A));
    my $normal = [$vector->[1], 0-$vector->[0]];

    my $C = add_2d ($B, scale_2d ($out, $normal));
    my $D = add_2d ($A, scale_2d ($out, $normal));

    $A = add_2d ($A, scale_2d ($in, $normal));
    $B = add_2d ($B, scale_2d ($in, $normal));

    push @{$dxf_out->{ENTITIES}}, _box ($A, $B, $C, $D, $start->[2], $start->[2]+$height);
}

open my $DXF, '>', $ARGV[2];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;


sub _3dface
{
    my ($a, $b, $c, $d) = @_;
    my @data;
    push @data, [8, 0];

    push @data, [10, $a->[0]];
    push @data, [20, $a->[1]];
    push @data, [30, $a->[2]];

    push @data, [11, $b->[0]];
    push @data, [21, $b->[1]];
    push @data, [31, $b->[2]];

    push @data, [12, $c->[0]];
    push @data, [22, $c->[1]];
    push @data, [32, $c->[2]];

    push @data, [13, $d->[0]];
    push @data, [23, $d->[1]];
    push @data, [33, $d->[2]];
    return {type => '3DFACE', _data => [@data]};
}

# draw a vertical quad face

sub _vertical
{
    my ($a, $b) = @_;
    my @data;
    push @data, [8, 0];

    push @data, [10, $a->[0]];
    push @data, [20, $a->[1]];
    push @data, [30, $a->[2]];

    push @data, [11, $b->[0]];
    push @data, [21, $b->[1]];
    push @data, [31, $a->[2]];

    push @data, [12, $b->[0]];
    push @data, [22, $b->[1]];
    push @data, [32, $b->[2]];

    push @data, [13, $a->[0]];
    push @data, [23, $a->[1]];
    push @data, [33, $b->[2]];
    return {type => '3DFACE', _data => [@data]};
}

# draw a box that is quadrilateral in plan with vertical sides and horizontal top/bottom

sub _box
{
    my ($a, $b, $c, $d, $top, $bot) = @_;
    my @list;
     
    # top face
    push @list, _3dface ([@{$a}, $top],
                         [@{$b}, $top],
                         [@{$c}, $top],
                         [@{$d}, $top]);

    # bottom face
    push @list, _3dface ([@{$d}, $bot],
                         [@{$c}, $bot],
                         [@{$b}, $bot],
                         [@{$a}, $bot]);

    # outer faces
    push @list, _vertical ([@{$a}, $bot],
                           [@{$b}, $top]);
    push @list, _vertical ([@{$b}, $bot],
                           [@{$c}, $top]);
    push @list, _vertical ([@{$c}, $bot],
                           [@{$d}, $top]);
    push @list, _vertical ([@{$d}, $bot],
                           [@{$a}, $top]);

    return @list;
}

0;
