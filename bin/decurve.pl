#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Math qw /distance_3d add_3d average_3d angle_triangle/;
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use List::Util qw /sum min max shuffle/;
use Carp;
use 5.010;

local $SIG{INT} = \&interrupt;

# take a curved reference mesh and a 2d partially straightened mesh and produce
# a straightened mesh with similar proportions to the curved mesh

croak "usage: $0 ref_in.dxf input.dxf output.dxf" unless @ARGV == 3;

my $dxf_1 = File::DXF->new;
   $dxf_1->Process (read_DXF ($ARGV[0], 'CP1252'));
my ($mesh_1) = $dxf_1->{ENTITIES}->Extract_Meshes;

my $nodes_1 = [map {[split / /, $_]} @{$mesh_1->[0]}];
my $faces_1 = [map {[split / /, $_]} @{$mesh_1->[1]}];

my $dxf_2 = File::DXF->new;
   $dxf_2->Process (read_DXF ($ARGV[1], 'CP1252'));
my ($mesh_2) = $dxf_2->{ENTITIES}->Extract_Meshes;

my $nodes_2 = [map {[split / /, $_]} @{$mesh_2->[0]}];
my $faces_2 = [map {[split / /, $_]} @{$mesh_2->[1]}];

my $progress = 1;

for my $inc (0 .. 10000)
{
    my @bests;
    for my $node_id (shuffle (0 .. scalar @{$nodes_1} -1))
    #for my $node_id (0 .. scalar @{$nodes_1} -1)
    {
        my @faces;
        for my $face (@{$faces_1})
        {
            push @faces, $face if (grep {/^$node_id$/x} @{$face});
        }
        # shuffle x position of this node to try and match proportions of reference mesh
        my $scores;
        for my $i (1 .. 9)
        {
            $i = $i**3 / 8100;
            $nodes_2->[$node_id] = add_3d ($nodes_2->[$node_id], [$i,0,0]);

            my @values = map {(distance_3d (average_3d ($nodes_1->[$_->[0]],$nodes_1->[$_->[1]]),
                                            average_3d ($nodes_1->[$_->[2]],$nodes_1->[$_->[3]]))
                             / distance_3d (average_3d ($nodes_1->[$_->[1]],$nodes_1->[$_->[2]]),
                                            average_3d ($nodes_1->[$_->[3]],$nodes_1->[$_->[0]])))
                            / (distance_3d (average_3d ($nodes_2->[$_->[0]],$nodes_2->[$_->[1]]),
                                            average_3d ($nodes_2->[$_->[2]],$nodes_2->[$_->[3]]))
                             / distance_3d (average_3d ($nodes_2->[$_->[1]],$nodes_2->[$_->[2]]),
                                            average_3d ($nodes_2->[$_->[3]],$nodes_2->[$_->[0]])))}
                                  @faces;

            for my $face (@faces)
            {
                for my $quad_id (0 ..3)
                {
                    push @values, _angle ($nodes_1, $face, $quad_id)
                                / _angle ($nodes_2, $face, $quad_id) if _angle ($nodes_2, $face, $quad_id);
                }
            }

            #my $variance = _variance (@values);
            #my $variance = _max_variance (@values);
            my $variance = _mean (map {($_-1)**2} @values);
            $nodes_2->[$node_id] = add_3d ($nodes_2->[$node_id], [0-$i,0,0]);
            $scores->{$variance} = $i;
        }
        my $best = min (keys %{$scores});
        $nodes_2->[$node_id] = add_3d ($nodes_2->[$node_id], [$scores->{$best},0,0]);
        push @bests, $best;
    }
    my $old_progress = $progress;
    $progress = _mean (@bests);
    say join ' ', $inc, $progress;
    last if ($progress == $old_progress);
}

finish ();
exit 0;

sub interrupt
{
    say STDERR 'SIG_INT';
    finish ();
    exit 1;
}

# write DXF

sub finish
{

my $dxf_out = File::DXF->new;

# mesh header
my $polyline = {type => 'POLYLINE', _data => [
    [8, 'mesh'],
    [62, 0],
    [66, 1],
    [10, 0.0],
    [20, 0.0],
    [30, 0.0],
    [70, 64],
    [71, scalar (@{$nodes_2})],
    [72, scalar (@{$faces_2})]
]};

push @{$dxf_out->{ENTITIES}}, $polyline;

# mesh points
for my $node (@{$nodes_2})
{
    my $vertex = {type => 'VERTEX', _data => [
        [8, 'mesh'],
        [10, $node->[0]],
        [20, $node->[1]],
        [30, $node->[2]],
        [70, 192]
    ]};

    push @{$dxf_out->{ENTITIES}}, $vertex;
}

# mesh triangles
for my $triangle (@{$faces_2})
{
    my $vertex = {type => 'VERTEX', _data => [
        [8, 'mesh'],
        [62, 0],
        [10, 0.0],
        [20, 0.0],
        [30, 0.0],
        [70, 128],
        [71, $triangle->[0] +1],
        [72, $triangle->[1] +1],
        [73, $triangle->[2] +1],
    ]};

    push @{$vertex->{_data}}, [74, $triangle->[3] +1] if defined $triangle->[3];

    push @{$dxf_out->{ENTITIES}}, $vertex;
}

# mesh footer
push @{$dxf_out->{ENTITIES}}, {type => 'SEQEND', _data => []};

open my $DXF ,">", $ARGV[2];
binmode $DXF, ":crlf";
print $DXF $dxf_out->DXF;
close $DXF;
return;

}

sub _angle
{
    my ($nodes, $quad, $quad_id) = @_;
    my $a_coor = $nodes->[$quad->[$quad_id]];
    my $b_coor = $nodes->[$quad->[$quad_id-3]];
    my $c_coor = $nodes->[$quad->[$quad_id-1]];
    my $a_dist = distance_3d ($b_coor, $c_coor);
    my $b_dist = distance_3d ($c_coor, $a_coor);
    my $c_dist = distance_3d ($a_coor, $b_coor);
    return angle_triangle ($a_dist, $b_dist, $c_dist);
}

# stats

sub _mean
{
    return sum (@_) / scalar @_;
}

sub _max_variance
{
    my $mean = _mean (@_);
    return max (map {($_-$mean)**2} @_);
}

sub _variance
{
    my $mean = _mean (@_);
    return _mean (map {($_-$mean)**2} @_);
}

sub _sigma
{
    return sqrt _variance (@_);
}

