#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use 5.010;
use File::DXF;
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF::Math qw /add_3d scale_3d distance_2d distance_3d normalise_3d magnitude_3d add_2d subtract_2d subtract_3d points_2line line_intersection/;
use Carp;

croak "Usage: $0 IN.DXF OUT.DXF" unless scalar @ARGV == 2;

# joins all points into a single 3D POLYLINE
# starting at any point that is coloured

my $filename = $ARGV[0];
my $data = read_DXF ($filename, 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my @points = $dxf->{ENTITIES}->Report ('POINT');

for my $index_a (0 .. scalar @points -1)
{
    next unless ($points[$index_a]->{colour});
    say 'found chain';

    my @chain = splice (@points, $index_a, 1);

    until (scalar @points == 0)
    {
        my $closest_distance = 999999999999;
        my $closest_id = undef;
        for my $index_b (0 .. scalar @points -1)
        {
            my $distance = distance_3d ($chain[-1]->Coordinates, $points[$index_b]->Coordinates);
            if ($distance < $closest_distance)
            {
                $closest_distance = $distance;
                $closest_id = $index_b;
            }
        }
        push @chain, splice (@points, $closest_id, 1);
    }
    say 'points: '. scalar @chain;

    open my $DXF, ">", $ARGV[1];
    binmode $DXF, ":crlf";

    print $DXF dxf (0, 'SECTION');
    print $DXF dxf (2, 'ENTITIES');
    print $DXF dxf (0, 'POLYLINE');
    print $DXF dxf (8, 0);
    print $DXF dxf (66, 1);
    print $DXF dxf (70, 8);

    while (@chain)
    { 
        my $node = shift @chain;
        my $coor = $node->Coordinates;
        print $DXF dxf (0, 'VERTEX');
        print $DXF dxf (8, 0);
        print $DXF dxf (10, $coor->[0]);
        print $DXF dxf (20, $coor->[1]);
        print $DXF dxf (30, $coor->[2]);
        print $DXF dxf (70, 32);
    }

    print $DXF dxf (0, 'SEQEND');
    print $DXF dxf (8, 0);

    print $DXF dxf (0, 'ENDSEC');
    print $DXF dxf (0, 'EOF');

    close $DXF;

    last;
}

0;
