#!/usr/bin/perl

use strict;
use warnings;
use 5.010;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::DXF::ENTITIES;
use Carp;

# Reads a DXF and generates collada geometry.
# only handles POLYLINE 'polyface meshes' and BLOCKS of the same
# BLOCK scale and out-of XY-plane rotation not supported

croak "usage: $0 input.dxf output.dae" unless @ARGV == 2;

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

my $xml_instances;
my $xml_geometries;
my $xml_lights = '';

my %colours_in_model;

my $id = 0;

for my $mesh ($dxf->{ENTITIES}->Extract_Meshes)
{
    $xml_geometries .= _collada_mesh ("Mesh$id", $mesh->[0], $mesh->[1], $mesh->[3]);

    my %colours_in_mesh;
    map {$colours_in_mesh{$_} = 1; $colours_in_model{$_} = 1} @{$mesh->[3]};

    $xml_instances .= _collada_instance ("Mesh$id", [0.0,0.0,0.0], 0.0, [1.0,1.0,1.0], [keys %colours_in_mesh]);
    $id++;
}

my $lines = [];
for my $line ($dxf->{ENTITIES}->Report ('LINE'))
{
    push @{$lines}, [join (' ', @{$line->Coordinates (0)}), join (' ', @{$line->Coordinates (1)})];
}

if (scalar @{$lines})
{
    $xml_geometries .= _collada_lines ("Mesh$id", $lines);
    $xml_instances .= _collada_instance ("Mesh$id", [0.0,0.0,0.0], 0.0, [1.0,1.0,1.0], [0]);
    $colours_in_model{0} = 1;
    $id++;
}

my $valid_blocks = {};

for my $key (keys %{$dxf->{BLOCKS}})
{
    my $entities = File::DXF::ENTITIES->new;
    $entities->Process ($dxf->{BLOCKS}->{$key});
    my @meshes = $entities->Extract_Meshes;
    my $mesh = shift @meshes;
    next unless defined $mesh->[0];
    $xml_geometries .= _collada_mesh ($key, $mesh->[0], $mesh->[1], $mesh->[3]);
    $valid_blocks->{$key} = $mesh;
}

for my $insert ($dxf->{ENTITIES}->Report ('INSERT'))
{
    my $mesh = $valid_blocks->{$insert->{name}} || next;

    my %colours_in_mesh;
    map {$colours_in_mesh{$_} = 1; $colours_in_model{$_} = 1} @{$mesh->[3]};

    $xml_instances .= _collada_instance ($insert->{name},
                          $insert->Coordinates, $insert->{rotation},
                          $insert->Scale, [keys %colours_in_mesh]);
}

my %lights_in_model;
for my $point ($dxf->{ENTITIES}->Report ('POINT'))
{
    next unless $point->{layer} =~ /light/i;
    $lights_in_model{$point->{colour}} = 1;
    $xml_lights .= _collada_light ("Light$id",
                                   [$point->{x},$point->{y},$point->{z}],
                                    $point->{colour});
    $id++;
}

my $xml_effects = join '', map {_effect ($_)} keys %colours_in_model;
my $xml_materials = join '', map {_material ($_)} keys %colours_in_model;
my $xml_library_lights = join '', map {_light ($_)} keys %lights_in_model;

open my $DAE, '>', $ARGV[1];
binmode $DAE, ":crlf";

say $DAE '<?xml version="1.0" encoding="utf-8"?>';
say $DAE '<COLLADA version="1.4.0" xmlns="http://www.collada.org/2005/11/COLLADASchema">';

say $DAE '<asset>';
say $DAE ' <contributor>';
say $DAE '  <author></author>';
say $DAE '  <authoring_tool>File::DXF</authoring_tool>';
say $DAE '  <comments></comments>';
say $DAE '  <copyright></copyright>';
say $DAE '  <source_data>'.$ARGV[0].'</source_data>';
say $DAE ' </contributor>';
say $DAE ' <created>2012-01-03T16:37:29.985000</created>';
say $DAE ' <modified>2012-01-03T16:37:29.985000</modified>';
say $DAE ' <unit meter="1.00" name="meter"/>';
say $DAE ' <up_axis>Z_UP</up_axis>';
say $DAE '</asset>';

say $DAE '<library_effects>';
say $DAE $xml_effects;
say $DAE '</library_effects>';

say $DAE '<library_materials>';
say $DAE $xml_materials;
say $DAE '</library_materials>';

say $DAE '<library_lights>';
say $DAE $xml_library_lights;
say $DAE '</library_lights>';

say $DAE '<library_geometries>';

say $DAE $xml_geometries;

say $DAE '</library_geometries>';

say $DAE '<library_visual_scenes>';
say $DAE ' <visual_scene id="Scene" name="Scene">';

say $DAE $xml_instances;
say $DAE $xml_lights;

say $DAE ' </visual_scene>';
say $DAE '</library_visual_scenes>';
say $DAE '<scene>';
say $DAE ' <instance_visual_scene url="#Scene"/>';
say $DAE '</scene>';
say $DAE '</COLLADA>';

close $DAE;

exit 0;

sub _collada_instance
{
    my ($id, $coor, $rotation, $scale, $id_colours) = @_;
    $rotation = 0.0 unless $rotation;
    $scale = [1,1,1] unless $scale;
    my $xml = '  <node layer="L1" id="'.$id.'" name="'.$id.'">
   <translate sid="translate">'. join (' ', @{$coor}) .'</translate>
   <rotate sid="rotateZ">0 0 1 '.$rotation.'</rotate>
   <rotate sid="rotateY">0 1 0 0.00000</rotate>
   <rotate sid="rotateX">1 0 0 0.00000</rotate>
   <scale sid="scale">'. join (' ', @{$scale}) .'</scale>
   <instance_geometry url="#'.$id.'-Geometry">
    <bind_material>
     <technique_common>
'. join ("\n", map {"      <instance_material symbol=\"Material-ACAD-$_\" target=\"#Material-ACAD-$_\"/>"} @{$id_colours}) .'
     </technique_common>
    </bind_material>
   </instance_geometry>
  </node>
';
    return $xml;
}

sub _collada_light
{
    my ($id, $coor, $colour) = @_;
    return "  <node id=\"Light-$id\" name=\"Light-$id\" type=\"NODE\">
   <translate sid=\"translate\">". join (' ', @{$coor}) ."</translate>
   <instance_light url=\"#Light-$colour\"/>
  </node>
";
}

sub _collada_mesh
{
    my ($id, $nodes, $triangles, $colours) = @_;
    say STDERR "Writing mesh. ID, nodes, triangles: $id, ".
        scalar @{$nodes}. ', ' .scalar @{$triangles};

    my $triangles_by_colour;
    for (0 .. scalar @{$triangles} -1)
    {
        push @{$triangles_by_colour->{$colours->[$_]}}, $triangles->[$_];
    }

    my @polygons;
    for my $colour (keys %{$triangles_by_colour})
    {
        my $triangles = $triangles_by_colour->{$colour};
        push @polygons, 
   '   <polygons material="Material-ACAD-'.$colour.'" count="'. scalar (@{$triangles}) .'">
    <input offset="0" semantic="VERTEX" source="#'.$id.'-Geometry-Vertex"/>
     <p>'.
    join ('</p><p>', @{$triangles}).'</p>
   </polygons>';
    }

    my $xml = '
 <geometry id="'.$id.'-Geometry" name="'.$id.'-Geometry">
  <mesh>
   <source id="'.$id.'-Geometry-Position">
    <float_array count="'. (scalar (@{$nodes}) * 3) .'" id="'.$id.'-Geometry-Position-array">
'.
    join (' ', @{$nodes}) .'
    </float_array>
    <technique_common>
     <accessor count="'. scalar (@{$nodes}) .'" source="#'.$id.'-Geometry-Position-array" stride="3">
      <param type="float" name="X"></param>
      <param type="float" name="Y"></param>
      <param type="float" name="Z"></param>
     </accessor>
    </technique_common>
   </source>
   <vertices id="'.$id.'-Geometry-Vertex">
    <input semantic="POSITION" source="#'.$id.'-Geometry-Position"/>
   </vertices>
'.
    join ("\n", @polygons) .'
  </mesh>
 </geometry>';
    return $xml;
}

sub _collada_lines
{
    my ($id, $lines) = @_;
    say STDERR "Writing LINES: $id, ". scalar @{$lines};

    my $nodes;
    my $nodes_hash;
    my @edges;
    for my $line (@{$lines})
    {
        $nodes_hash->{$line->[0]} = 1;
        $nodes_hash->{$line->[1]} = 1;
    }

    my $inc = 0;
    for my $key (keys %{$nodes_hash})
    {
        push @{$nodes}, $key;
        $nodes_hash->{$key} = $inc;
        $inc++;
    }
    for my $line (@{$lines})
    {
        push @edges, $nodes_hash->{$line->[0]}, $nodes_hash->{$line->[1]};
    }

    my $xml = '
 <geometry id="'.$id.'-Geometry" name="'.$id.'-Geometry">
  <mesh>
   <source id="'.$id.'-Geometry-Position">
    <float_array count="'. (scalar (@{$nodes}) * 3) .'" id="'.$id.'-Geometry-Position-array">
'. join (' ', @{$nodes}) .'
    </float_array>
    <technique_common>
     <accessor count="'. scalar (@{$nodes}) .'" source="#'.$id.'-Geometry-Position-array" stride="3">
      <param type="float" name="X"></param>
      <param type="float" name="Y"></param>
      <param type="float" name="Z"></param>
     </accessor>
    </technique_common>
   </source>
   <vertices id="'.$id.'-Geometry-Vertex">
    <input semantic="POSITION" source="#'.$id.'-Geometry-Position"/>
   </vertices>
   <lines count="'. scalar @{$lines} .'">
    <input offset="0" semantic="VERTEX" source="#'.$id.'-Geometry-Vertex"/>
    <p>'. join (' ', @edges) .'</p>
   </lines>
  </mesh>
 </geometry>';
    return $xml;
}

sub _effect
{
    my $colour = shift;
    my $float = $File::DXF::Util::colourfloat->[$colour] || [0.8,0.8,0.8];
    return " <effect id=\"Material-ACAD-effect-$colour\">
  <profile_COMMON><technique sid=\"common\"><phong>
   <diffuse>
    <color sid=\"diffuse\">" . join (' ', @{$float}) ." 1</color>
   </diffuse>
  </phong></technique></profile_COMMON>
 </effect>
";
}

sub _material
{
    my $colour = shift;
    return " <material id=\"Material-ACAD-$colour\" name=\"Material-ACAD-$colour\">
  <instance_effect url=\"#Material-ACAD-effect-$colour\"/>
 </material>
";
}

sub _light
{
    my $colour = shift;
    my $float = $File::DXF::Util::colourfloat->[$colour] || [1.0,1.0,1.0];
    return " <light id=\"Light-$colour\" name=\"Light\">
  <technique_common>
   <point>
    <color sid=\"color\">". join (' ', @{$float}) ."</color>
    <constant_attenuation>1</constant_attenuation>
    <linear_attenuation>0</linear_attenuation>
    <quadratic_attenuation>0.00111109</quadratic_attenuation>
   </point>
  </technique_common>
  <extra>
   <technique profile=\"blender\">
    <red sid=\"red\" type=\"float\">".$float->[0]."</red>
    <green sid=\"green\" type=\"float\">".$float->[1]."</green>
    <blue sid=\"blue\" type=\"float\">".$float->[2]."</blue>
    <energy sid=\"blender_energy\" type=\"float\">100</energy>
   </technique>
  </extra>
 </light>";
}

