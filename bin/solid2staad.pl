#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use 5.010;
use File::DXF::Util qw /read_DXF/;
use File::DXF::Math qw /distance_3d/;
use File::DXF;
use Carp;

# Reads a DXF consisting of POLYLINE polyface boxes and generates STAAD solid geometry.

croak "usage: $0 input.dxf > output.std" unless @ARGV == 1;
binmode STDOUT, ":crlf";

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);
my $entities = $dxf->{ENTITIES};

my @report = $entities->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

my $tolerance = 0.0001;
my $state = 'INIT';
my @nodes;
my @triangles;
my @vertices_all;
my @cubes;
my @wedges;
my @tetrahedrons;

for my $item (@report)
{
    if ($item->Type eq 'POLYLINE' and $item->Triangular)
    {
        say STDERR "found triangular mesh polyline";
        say STDERR "nodes: ". $item->Nodes;
        say STDERR "triangles: ". $item->Triangles;
        $state = 'POINTS';
        @nodes = (undef);
        @triangles = ();
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Point)
    {
        # STAAD has a maximum line length limit
        $item->{x} = sprintf ("%.6f", $item->{x});
        $item->{y} = sprintf ("%.6f", $item->{y});
        $item->{z} = sprintf ("%.6f", $item->{z});

        push @nodes, [$item->{x}, $item->{y}, $item->{z}];
        push @vertices_all, [$item->{x}, $item->{y}, $item->{z}];
    }

    if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Triangle)
    {
	$item->{a} = 0 - $item->{a} if ($item->{a} < 0);
	$item->{b} = 0 - $item->{b} if ($item->{b} < 0);
	$item->{c} = 0 - $item->{c} if ($item->{c} < 0);

        my $faces = [];

        # some triangles have four sides
        if (defined $item->{d})
        {
	    $item->{d} = 0 - $item->{d} if ($item->{d} < 0);

            push @triangles, [$nodes[$item->{a}],
                              $nodes[$item->{b}],
                              $nodes[$item->{c}],
                              $nodes[$item->{d}]];
        }
        else
        {
            push @triangles, [$nodes[$item->{a}],
                              $nodes[$item->{b}],
                              $nodes[$item->{c}]];
        }
    }

    if ($item->Type eq 'SEQEND')
    {
        # we are collecting only cuboid solids for now
        if (scalar @triangles == 6)
        {
             push @cubes, [@triangles];
        }
        if (scalar @triangles == 5)
        {
             push @wedges, [@triangles];
        }
        if (scalar @triangles == 4)
        {
             push @tetrahedrons, [@triangles];
        }
        $state = 'INIT';
    }
}

my @vertices_clean;
for my $id1 (0 .. scalar @vertices_all -2)
{
    next unless defined $vertices_all[$id1];
    for my $id2 ($id1 +1 .. scalar @vertices_all -1)
    {
        next unless defined $vertices_all[$id2];
        undef $vertices_all[$id2] if distance_3d ($vertices_all[$id1], $vertices_all[$id2]) < $tolerance;
    }
}
for my $foo (@vertices_all)
{
     push @vertices_clean, $foo if defined $foo;
}
 
# $vertices_hash is all the unique 3d coordinates as strings. Here we list and number them.
my $vertices_hash;
my $id_node = 1;
my @vertices;

for my $vertex_vec (@vertices_clean)
{
    $vertices_hash->{join ' ', @{$vertex_vec}} = $id_node;
    push @vertices, (join ' ', $id_node, @{$vertex_vec});
    $id_node++;
}

# @cubes are six quad faces in no particular order, STAAD needs just the two
# opposite faces, one clockwise and the other anticlockwise.
my @elements_cubic;
for my $faces (@cubes)
{
    my $first_face = shift @{$faces};
    next unless scalar @{$first_face} == 4;
    my $opp_face;
    for my $face (@{$faces})
    {
        next unless scalar @{$face} == 4;
        # we are looking for the opposite face that doesn't share any corners
        next if (distance_3d ($face->[0], $first_face->[0]) < $tolerance);
        next if (distance_3d ($face->[1], $first_face->[0]) < $tolerance);
        next if (distance_3d ($face->[2], $first_face->[0]) < $tolerance);
        next if (distance_3d ($face->[3], $first_face->[0]) < $tolerance);
        next if (distance_3d ($face->[0], $first_face->[1]) < $tolerance);
        next if (distance_3d ($face->[1], $first_face->[1]) < $tolerance);
        next if (distance_3d ($face->[2], $first_face->[1]) < $tolerance);
        next if (distance_3d ($face->[3], $first_face->[1]) < $tolerance);
        next if (distance_3d ($face->[0], $first_face->[2]) < $tolerance);
        next if (distance_3d ($face->[1], $first_face->[2]) < $tolerance);
        next if (distance_3d ($face->[2], $first_face->[2]) < $tolerance);
        next if (distance_3d ($face->[3], $first_face->[2]) < $tolerance);
        next if (distance_3d ($face->[0], $first_face->[3]) < $tolerance);
        next if (distance_3d ($face->[1], $first_face->[3]) < $tolerance);
        next if (distance_3d ($face->[2], $first_face->[3]) < $tolerance);
        next if (distance_3d ($face->[3], $first_face->[3]) < $tolerance);
        $opp_face = $face;
    }

    $opp_face = [reverse @{$opp_face}];
    my $best = [@{$opp_face}];
    my $distance = _all_distance ($first_face, $opp_face);
    for (0 .. 2)
    {
        # the opposite face needs to match the node order of the first face
        $opp_face = [$opp_face->[1], $opp_face->[2], $opp_face->[3], $opp_face->[0]];
        next if _all_distance ($first_face, $opp_face) > $distance;
        $distance = _all_distance ($first_face, $opp_face);
        $best = [@{$opp_face}];
    }
    push @elements_cubic, [@{$first_face}, @{$best}];
    say STDERR 'Cube';
}

# @wedges are three quad faces and two triangular faces in no particular order,
# or one quad face and four triangular faces,
# STAAD needs just the two opposite triangular faces, one clockwise and the
# other anticlockwise.
for my $faces (@wedges)
{
    my $first_face;
    my $opp_face;
    for my $face (@{$faces})
    {
        next unless scalar @{$face} == 3;
        $first_face = $face unless defined $first_face;

        # we are looking for the opposite face that shares max one corners
        next if (distance_3d ($face->[0], $first_face->[0]) < $tolerance
             and distance_3d ($face->[1], $first_face->[2]) < $tolerance);
        next if (distance_3d ($face->[0], $first_face->[1]) < $tolerance
             and distance_3d ($face->[1], $first_face->[0]) < $tolerance);
        next if (distance_3d ($face->[0], $first_face->[2]) < $tolerance
             and distance_3d ($face->[1], $first_face->[1]) < $tolerance);

        next if (distance_3d ($face->[1], $first_face->[0]) < $tolerance
             and distance_3d ($face->[2], $first_face->[2]) < $tolerance);
        next if (distance_3d ($face->[1], $first_face->[1]) < $tolerance
             and distance_3d ($face->[2], $first_face->[0]) < $tolerance);
        next if (distance_3d ($face->[1], $first_face->[2]) < $tolerance
             and distance_3d ($face->[2], $first_face->[1]) < $tolerance);

        next if (distance_3d ($face->[2], $first_face->[0]) < $tolerance
             and distance_3d ($face->[0], $first_face->[2]) < $tolerance);
        next if (distance_3d ($face->[2], $first_face->[1]) < $tolerance
             and distance_3d ($face->[0], $first_face->[0]) < $tolerance);
        next if (distance_3d ($face->[2], $first_face->[2]) < $tolerance
             and distance_3d ($face->[0], $first_face->[1]) < $tolerance);

        $opp_face = $face if defined $first_face;
    }

    $opp_face = [reverse @{$opp_face}];
    my $best = [@{$opp_face}];
    my $distance = _all_distance ($first_face, $opp_face);
    for (0 .. 1)
    {
        # the opposite face needs to match the node order of the first face
        $opp_face = [$opp_face->[1], $opp_face->[2], $opp_face->[0]];
        next if _all_distance ($first_face, $opp_face) > $distance;
        $distance = _all_distance ($first_face, $opp_face);
        $best = [@{$opp_face}];
    }
    push @elements_cubic, [@{$first_face}, $first_face->[0], @{$best}, $best->[0]];
    say STDERR 'Wedge';
}

# tetrahedrons are just four triangles, any two faces define the shape
for my $faces (@tetrahedrons)
{
    my $first_face = shift @{$faces};
    my $opp_face = shift @{$faces};
    next unless scalar @{$first_face} == 3;
    next unless scalar @{$opp_face} == 3;

    $opp_face = [reverse @{$opp_face}];
    my $best = [@{$opp_face}];
    my $distance = _all_distance ($first_face, $opp_face);
    for (0 .. 1)
    {
        # the opposite face needs to match the node order of the first face
        $opp_face = [$opp_face->[1], $opp_face->[2], $opp_face->[0]];
        next if _all_distance ($first_face, $opp_face) > $distance;
        $distance = _all_distance ($first_face, $opp_face);
        $best = [@{$opp_face}];
    }
    push @elements_cubic, [@{$first_face}, $first_face->[0], @{$best}, $best->[0]];
    say STDERR 'Tetrahedron';
}

say 'STAAD SPACE';
say 'START JOB INFORMATION';
say 'ENGINEER DATE 31-Aug-16';
say 'END JOB INFORMATION';
say 'INPUT WIDTH 79';
say 'SET Z UP';
say 'UNIT METER KN';
say 'JOINT COORDINATES';
say join '; ', @vertices, '';
say 'ELEMENT INCIDENCES SOLID';
my $element_id = 1;
for my $element (@elements_cubic)
{
    @{$element} = map {_clean_coor ($_, \@vertices_clean)} @{$element};
    say join (' ', $element_id, map {$vertices_hash->{join ' ', @{$_}}} @{$element}). ';';
    $element_id++;
}
say 'FINISH';

sub _all_distance
{
    my ($face_a, $face_b) = @_;
    my $distance = 0;
    for my $id (0 .. scalar @{$face_a} -1)
    {
         $distance += distance_3d ($face_a->[$id], $face_b->[$id]);
    }
    return $distance;
}

sub _clean_coor
{
    my ($coor, $dic) = @_;
    for my $good (@{$dic})
    {
        return $good if distance_3d ($coor, $good) < $tolerance;
    }
    return;
}

0;

