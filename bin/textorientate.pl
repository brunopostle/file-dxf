#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF dxf/;
use File::DXF;
use Carp;

# Reads a DXF and removes orientation vectors (210,220,230 codes)

croak "usage: $0 input.dxf output.dxf" unless @ARGV == 2;

#my $x_factor = shift;
#my $y_factor = shift;
#my $z_factor = shift;

my $data = read_DXF ($ARGV[0], 'CP1252');

#my ($x, $y, $z) = (0.0,0.0,0.0);
my $data_out = [];
my $ENTITIES = 0;

while (@{$data})
{
    my ($code, $string) = File::DXF::_next ($data);

    $ENTITIES = 1 if ($code == 2 and $string eq 'ENTITIES');

    #orientation  codes are '210, 220, 230'
    unless ($ENTITIES and $code =~ /^[2][123]0$/)
    {
        push @{$data_out}, [$code, $string];
        next;
    }

    # a Z code, capture the suffix
    if ($code =~ /^230$/)
    {
        push @{$data_out}, ["210", '0.0'], ["220", '0.0'], ["230", '1.0'];
    }
}

open my $DXF, '>', $ARGV[1];
binmode $DXF, ":crlf";

for my $item (@{$data_out})
{
    print $DXF dxf (@{$item});
}

close $DXF;

0;

