#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::DXF::ENTITIES;
use Carp;
use 5.010;

# Reads a DXF POLYLINE 'triangle mesh' and generates an ASCII SDNF (Steel Detailing Neutral Format) file

croak "usage: $0 input.dxf > output.sdnf" unless @ARGV == 1;
binmode STDOUT, ":crlf";

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

my @meshes = $dxf->{ENTITIES}->Extract_Meshes;

my $faces = 0;
$faces += scalar @{$_->[1]} for @meshes;

#my $mesh = shift @meshes;

say 'Packet 00';
say '""';
say '""';
say '""';
say '""';
say '"10/02/13" "16:27:18"';
say '1 "_Issue_Code_"';
say '"_Design_Code_"';
say '0';
say 'Packet 20';
#say '"millimeters" "millimeters" '. scalar @{$mesh->[1]};
say '"millimeters" "millimeters" '. $faces;

my $index = 200001;
my $thickness = 8;
for my $mesh (@meshes)
{

my $nodes = $mesh->[0];
my $triangles = $mesh->[1];

for my $triangle (@{$triangles})
{
    my @face = split ' ', $triangle;

    say "$index 1 0 0 \"plate\"";
    say '"" "S355" '. $thickness . ' ' . scalar @face;

    say join ' ', map {$_ * 1000} split ' ', $nodes->[$face[0]];
    say join ' ', map {$_ * 1000} split ' ', $nodes->[$face[1]];
    say join ' ', map {$_ * 1000} split ' ', $nodes->[$face[2]];

    if (scalar @face == 4)
    {
        say join ' ', map {$_ * 1000} split ' ', $nodes->[$face[3]];
    }

    $index++;
}
$thickness++;
}

0;
