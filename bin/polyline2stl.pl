#!/usr/bin/perl

use strict;
use warnings;

use lib ('lib');
use File::DXF::Util qw /read_DXF/;
use File::DXF;
use File::DXF::ENTITIES;
use Carp;
use 5.010;

# Reads a DXF POLYLINE 'triangle mesh' and generates an ASCII STL stereolithography file

croak "usage: $0 input.dxf > output.stl" unless @ARGV == 1;
binmode STDOUT, ":unix";

my $data = read_DXF ($ARGV[0], 'CP1252');

my $dxf = File::DXF->new;
   $dxf->Process ($data);

say 'solid '. $ARGV[0];

for my $mesh ($dxf->{ENTITIES}->Extract_Meshes)
{
    my $nodes = $mesh->[0];
    my $triangles = $mesh->[1];

    for my $node (@{$nodes})
    {
        say STDERR "Warning: negative coordinate: $node" if $node =~ /-/;
    }

    for my $triangle (@{$triangles})
    {
        my @face = split ' ', $triangle;
        say 'facet normal 0 0 0';
        say 'outer loop';

        say join ' ', 'vertex', split ' ', $nodes->[$face[0]];
        say join ' ', 'vertex', split ' ', $nodes->[$face[1]];
        say join ' ', 'vertex', split ' ', $nodes->[$face[2]];

        say 'endloop';
        say 'endfacet';

        if (scalar @face == 4)
        {
            say 'facet normal 0 0 0';
            say 'outer loop';

            say join ' ', 'vertex', split ' ', $nodes->[$face[2]];
            say join ' ', 'vertex', split ' ', $nodes->[$face[3]];
            say join ' ', 'vertex', split ' ', $nodes->[$face[0]];

            say 'endloop';
            say 'endfacet';
        }
    }
}

say 'endsolid '. $ARGV[0];

0;
