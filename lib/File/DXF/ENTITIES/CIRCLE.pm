package File::DXF::ENTITIES::CIRCLE;

=head1 NAME

File::DXF::ENTITIES::CIRCLE - AutoCAD DXF CIRCLE entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use File::DXF::ENTITIES::Entity qw /next_item pi/;

use base 'File::DXF::ENTITIES::Entity';

=over

=item new

Create one like so:

  $foo = new File::DXF::ENTITIES::CIRCLE;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = { x => 0,
                 y => 0,
                 z => 0,
             layer => 0,
            colour => 0,
            radius => 0 };

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=item Radius

  $float = $foo->Radius;

=item Diameter

  $float = $foo->Diameter;

=item Circumference

  $float = $foo->Circumference;

=item Area

  $float = $foo->Area;

=back

=cut

sub Process
{
    my $self = shift;
    my $data = shift;

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);
        $self->Radius ($string) if ($code == 40);
        $self->{x}        = $string if ($code == 10);
        $self->{y}        = $string if ($code == 20);
        $self->{z}        = $string if ($code == 30);
        $self->{normal_x} = $string if ($code == 210);
        $self->{normal_y} = $string if ($code == 220);
        $self->{normal_z} = $string if ($code == 230);
        $self->{layer}    = $string if ($code == 8);
        $self->{colour}   = $string if ($code == 62);
    }
    return;
}

sub Radius
{
    my $self = shift;
    $self->{radius} = shift || $self->{radius};
    return $self->{radius};
}

sub Layer
{
    my $self = shift;
    $self->{layer} = shift || $self->{layer};
    return $self->{layer};
}

sub Centre
{
    my $self = shift;
    return [$self->{x}, $self->{y}, $self->{z}]
}

sub Diameter
{
    my $self = shift;
    return (2 * $self->Radius);
}

sub Circumference
{
    my $self = shift;
    return (&pi * $self->Diameter);
}

sub Length
{
    my $self = shift;
    return $self->Circumference;
}

sub Start
{
    return 0.0
}

sub End
{
    return 360.0
}

sub Angle
{
    return 360.0
}

sub Area
{
    my $self = shift;
    return (&pi * $self->Radius * $self->Radius);
}

1;

