package File::DXF::ENTITIES::LWPOLYLINE;

use strict;
use warnings;

use File::DXF::ENTITIES::Entity 'next_item';
use File::DXF::ENTITIES::POINT;

use base 'File::DXF::ENTITIES::POINT';

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = { z => 0,
              mode => undef,
            smooth => undef,
	     nodes => [],
	    colour => 0,
             layer => 0 };

    bless $self, $class;
    return $self;
}

sub Process
{
    my $self = shift;
    my $data = shift;

    my $x;
    while (@{$data})
    {
        my ($code, $string) = next_item ($data);
        $self->{layer}  = $string if ($code == 8);
	$x              = $string if ($code == 10);
	push @{$self->{nodes}}, [$x,$string] if ($code == 20);
        $self->{z}      = $string if ($code == 38);
        $self->{colour} = $string if ($code == 62);
        $self->{mode}   = $string if ($code == 70);
    }
    return;
}

1;

