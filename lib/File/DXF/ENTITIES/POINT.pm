package File::DXF::ENTITIES::POINT;

=head1 NAME

File::DXF::ENTITIES::POINT - AutoCAD DXF POINT entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use File::DXF::ENTITIES::Entity 'next_item';

use base 'File::DXF::ENTITIES::Entity';

=over

=item new

Create one like so:

  $foo = new File::DXF::ENTITIES::POINT;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = { x => 0,
                 y => 0,
                 z => 0,
             layer => 0,
            colour => 0 };

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=back

=cut

sub Process
{
    my $self = shift;
    my $data = shift;

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);
        $self->{x} = $string if ($code == 10);
        $self->{y} = $string if ($code == 20);
        $self->{z} = $string if ($code == 30);
        $self->{mode} = $string if ($code == 70);
        $self->{layer} = $string if ($code == 8);
        $self->{colour} = $string if ($code == 62);
    }
    return;
}

1;

