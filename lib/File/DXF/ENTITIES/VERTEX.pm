package File::DXF::ENTITIES::VERTEX;

=head1 NAME

File::DXF::ENTITIES::VERTEX - AutoCAD DXF VERTEX entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use File::DXF::ENTITIES::POINT;
use File::DXF::ENTITIES::Entity 'next_item';

use base 'File::DXF::ENTITIES::POINT';

=over

=item Process

  $vertex->Process;

=cut

sub Process
{
    my $self = shift;
    my $data = shift;

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);
        $self->{x} = $string if ($code == 10);
        $self->{y} = $string if ($code == 20);
        $self->{z} = $string if ($code == 30);
        $self->{colour} = $string if ($code == 62);
        $self->{mode} = $string if ($code == 70);
        $self->{a} = $string if ($code == 71);
        $self->{b} = $string if ($code == 72);
        $self->{c} = $string if ($code == 73);
        $self->{d} = $string if ($code == 74);
    }
    return;
}

=item Triangle

  say 'a triangle!' if $vertex->Triangle;

A VERTEX entity can be a three or four sided 'triangle' in a POLYLINE mesh.

=cut

sub Triangle
{
    my $self = shift;
    return 1 if $self->{mode} == 128;
    return 0;
}

=item Point

  say 'a point!' if $vertex->Point;

Alternatively a VERTEX can be a coordinate 'point' in a POLYLINE mesh.

=back

=cut

sub Point
{
    my $self = shift;
    return 1 if $self->{mode} == 192;
    return 0;
}

1;

