package File::DXF::ENTITIES::SEQEND;

=head1 NAME

File::DXF::ENTITIES::SEQEND - AutoCAD DXF SEQEND entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use File::DXF::ENTITIES::Entity 'next_item';

use base 'File::DXF::ENTITIES::Entity';

1;

