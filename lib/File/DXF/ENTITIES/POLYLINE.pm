package File::DXF::ENTITIES::POLYLINE;

=head1 NAME

File::DXF::ENTITIES::POLYLINE - AutoCAD DXF POLYLINE entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use File::DXF::ENTITIES::Entity 'next_item';
use File::DXF::ENTITIES::POINT;

use base 'File::DXF::ENTITIES::POINT';

=over

=item new

Create one like so:

  $foo = new File::DXF::ENTITIES::POLYLINE;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = { x => 0,
                 y => 0,
                 z => 0,
              mode => undef,
            smooth => undef,
	    colour => 0,
             layer => 0,
            size_m => 0,
            size_n => 0 };

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=cut

sub Process
{
    my $self = shift;
    my $data = shift;

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);
        $self->{x}      = $string if ($code == 10);
        $self->{layer}  = $string if ($code == 8);
        $self->{y}      = $string if ($code == 20);
        $self->{z}      = $string if ($code == 30);
        $self->{colour} = $string if ($code == 62);
        $self->{mode}   = $string if ($code == 70);
        $self->{size_m} = $string if ($code == 71);
        $self->{size_n} = $string if ($code == 72);
        $self->{smooth} = $string if ($code == 75);
    }
    return;
}

=item Size_M Size_N

A rectangular mesh has grid dimensions M,N

=cut

sub Size_M
{
    my $self = shift;
    return 0 unless ($self->Rectangular);
    return $self->{size_m};
}

sub Size_N
{
    my $self = shift;
    return 0 unless ($self->Rectangular);
    return $self->{size_n};
}

=item Rectangular Triangular

Query if a POLYLINE is a rectangular mesh or a triangular mesh, note it could be neither.

=cut

sub Rectangular
{
    my $self = shift;
    return 0 unless defined $self->{mode};
    return 1 if ($self->{mode} == 16); # open mesh
    return 1 if ($self->{mode} == 17); # closed M only
    return 1 if ($self->{mode} == 48); # closed N only
    return 1 if ($self->{mode} == 49); # closed M & N
    return 0;
}

sub Triangular
{
    my $self = shift;
    return 0 unless defined $self->{mode};
    return 1 if ($self->{mode} == 64); # triangular mesh
    return 0;
}

=item Closed_M Closed_N

A rectangular mesh can be closed in the N or M direction (a cylinder) or both (a torus).

=cut

sub Closed_M
{
    my $self = shift;
    return 1 if ($self->{mode} == 17); # closed M only
    return 1 if ($self->{mode} == 49); # closed M & N
    return 0;
}

sub Closed_N
{
    my $self = shift;
    return 1 if ($self->{mode} == 48); # closed N only
    return 1 if ($self->{mode} == 49); # closed M & N
    return 0;
}

=item Index

Get the location in the rectangular mesh of a NODE with index value $i:

    ($m, $n) = $foo->Index ($i);

For example, the first NODE in a mesh is always the origin node:

    ($m, $n) = $foo->Index (0);

now $m is 0 and $n is 0

=cut

sub Index
{
    my $self = shift;
    my $index = shift;
    return if ($index >= $self->Nodes);
    return if ($self->Nodes == 0);
    my $m = int ($index / $self->Size_N);
    my $n = $index - ($m * $self->Size_N);
    return ($m, $n);
}

=item Nodes Triangles

Query the number of nodes in the mesh, or the number of faces if a triangular mesh.

=back

=cut

sub Nodes
{
   my $self = shift;
   return ($self->Size_M * $self->Size_N) if $self->Rectangular;
   return $self->{size_m} if $self->Triangular;
   return 0;
}

sub Triangles
{
   my $self = shift;
   return $self->{size_n} if $self->Triangular;
   return 0;
}

1;

