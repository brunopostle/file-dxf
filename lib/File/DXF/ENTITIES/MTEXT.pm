package File::DXF::ENTITIES::MTEXT;

=head1 NAME

File::DXF::ENTITIES::MTEXT - AutoCAD DXF MTEXT entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;
use Encode qw /decode/;

use File::DXF::ENTITIES::Entity 'next_item';
use File::DXF::Util 'decode_AutoCAD';

use base 'File::DXF::ENTITIES::Entity';

=over

=item new

Create one like so:

  $foo = new File::DXF::ENTITIES::MTEXT;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = {content => '',
                      x => 0,
                      y => 0,
                      z => 0,
                 height => 2.5,
                  layer => 0,
		 colour => 0,
               rotation => 0};

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=cut

sub Process
{
    my $self = shift;
    my $data = shift;

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);
        $self->{content} = decode_AutoCAD ($string) if ($code == 1);
        $self->{x} = $string if ($code == 10);
        $self->{y} = $string if ($code == 20);
        $self->{z} = $string if ($code == 30);
        $self->{height} = $string if ($code == 40);
        $self->{rotation} = $string if ($code == 50);
        $self->{layer} = $string if ($code == 8);
        $self->{colour} = $string if ($code == 62);
    }
    return;
}

=item Content

  my $text_string = $foo->Content;
  $foo->Content ('new text string');

=cut

sub Content
{
    my $self = shift;
    $self->{content} = shift || $self->{content};
    return $self->{content};
}

=item Height

  my $text_height = $foo->Height;
  $foo->Height (10.5);

=cut

sub Height
{
    my $self = shift;
    $self->{height} = shift || $self->{height};
    return $self->{height};
}

=item Rotation

  my $text_degrees = $foo->Rotation;
  $foo->Rotation (30.0);

=back

=cut

sub Rotation
{
    my $self = shift;
    $self->{rotation} = shift || $self->{rotation};
    return $self->{rotation};
}

=item Colour

  my $text_degrees = $foo->Colour;
  $foo->Colour (30.0);

=back

=cut

sub Colour
{
    my $self = shift;
    $self->{colour} = shift || $self->{colour};
    return $self->{colour};
}

1;

