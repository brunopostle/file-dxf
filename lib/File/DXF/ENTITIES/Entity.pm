package File::DXF::ENTITIES::Entity;

=head1 NAME

File::DXF::ENTITIES::Entity - Generic AutoCAD DXF entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;
use base 'Exporter';
use vars qw /@EXPORT_OK/;

@EXPORT_OK = qw /next_item pi distance/;

=over

=item new

Create one like so:

  $foo = new File::DXF::ENTITIES::$bar;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = {};

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=item Coordinates

  $xyz_coor = $entity->Coordinates;

=item Normal

  $xyz_vector = $entity->Normal;

=item Type

  $string = $entity->Type;

=cut

sub Colour
{
    my $self = shift;
    $self->{colour} = shift if @_;
    return $self->{colour} || 0;
}

sub Process
{
    my $self = shift;
    my $data = shift;
    return;
}

sub Coordinates
{
    my $self = shift;
    return [$self->{x}, $self->{y}, $self->{z}];
}

sub Normal
{
    my $self = shift;
    return [0, 0, 1] unless defined $self->{normal_x};
    return [$self->{normal_x}, $self->{normal_y}, $self->{normal_z}];
}

sub Type
{
    my $self = shift;
    my $type = ref $self;
    $type =~ s/.*:://x;
    return $type;
}

sub Layer
{
    my $self = shift;
    $self->{layer} = shift || $self->{layer};
    return $self->{layer};
}

=head2 misc functions

=item distance

  $distance_3d = distance ($coor_a, $coor_b);

=cut

sub distance
{
    my ($a, $b) = @_;
    return sqrt ( (($a->[0] - $b->[0]) * ($a->[0] - $b->[0]))
                + (($a->[1] - $b->[1]) * ($a->[1] - $b->[1]))
                + (($a->[2] - $b->[2]) * ($a->[2] - $b->[2])) );
}

=item pi

  say 2 * pi() * $r;

=cut

sub pi
{
    return 3.1415926535897932384626433832795;
}

=item next_item

iterate through list of items:

  my ($key, $value) = next_item ($data);

=back

=cut

sub next_item
{
    my $data = shift;
    my $item = shift @{$data};
    return @$item;
}

1;

