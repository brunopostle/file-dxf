package File::DXF::ENTITIES::INSERT;

=head1 NAME

File::DXF::ENTITIES::INSERT - AutoCAD DXF INSERT entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use File::DXF::ENTITIES::POINT;
use File::DXF::ENTITIES::Entity 'next_item';

use base 'File::DXF::ENTITIES::POINT';

=over

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=item Scale

  my $xyz_scale = $foo->Scale;

=back

=cut

sub Process
{
    my $self = shift;
    my $data = shift;

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);
        $self->{name} = $string if ($code == 2);
        $self->{x} = $string if ($code == 10);
        $self->{y} = $string if ($code == 20);
        $self->{z} = $string if ($code == 30);
        $self->{scale_x} = $string if ($code == 41);
        $self->{scale_y} = $string if ($code == 42);
        $self->{scale_z} = $string if ($code == 43);
        $self->{rotation} = $string if ($code == 50);
        $self->{mode} = $string if ($code == 70);
    }
    return;
}

sub Scale
{
    my $self = shift;
    $self->{scale_x} = 1 unless defined $self->{scale_x};
    $self->{scale_y} = 1 unless defined $self->{scale_y};
    $self->{scale_z} = 1 unless defined $self->{scale_z};
    return [$self->{scale_x}, $self->{scale_y}, $self->{scale_z}];
}

=item Rotation

  my $text_degrees = $foo->Rotation;
  $foo->Rotation (30.0);

=back

=cut

sub Rotation
{
    my $self = shift;
    $self->{rotation} = shift || $self->{rotation};
    return $self->{rotation} || 0;
}

1;

