package File::DXF::ENTITIES::3DFACE;

=head1 NAME

File::DXF::ENTITIES::3DFACE - AutoCAD DXF 3DFACE entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use File::DXF::ENTITIES::Entity ('next_item', 'distance');

use base 'File::DXF::ENTITIES::Entity';

=over

=item new

Create one like so:

  $foo = new File::DXF::ENTITIES::3DFACE;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = {nodes => [[undef],[undef],[undef],[undef]], hidden => [0,0,0,0], layer => 0, colour => 0};

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=item Coordinates

  $xyz_coor = $foo->Coordinates (2); # (range 0 .. 3)

=item Colour

  $colour = $foo->Colour;

=item Packed

  $string = $foo->Packed (2); # (range 0 .. 3)

=back

=cut

sub Process
{
    my $self = shift;
    my $data = shift;

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);
        $self->{nodes}->[0]->[0] = $string if ($code == 10);
        $self->{nodes}->[0]->[1] = $string if ($code == 20);
        $self->{nodes}->[0]->[2] = $string if ($code == 30);
        $self->{nodes}->[1]->[0] = $string if ($code == 11);
        $self->{nodes}->[1]->[1] = $string if ($code == 21);
        $self->{nodes}->[1]->[2] = $string if ($code == 31);
        $self->{nodes}->[2]->[0] = $string if ($code == 12);
        $self->{nodes}->[2]->[1] = $string if ($code == 22);
        $self->{nodes}->[2]->[2] = $string if ($code == 32);
        $self->{nodes}->[3]->[0] = $string if ($code == 13);
        $self->{nodes}->[3]->[1] = $string if ($code == 23);
        $self->{nodes}->[3]->[2] = $string if ($code == 33);
        $self->{layer}  = $string if ($code == 8);
        $self->{colour} = $string if ($code == 62);
        $self->{hidden} = [reverse (split '', sprintf ('%04b', $string))] if $code == 70;
    }
    return;
}

sub Coordinates
{
    my $self = shift;
    my $index = shift || 0;
    return $self->{nodes}->[$index];
}

sub Colour
{
    my $self = shift;
    $self->{colour} = shift if @_;
    return $self->{colour} || 0;
}

sub Packed
{
    my $self = shift;
    my $index = shift || 0;
    return _stringify ($self->Coordinates ($index));
}

sub _stringify
{
    my $point = shift;
    local $^A = '';
    # note, this only works for values between -999,999,999 and 9,999,999,999
    formline ('@##########.######:@##########.######:@##########.######',
        @{$point});
    my $key = $^A;
    local $^A = '';
    return $key;
}

1;

