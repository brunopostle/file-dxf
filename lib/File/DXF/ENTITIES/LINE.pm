package File::DXF::ENTITIES::LINE;

=head1 NAME

File::DXF::ENTITIES::LINE - AutoCAD DXF LINE entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use File::DXF::ENTITIES::Entity ('next_item', 'distance');
use File::DXF::Math qw /add_3d scale_3d/;

use base 'File::DXF::ENTITIES::Entity';

=over

=item new

Create one like so:

  $foo = new File::DXF::ENTITIES::LINE;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = { x => 0,
                 y => 0,
                 z => 0,
            colour => 0,
             layer => 0,
         thickness => 0,
                x1 => 0,
                y1 => 0,
                z1 => 0 };

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=item Coordinates

  my $xyz_coor = $foo->Coordinates (1); # range is 0 .. 1

=item Length

  my $float = $foo->Length;

=back

=cut

sub Process
{
    my $self = shift;
    my $data = shift;

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);
        $self->{x} = $string if ($code == 10);
        $self->{y} = $string if ($code == 20);
        $self->{z} = $string if ($code == 30);
        $self->{x1} = $string if ($code == 11);
        $self->{y1} = $string if ($code == 21);
        $self->{z1} = $string if ($code == 31);
        $self->{layer}  = $string if ($code == 8);
        $self->{colour} = $string if ($code == 62);
        $self->{thickness} = $string if ($code == 39);
    }
    return;
}

sub Coordinates
{
    my $self = shift;
    my $index = shift;
    return [$self->{x}, $self->{y}, $self->{z}] unless defined $index;
    my $coor_new = shift;
    ($self->{x}, $self->{y}, $self->{z}) = (@{$coor_new}) if ($index == 0 and $coor_new);
    ($self->{x1}, $self->{y1}, $self->{z1}) = (@{$coor_new}) if ($index == 1 and $coor_new);
    return [$self->{x1}, $self->{y1}, $self->{z1}] if $index == 1;
    return [$self->{x}, $self->{y}, $self->{z}];
}

sub Middle
{
    my $self = shift;
    return scale_3d(0.5, add_3d($self->Coordinates(0), $self->Coordinates(1)));
}

sub Length
{
    my $self = shift;
    return distance ( $self->Coordinates(0), $self->Coordinates(1));
}

1;

