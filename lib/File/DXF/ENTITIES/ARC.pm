package File::DXF::ENTITIES::ARC;

=head1 NAME

File::DXF::ENTITIES::ARC - AutoCAD DXF ARC entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use File::DXF::ENTITIES::Entity qw /next_item pi/;

use base 'File::DXF::ENTITIES::Entity';

=over

=item new

Create one like so:

  $foo = new File::DXF::ENTITIES::ARC;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = { x => 0,
                 y => 0,
                 z => 0,
             layer => 0,
            colour => 0,
            radius => 0,
             start => 0,
               end => 0 };

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=item Radius

  $float = $foo->Radius;

=item Diameter

  $float = $foo->Diameter;

=item Start

  $degrees = $foo->Start;

=item End

  $degrees = $foo->End;

=back

=cut

sub Process
{
    my $self = shift;
    my $data = shift;

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);
        $self->Radius ($string) if ($code == 40);
        $self->{x}        = $string if ($code == 10);
        $self->{y}        = $string if ($code == 20);
        $self->{z}        = $string if ($code == 30);
        $self->{start}    = $string if ($code == 50);
        $self->{end}      = $string if ($code == 51);
        $self->{normal_x} = $string if ($code == 210);
        $self->{normal_y} = $string if ($code == 220);
        $self->{normal_z} = $string if ($code == 230);
        $self->{layer}    = $string if ($code == 8);
        $self->{colour}   = $string if ($code == 62);
    }
    return;
}

sub Radius
{
    my $self = shift;
    $self->{radius} = shift || $self->{radius};
    return $self->{radius};
}

sub Start
{
    my $self = shift;
    $self->{start} = shift || $self->{start};
    return $self->{start};
}

sub End
{
    my $self = shift;
    $self->{end} = shift || $self->{end};
    return $self->{end};
}

sub Centre
{
    my $self = shift;
    return [$self->{x}, $self->{y}, $self->{z}];
}

sub Diameter
{
    my $self = shift;
    return (2 * $self->Radius);
}

sub Angle
{
    my $self = shift;
    if ($self->Start > $self->End)
    {
        return 360 - ($self->Start - $self->End);
    }
    return $self->End - $self->Start;
}

sub Length
{
    my $self = shift;
    my $circumference = &pi * $self->Diameter;
    return $circumference * $self->Angle / 360;
}

1;
