package File::DXF::ENTITIES::ATTRIB;

=head1 NAME

File::DXF::ENTITIES::ATTRIB - AutoCAD DXF ATTRIB entity

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;

use File::DXF::ENTITIES::Entity 'next_item';
use File::DXF::Util 'decode_AutoCAD';
use File::DXF::ENTITIES::TEXT;

use base 'File::DXF::ENTITIES::TEXT';

=over

=item new

Create one like so:

  $foo = new File::DXF::ENTITIES::ATTRIB;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = {content => '',
                   name => '',
                      x => 0,
                      y => 0,
                      z => 0,
                 height => 2.5,
		 colour => 0,
               rotation => 0};

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=cut

sub Process
{
    my $self = shift;
    my $data = shift;

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);
        $self->{content} = decode_AutoCAD ($string) if ($code == 1);
        $self->{name} = $string if $code == 2;
        $self->{x} = $string if ($code == 10);
        $self->{y} = $string if ($code == 20);
        $self->{z} = $string if ($code == 30);
        $self->{height} = $string if ($code == 40);
        $self->{rotation} = $string if ($code == 50);
        $self->{colour} = $string if ($code == 62);
    }
    return;
}

=item Name

  my $text_string = $foo->Name;
  $foo->Name ('POINT_ELEV');

=cut

sub Name
{
    my $self = shift;
    $self->{name} = shift || $self->{name};
    return $self->{name};
}

1;

