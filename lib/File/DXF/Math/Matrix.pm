package File::DXF::Math::Matrix;

=head1 NAME

File::DXF::Math::Matrix - Miscellaneous matrix math

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 USAGE

  use File::DXF::Math::Matrix qw(transpose multiply vekpro);

  my $point  = [[$x1], [$y1], [$z1]];

  my $matrix = [[ 1, 0, 0 ],
                [ 0, 1, 0 ],
                [ 0, 0, 1 ]];

=over

=item transpose

  $matrix_tansposed = transpose ($matrix);

=item multiply

  $matrix_multiplied = multiply ($matrix, $point);

  ($x2, $y2, $z2) = ($result->[0]->[0], $result->[1]->[0], $result->[2]->[0]);

=item vekpro

  $scalar = vekpro ($matrix_a, $matrix_b);

=back

=cut

use Math::Trig;
use Math::Trig ':radial';
use strict;
use warnings;

use base 'Exporter';
use vars qw(@EXPORT_OK);
@EXPORT_OK = qw(transpose multiply vekpro); 

sub transpose
{
    my $matrix_in = shift;
    my $matrix_out;

    my $n = 0;
    for my $row (@{$matrix_in})
    {
        my $m = 0;
        for my $column (@{$row})
        {
            $matrix_out->[$m]->[$n] = $matrix_in->[$n]->[$m];
            $m++;
        }
        $n++;
    }
    return $matrix_out;
}

sub multiply
{
    my $matrix_a = shift;
    my $transposed_b = transpose (shift);
    my $matrix_out;

    return if (scalar @{$matrix_a->[0]} != scalar @{$transposed_b->[0]});
    for my $row (@{$matrix_a})
    {
        my $rescol = [];
        for my $column (@{$transposed_b})
        {
            push (@{$rescol}, vekpro ($row, $column));
        }
        push (@{$matrix_out}, $rescol);
    }
    return $matrix_out;
}

sub vekpro
{
    my ($a, $b) = @_;
    my $result = 0;

    for my $i (0 .. scalar @{$a} - 1)
    {
        $result += $a->[$i] * $b->[$i];
    }
    return $result;
}

1;


