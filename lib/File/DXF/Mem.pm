package File::DXF::Mem;

=head1 NAME

File::DXF::Mem - Patterner geometry mesh files

=head1 SYNOPSIS

Simple Mem writing

=head1 DESCRIPTION

=cut

use strict;
use warnings;
use Carp;
use base 'Exporter';

use vars qw /@EXPORT_OK/;
@EXPORT_OK = qw /FREE FIXED STRAIGHT/;

=over

=item new

Create a Mem object like so:

  $mem = new File::DXF::Mem;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self;

    $self = {data => [[]],
       view_scale => 50,
           header => "Mem mesh produced by $class"};

    bless $self, $class;
    return $self;
}

sub Mesh_Size_X
{
    my $self = shift;
    my $data = $self->{data};
    return scalar @{$data};
}

sub Mesh_Size_Y
{
    my $self = shift;
    my $data = $self->{data};
    my $y = 1;
    for (@{$data})
    {
        next unless $_;
        $y = scalar @{$_} if ($y lt scalar @{$_});
    }
    return $y;
}

sub FIXITY {return 3}
sub FREE {return 0}
sub FIXED {return 1}
sub STRAIGHT {return 3}

sub Fixup
{
    my $self = shift;
    my $data = $self->{data};
    for my $i (0 .. $self->Mesh_Size_X - 1)
    {
        for my $j (0 .. $self->Mesh_Size_Y - 1)

        {
            my $node = $data->[$i]->[$j];

            # the node doesn't have coordinates, set defaults
            $node = [0.0, 0.0, 0.0, FREE] unless $node->[0];

            # the node doesn't have any fixity
            if ($node->[FIXITY] == FREE)
            {
                # the node is on an edge, make 'fixed'
                $node->[FIXITY] = FIXED if ($i == 0);
                $node->[FIXITY] = FIXED if ($j == 0);
                $node->[FIXITY] = FIXED if ($i == $self->Mesh_Size_X - 1);
                $node->[FIXITY] = FIXED if ($j == $self->Mesh_Size_Y - 1);
            }

            $data->[$i]->[$j] = $node;
        }
    }
    return;
}

sub Read
{
    my $self = shift;
    my $filename = shift;
    open my $FILE, '<', $filename;
    binmode $FILE, ':crlf';
    my @lines = <$FILE>;
    close $FILE;

    my $size_x;
    my $size_y;
    while (scalar @lines)
    {
        my $line = shift @lines;
        $line =~ s/(^ +| +$|\r|\n|\l)//g;
        if ($line =~ m/^\[Mesh Size X\]/i)
        {
            $size_x = shift @lines;
        }
        elsif ($line =~ m/^\[Mesh Size Y\]/i)
        {
            $size_y = shift @lines;
        }
        elsif ($line =~ m/^\[Mesh Data\]/i)
        {
            for (1 .. $size_x * $size_y)
            {
                 my $m = shift @lines;
                 my $n = shift @lines;
                 $self->{data}->[$m-1]->[$n-1]->[0] = shift @lines;
                 $self->{data}->[$m-1]->[$n-1]->[1] = shift @lines;
                 $self->{data}->[$m-1]->[$n-1]->[2] = shift @lines;
                 $self->{data}->[$m-1]->[$n-1]->[3] = shift @lines;
            }
        }
    }
    return 1;
}

=item Write

Output the object as a MEM file like so:

  $mem->Write ("filename.mem");

=cut

sub Write
{
    my $self = shift;
    my $filename = shift;
    my $data = $self->{data};

    open my $FILE, ">:encoding(utf8)", $filename
         or croak "cannot write-open $filename";
    binmode $FILE, ':crlf';
    print $FILE "[Header]\n". $self->{header} ."\n";
    print $FILE "[View Scale]\n". $self->{view_scale} ."\n";
    print $FILE "[Mesh Size X]\n". $self->Mesh_Size_X ."\n";
    print $FILE "[Mesh Size Y]\n". $self->Mesh_Size_Y ."\n";
    print $FILE "[Mesh Data]\n";
    for my $i (0 .. $self->Mesh_Size_X - 1)
    {
        for my $j (0 .. $self->Mesh_Size_Y - 1)
        {
            print $FILE ($i + 1) ."\n". ($j + 1) ."\n";
            print $FILE (join "\n", @{$data->[$i]->[$j]}) ."\n";
        }
    }

    print $FILE "[EOF]\n";
    close $FILE;
    return 1;
}

1;
