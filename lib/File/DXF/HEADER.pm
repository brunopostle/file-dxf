package File::DXF::HEADER;

use strict;
use warnings;
use File::DXF::Util 'dxf';

use File::DXF::Section;
use base 'File::DXF::Section';

=head1 NAME

File::DXF::HEADER - Handle AutoCAD DXF HEADER list

=head1 SYNOPSIS

HEADER is one section of a DXF file, this class inherits from L<File::DXF::Section>

=head1 DESCRIPTION

=over

=item DXF

  $dxf = $section->DXF;

=back

=cut

sub DXF
{
    my $self = shift;
    my $text;
    $text .= dxf (0, 'SECTION');
    $text .= dxf (2, 'HEADER');
    $text .= dxf (9, '$ACADVER');
    $text .= dxf (1, 'AC1009');
    $text .= dxf (0, 'ENDSEC');
    return $text;
}

1;

