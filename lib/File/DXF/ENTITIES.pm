package File::DXF::ENTITIES;

=head1 NAME

File::DXF::ENTITIES - Handle AutoCAD DXF ENTITIES list

=head1 SYNOPSIS

ENTITIES is one section of a DXF file, this class inherits from L<File::DXF::Section>

=head1 DESCRIPTION

=cut

use strict;
use warnings;
use File::DXF::Util 'dxf';

use File::DXF::Section 'next_item';
use vars qw /@EXPORT_OK/;
use base qw /File::DXF::Section Exporter/;

our @EXPORT_OK = qw /polyface_from_3dfaces/;

=over

=item Process

Try and find some structure in the raw data (which needs to be in two column
format as produced by read_DXF().

  $entities->Process ($data);

..or:

  $entities->Process ([[0, 'POINT'],
                       [10, 1.23],
                       [20, 4.56],
                         ...
                       [0, 'TEXT'],
                       [10, 7.89],
                         ...
                       [50, 90]]);

$entities becomes a list of all the entities, each of which are a hash with
'type' entry which is a string representing type and _data consisting of
unprocessed two-column data for that entity.

=cut

sub Process
{
    my $self = shift;
    my $data = shift;
    my $entity_type;
    my $chunk = [];

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);

        if ($code == 0)
        {
            if ($entity_type)
            {
                my $struct = { type => $entity_type,
                              _data => $chunk };
                push @{$self}, $struct;
            }
            $chunk = [];
            $entity_type = $string;
            next;
        }

        push @{$chunk}, [$code, $string];
    }
    if ($entity_type)
    {
        my $struct = { type => $entity_type,
                      _data => $chunk };
        push @{$self}, $struct;
    }
    return;
}

=item Report

Return a list of entities that match one or more 'types'

  @report = $entities->Report (('TEXT', 'POINT'));

These are blessed into appropriate classes (such as File::DXF::ENTITIES::TEXT).

=cut

sub Report
{
    my $self = shift;
    my @types = @_;
    my @out;

    for my $entity (@{$self})
    {
        next unless $entity;
        next unless (grep {/^$entity->{type}$/x} @types );
        my $type = $entity->{type};
        eval "use File::DXF::ENTITIES::$type";
        next if $@;
        my $item = eval "new File::DXF::ENTITIES::$type";
        $item->Process ($entity->{_data});
        push @out, $item;
    }
    return @out;
}

=item DXF

Generate DXF formatted data, just for this section:

  $text = $entities->DXF;

=cut

sub DXF
{
    my $self = shift;
    my $text;
    $text .= dxf (0, 'SECTION');
    $text .= dxf (2, 'ENTITIES');

    for my $entity (@{$self})
    {
        next unless $entity;
        my $type = $entity->{type};
        $text .= dxf (0, $type);
        for my $item (@{$entity->{_data}})
        {
            next if $item->[0] == 5;
            next if $item->[0] == 100;
            next if $item->[0] == 330;
            next if $item->[0] >= 1000;
            $text .= dxf (@{$item});
        }
    }

    $text .= dxf (0, 'ENDSEC');
    return $text;
}

=item Mesh

Generate POLYLINE/VERTEX DXF entities representing a supplied mesh:

  my @entities = $entities->Mesh ($nodes, $faces, {colour => 7, layer => 'meshes'});

$nodes is a list of coordinates:

   push @{$nodes}, [12.3,23.4,34.5];
   push @{$nodes}, [45.6,56.7,67.8];
   ...

$faces is a list of faces each consisting of 3 or 4 node ids and an optional colour:

   push @{$faces}, {node_id => [3,12,0,4], colour => 7};
   push @{$faces}, {node_id => [0,3,4], colour => 2};
   ...

This all uses a normal numbering system starting at 0 (DXF uses a weird
numbering system starting at 1, but we hide this).

=cut

sub Mesh
{
    my $self = shift;
    my $nodes = shift;
    my $faces = shift;
    my $meta = shift;

    return _mesh ($nodes, $faces, $meta);
}

sub _mesh
{
    my $nodes = shift;
    my $faces = shift;
    my $meta = shift;

    my $colour = 0;
    my $layer = 0;
    $colour = $meta->{colour} if defined $meta->{colour};
    $layer = $meta->{layer} if defined $meta->{layer};

    my @entities,

    # dump header
    my $polyline = {type => 'POLYLINE', _data => [
        [8, $layer],
        [62, $colour],
        [66, 1],
        [10, 0.0],
        [20, 0.0],
        [30, 0.0],
        [70, 64],
        [71, scalar @{$nodes}],
        [72, scalar @{$faces}]
    ]};

    push @entities, $polyline;

    # dump nodes
    for my $coor (@{$nodes})
    {
        my $vertex = {type => 'VERTEX', _data => [
            [8, $layer],
            [10, $coor->[0]],
            [20, $coor->[1]],
            [30, $coor->[2]],
            [70, 192]
        ]};

        push @entities, $vertex;
    }

    # dump faces
    for my $face (@{$faces})
    {
        $face->{colour} = 0 unless defined $face->{colour};
        my $vertex = {type => 'VERTEX', _data => [
            [8, $layer],
            [10, 0.0],
            [20, 0.0],
            [30, 0.0],
            [62, $face->{colour}],
            [70, 128],
            # DXF nodes are numbered starting at 1
            [71, $face->{node_id}->[0] +1],
            [72, $face->{node_id}->[1] +1],
            [73, $face->{node_id}->[2] +1],
        ]};
        push @{$vertex->{_data}}, [74, $face->{node_id}->[3] +1] if defined $face->{node_id}->[3];

        push @entities, $vertex;
    }

    push @entities, {type => 'SEQEND', _data => []};
    return @entities;
}

sub polyface_from_3dfaces
{
    return _mesh (_faces_to_mesh (@_));
}

=item Rectangular_to_Meshes

Extract all the rectangular meshes and convert to normal node/face meshes:

  my @meshes = $entities->Rectangular_to_Meshes;

The output is suitable for input to the Mesh() method above:

  my $mesh = shift @meshes;
  my @entities = $entities->Mesh (@{$mesh});

=cut

sub Rectangular_to_Meshes
{
    my $self = shift;
    my @report = $self->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

    my $mesh;
    my $rect;
    my $layer;
    my $colour;
    my $vertex_index;
    my $state = 'INIT';

    my @out;
    for my $item (@report)
    {
        if ($item->Type eq 'POLYLINE' and $item->Rectangular)
        {
            $mesh = $item;
            $rect = [];
            $vertex_index = 0;
            $colour = $item->{colour};
            $layer = $item->{layer};
            $state = 'RECTANGULAR';
        }

        if ($state eq 'RECTANGULAR' and $item->Type eq 'VERTEX')
        {
            my ($i, $j) = $mesh->Index ($vertex_index);
            $rect->[$i]->[$j] = [@{$item->Coordinates}, $vertex_index];
            $vertex_index++;
        }

        if ($state eq 'RECTANGULAR' and $item->Type eq 'SEQEND')
        {
            my @nodes;
            my @faces;

            for my $m (0 .. scalar @{$rect} -1)
            {
                for my $n (0 .. scalar @{$rect->[0]} -1)
                {
                    push @nodes, [$rect->[$m]->[$n]->[0], $rect->[$m]->[$n]->[1], $rect->[$m]->[$n]->[2]];
                }
            }

            for my $m (0 - $mesh->Closed_M .. scalar (@{$rect}) -2)
            {
                for my $n (0 - $mesh->Closed_N .. scalar (@{$rect->[0]}) -2)
                {
                    push @faces, {node_id => [$rect->[$m]->[$n]->[3],
                                              $rect->[$m +1]->[$n]->[3],
                                              $rect->[$m +1]->[$n +1]->[3],
                                              $rect->[$m]->[$n +1]->[3]], colour => $colour};
                }
            }

            push @out, [[@nodes], [@faces], {layer => $layer, colour => $colour}];
            $state = 'NEXT';
        }
    }
    return @out;
}

=item Faces_to_Meshes

Merge all the 3DFACE entities into a single normal node/face mesh:

  my @mesh = $entities->Faces_to_Mesh;

The output is suitable for input to the Mesh() method above:

  my @entities = $entities->Mesh (@mesh);

=cut

sub Faces_to_Mesh
{
    my $self = shift;

    my @report = $self->Report ('3DFACE');

    print STDERR '3DFACE entities: '. scalar (@report) ."\n";

    return _faces_to_mesh (@report);
}

sub _faces_to_mesh
{
    my @report = @_;

    # get list of nodes used
    my $used_coors;
    for my $_3dface (@report)
    {
        # Packed() returns a stringified version of a 3dcoordinate
        $used_coors->{$_3dface->Packed (0)} = $_3dface->Coordinates (0);
        $used_coors->{$_3dface->Packed (1)} = $_3dface->Coordinates (1);
        $used_coors->{$_3dface->Packed (2)} = $_3dface->Coordinates (2);
        $used_coors->{$_3dface->Packed (3)} = $_3dface->Coordinates (3);
    }

    # Assemble list of unique nodes and build a lookup table of nodes
    my @nodes;
    my $lookup;
    my $lookup_index = 0;
    for my $packed (sort keys %{$used_coors})
    {
        $lookup->{$packed} = $lookup_index;
        $lookup_index++;
        push @nodes, $used_coors->{$packed};
    }

    my @faces;
    for my $_3dface (@report)
    {
        # get four node ids
        my $nodes = [map {$_3dface->Packed ($_)} (0 .. 3)];

        # transform from a quad to a triangle if consecutive nodes coincide
        my $nodes_new = [];
        for my $node_id (0 .. 3)
        {
            next if $nodes->[$node_id] eq $nodes->[$node_id -1];
            push @{$nodes_new}, $nodes->[$node_id];
        }

        # drop faces with 1 or 2 nodes
        next unless scalar @{$nodes_new} > 2;

        my $face = {colour => $_3dface->Colour,
                   node_id => [$lookup->{$nodes_new->[0]}, $lookup->{$nodes_new->[1]}, $lookup->{$nodes_new->[2]}]};

        push @{$face->{node_id}}, $lookup->{$nodes_new->[3]} if defined $nodes_new->[3];
        push @faces, $face;
    }
    return [@nodes], [@faces], {colour => $report[0]->{colour}, layer => $report[0]->{layer}};
}

=item Extract_Meshes

Find all polyline polyface meshes, and extract them in a simplified format

  @meshes = $entities->Extract_Meshes;

Each mesh is a list of points in space-separated '1.23 4.56 7.89' format,
followed by a list of triangle/quad faces in space-separated '12 34 56 78'
format.

=back

=cut

sub Extract_Meshes
{
    my $self = shift;
    my @report = $self->Report (('POLYLINE', 'VERTEX', 'SEQEND'));

    my $state = 'INIT';
    my $id_node;
    my $id_triangle;
    my @nodes;
    my @triangles;
    my @colours;
    my @meshes;
    my $colour;
    my $layer;

    for my $item (@report)
    {
        if ($item->Type eq 'POLYLINE' and $item->Triangular)
        {
            print STDERR "found triangular mesh polyline\n";
            print STDERR "nodes: ". $item->Nodes ."\n";
            print STDERR "triangles: ". $item->Triangles ."\n";
            $state = 'POINTS';
            $id_node = 1;
            $id_triangle = 1;
            @nodes = ();
            @triangles = ();
            @colours = ();
	    $colour = $item->{colour};
            $layer = $item->{layer};
        }

        if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Point)
        {
            # STAAD has a maximum line length limit
            $item->{x} = sprintf ("%.6f", $item->{x});
            $item->{y} = sprintf ("%.6f", $item->{y});
            $item->{z} = sprintf ("%.6f", $item->{z});

            push @nodes, join (' ', $item->{x}, $item->{y}, $item->{z});
            $id_node++;
        }

        if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Triangle)
        {
    	$item->{a} = 0 - $item->{a} if ($item->{a} < 0);
    	$item->{b} = 0 - $item->{b} if ($item->{b} < 0);
    	$item->{c} = 0 - $item->{c} if ($item->{c} < 0);

            # some triangles have four sides
            if (defined $item->{d})
            {
    	    $item->{d} = 0 - $item->{d} if ($item->{d} < 0);

                push @triangles, join (' ', $item->{a} -1, $item->{b} -1, $item->{c} -1, $item->{d} -1);
            }
            else
            {
                push @triangles, join (' ', $item->{a} -1, $item->{b} -1, $item->{c} -1);
            }

            $colours[$id_triangle -1] = $item->{colour} if defined $item->{colour};
            $colours[$id_triangle -1] = 0 unless defined $item->{colour};

            $id_triangle++;
        }

        if ($item->Type eq 'SEQEND' and $state eq 'POINTS')
        {
            push @meshes, [[@nodes], [@triangles], {colour => $colour, layer => $layer}, [@colours]] if (scalar @nodes and scalar @triangles);
            $state = 'INIT';
        }
    }
    return @meshes;
}

sub polyface_to_meshes
{
    my @report = @_;

    my $state = 'INIT';
    my @nodes;
    my @faces;
    my @meshes;
    my $colour;
    my $layer;

    for my $item (@report)
    {
        if ($item->Type eq 'POLYLINE' and $item->Triangular)
        {
            $state = 'POINTS';
            @nodes = ();
            @faces = ();
	    $colour = $item->{colour};
            $layer = $item->{layer};
        }

        if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Point)
        {
            push @nodes, {coor => [$item->{x}, $item->{y}, $item->{z}]};
        }

        if ($state eq 'POINTS' and $item->Type eq 'VERTEX' and $item->Triangle)
        {
            $item->{a} = 0 - $item->{a} if ($item->{a} < 0);
            $item->{b} = 0 - $item->{b} if ($item->{b} < 0);
            $item->{c} = 0 - $item->{c} if ($item->{c} < 0);

            my $face;
            # some triangles have four sides
            if (defined $item->{d})
            {
                $item->{d} = 0 - $item->{d} if ($item->{d} < 0);
                $face = {node_id => [$item->{a} -1, $item->{b} -1, $item->{c} -1, $item->{d} -1]};
            }
            else
            {
                $face = {node_id => [$item->{a} -1, $item->{b} -1, $item->{c} -1]};
            }

            $face->{colour} = 0 unless defined $item->{colour};
            $face->{colour} = $item->{colour} if defined $item->{colour};
            push @faces, $face;
        }

        if ($item->Type eq 'SEQEND' and $state eq 'POINTS')
        {
            push @meshes, [[@nodes], [@faces], {colour => $colour, layer => $layer}] if (scalar @nodes and scalar @faces);
            $state = 'INIT';
        }
    }
    return @meshes;
}

sub polyline_to_list
{
    my @report = @_;
    my $points = [];
    my $profiles = [];
    my $colour = 0;
    my $state;
    for my $item (@report)
    {
        if ($item->Type eq 'POLYLINE')
        {
            $state = 'POINTS';
            $colour = $item->{colour};
            $points = [];
        }

        if ($state eq 'POINTS' and $item->Type eq 'VERTEX')
        {
            push @{$points}, [$item->{x}, $item->{y}, $item->{z}];
        }

        if ($item->Type eq 'SEQEND' and $state eq 'POINTS')
        {
            push @{$profiles}, [$points, {colour => $colour}];
            $state = 'NEXT';
        }
    }
    return $profiles;
}
1;

