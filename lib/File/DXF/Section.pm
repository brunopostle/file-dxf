package File::DXF::Section;

use strict;
use warnings;
use Exporter;
use base 'Exporter';
use vars qw /@EXPORT_OK/;
@EXPORT_OK = qw /next_item/;

=head1 NAME

File::DXF::Section - base class

=head1 SYNOPSIS

Base class for DXF SECTION sections, see L<File::DXF::HEADER>, L<File::DXF::BLOCKS>, L<File::DXF::ENTITIES>

=head1 DESCRIPTION

=over

=item new

Create one like so:

  $foo = new File::DXF::$bar;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = [];

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=cut

sub Process
{
    my $self = shift;
    my $data = shift;
    return;
}


=item next_item

  ($key, $value) = $foo->next_item;

=back

=cut

sub next_item
{
    my $data = shift;
    my $item = shift @{$data};
    return @$item;
}


1;

