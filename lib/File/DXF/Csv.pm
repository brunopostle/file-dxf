package File::DXF::Csv;

=head1 NAME

File::DXF::Csv - Comma separated value files

=head1 SYNOPSIS

Simple CSV writing

=head1 DESCRIPTION

=cut

use strict;
use warnings;
use Carp;
use base 'Exporter';

use vars qw /@EXPORT_OK/;
@EXPORT_OK = qw /array2csv/;

=over

=item new

Create a Csv object like so:

  $cvs = new File::DXF::Csv;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self;

    $self = [];

    bless $self, $class;
    return $self;
}

=item Push

Add a line of data to the table:

  $cvs->Push (1, 1, 2, 3, 5, 8, 13, 21, 34, 55);

=cut

sub Push
{
    my $self = shift;
    push @{$self}, array2csv (@_);
    return 1;
}

=item Write

Output the table as a CSV file like so:

  $cvs->Write ("my-filename.csv");

Note that that line-endings are the system default and that all output is
assumed to be UTF-8.

Warning: Excel is a pile of crap and can't open UTF-8 encoded
CSV files.  If you have non-ascii characters in your data, you
need to use the Excel 'Data import wizard'.

=back

=cut

sub Write
{
    my $self = shift;
    my $filename = shift;

    open my $FILE, ">:encoding(utf8)", $filename
         or croak "cannot write-open $filename";
    print $FILE join ("\n", @{$self});
    close $FILE;
    return 1;
}

=pod

=head1 Utility methods

=over

=item array2csv

Take an array and turn it into a line of a CSV file (not including final
carriage return).

    $line = array2csv (@array);

=cut

sub array2csv
{
    my @line = @_;
    @line = map {_escape_for_csv ($_)} @line;
    return (join ",", @line);
}

=pod

" is escaped as "".

Carriage returns are replaced by whitespace.

Leading and trailing spaces are removed.

Items containing " or , are quoted with double quotes.

=back

=cut

sub _escape_for_csv
{
    my $item = shift;
    $item =~ s/"/""/gx;
    $item =~ s/[\r\n\l]+/ /g;
    $item =~ s/(^ +| +$)//g;
    $item = "\"$item\"" if $item =~ /[",]/x;
    return $item;
}

1;
