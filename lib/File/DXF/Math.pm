package File::DXF::Math;

=head1 NAME

File::DXF::Math - Some vector math

=head1 SYNOPSIS

Utility functions

=head1 DESCRIPTION

=cut

use strict;
use warnings;
use Math::Trig;
use base 'Exporter';
use Carp;
use 5.010;
use Data::Dumper;

use vars qw /@EXPORT_OK/;
@EXPORT_OK = qw /cog_3d average_3d add_3d scale_3d scale_2d multiply_3d area_3d green_theorem is_between_2d distance_2d distance_3d normalise_3d normalise_2d x_product_3d dot_product_3d magnitude_3d add_2d subtract_2d subtract_3d triangle_normal line_plane_intersection angle_vectors_3d PI angle_triangle point_is_in_triangle point_is_in_triangle_2 point_place map_point flatten_triangle rotate_3d points_2line line_intersection perpendicular_line perpendicular_distance angle_2d map_uv_to_point map_point_to_uv map_point_quad points_to_plane plane_distance shortest_link/;

=head1 USAGE

=over

=item cog_3d

Find the centre of gravity of the volume between a 3d triangle and the XY plane:

  my $coor = cog_3d ($a_coor, $b_coor, $c_coor);

=cut

sub cog_3d
{
    my @P = sort {$a->[2] <=> $b->[2]} @_;
    my $area = green_theorem (@P);
    my $vol_bot = $area * $P[0]->[2];
    my $vol_mid = ($area * average_3d ($P[0], $P[0], $P[1])->[2]) - $vol_bot;
    my $vol_top = ($area * average_3d (@P)->[2]) - ($vol_mid + $vol_bot);
    my $cog_bot = average_3d (@P);
    $cog_bot->[2] = $P[0]->[2] /2;
    my $cog_mid = average_3d ($P[0], $P[1], [$P[1]->[0],$P[1]->[1],$P[0]->[2]],
                                             [$P[2]->[0],$P[2]->[1],$P[0]->[2]]);
    my $cog_top = average_3d ($P[0], $P[1], $P[2], [$P[2]->[0],$P[2]->[1],$P[0]->[2]]);
    return average_3d (@P) if ($vol_bot+$vol_mid+$vol_top == 0);
    return scale_3d (add_3d (scale_3d ($vol_bot, $cog_bot),
                        scale_3d ($vol_mid, $cog_mid),
                        scale_3d ($vol_top, $cog_top)), 1/($vol_bot+$vol_mid+$vol_top));
}

=item average_3d

Find the average point of a list of 3d points:

  my $coor = average ($a_coor, $b_coor, $c_coor ... );

=cut

sub average_3d
{
    return scale_3d (add_3d (@_), 1/@_);
}

=item add_3d

Add a list of 3d points or vectors:

  my $coor = add_3d ($a_coor, $b_coor, $c_coor ... );

=cut

sub add_3d
{
    my @coors = @_;
    my $new = [@{shift @coors}];
    for my $coor (@coors)
    {
        $new->[0] += $coor->[0];
        $new->[1] += $coor->[1];
        $new->[2] += $coor->[2];
    }
    return [$new->[0], $new->[1], $new->[2]];
}

=item rotate_3d

Rotate a 3d point around the z axis:

  my $coor = rotate_3d ($radians, $a_coor);

=cut

sub rotate_3d
{
    my ($angle_rad, $coor_2d) = @_;
    ($angle_rad, $coor_2d) = ($coor_2d, $angle_rad) if ref $angle_rad;
    my $distance = distance_3d ([0,0,0], [$coor_2d->[0], $coor_2d->[1], 0]);
    my $angle;
    if ($coor_2d->[0] == 0 and $coor_2d->[1] >= 0)
    {
        $angle = 0.5 * PI();
    }
    elsif ($coor_2d->[0] == 0 and $coor_2d->[1] < 0)
    {
        $angle = 1.5 * PI();
    }
    else
    {
        $angle = atan ($coor_2d->[1]/$coor_2d->[0]);
        $angle += PI() if $coor_2d->[0] < 0;
    }
    $angle_rad += $angle;
    return [$distance * cos ($angle_rad), $distance * sin ($angle_rad), $coor_2d->[2]];
}

=item scale_3d

Scale a 3d point or vector by a factor:

  $coor = scale_3d ($factor, $coor);

=cut

sub scale_3d
{
    my ($factor, $coor_3d) = @_;
    ($factor, $coor_3d) = ($coor_3d, $factor) if ref $factor;
    return [$coor_3d->[0] * $factor, $coor_3d->[1] * $factor, $coor_3d->[2] * $factor];
}

sub scale_2d
{
    my ($factor, $coor_2d) = @_;
    ($factor, $coor_2d) = ($coor_2d, $factor) if ref $factor;
    return [$coor_2d->[0] * $factor, $coor_2d->[1] * $factor];
}

sub multiply_3d
{
    my ($a, $b) = @_;
    return [$a->[0] * $b->[0], $a->[1] * $b->[1], $a->[2] * $b->[2]];
}

# area of a 3d triangle, always >= 0.0

sub area_3d
{
    my ($a, $b, $c) = @_;

    my $A = distance_3d ($b, $c);
    my $B = distance_3d ($a, $c);
    my $C = distance_3d ($a, $b);
    my $S = ($A + $B + $C) / 2;
    my $foo = $S * ($S - $A) * ($S - $B) * ($S - $C);
    if ($foo < 0.0)
    {
        carp ("sqrt ($foo)");
        return 0.0;
    }

    return sqrt $foo;
}

# plan area of a 2d or 3d triangle.
# anti-clockwise triangles have +ve areas.
# clockwise triangles have -ve areas.

sub green_theorem
{
   my $triangle = [@_];
   my $asum = 0.0;
   for my $i (0 .. 2)
   {
      my $i1 = ($i + 1) % 3;
      my $x = $triangle->[$i]->[0];
      my $y = $triangle->[$i]->[1];
      my $x1 = $triangle->[$i1]->[0];
      my $y1 = $triangle->[$i1]->[1];
      $asum += $x*$y1 - $x1*$y;
   }
   return $asum /2;
}

=item is_between_2d

Is a point on a line between two other points:

  say 'nope!' unless is_between_2d ($coor, $coor_a, $coor_b);

=cut

sub is_between_2d
{
    my ($point, @points) = @_;
    my $length = distance_2d ($points[0], $points[1]);
    my $length_a = distance_2d ($points[0], $point);
    my $length_b = distance_2d ($points[1], $point);
    return 1 if abs ($length - $length_a - $length_b) < 0.000001;
    return 0;
}

sub distance_2d
{
    my ($a, $b) = @_;
    return sqrt (($a->[0] - $b->[0])**2
        + ($a->[1] - $b->[1])**2);
}

sub distance_3d
{
    my ($a, $b) = @_;
    if (scalar(@{$a}) == 2)
    {
        return distance_2d($a, $b);
    }
    return sqrt (($a->[0] - $b->[0])**2
        + ($a->[1] - $b->[1])**2
        + ($a->[2] - $b->[2])**2);
}

sub x_product_3d
{
    my ($A, $B) = @_;
    my $x = $A->[1] * $B->[2] - $B->[1] * $A->[2];
    my $y = $A->[2] * $B->[0] - $B->[2] * $A->[0];
    my $z = $A->[0] * $B->[1] - $B->[0] * $A->[1];
    return normalise_3d ([$x, $y, $z]);
}

sub dot_product_3d
{
    my ($a, $b) = @_;
    return (($a->[0] * $b->[0]) + ($a->[1] * $b->[1]) + ($a->[2] * $b->[2]));
}

sub normalise_3d
{
    my $vector = shift;
    my $magnitude = magnitude_3d ($vector);
    return [1,0,0] unless $magnitude;
    return scale_3d ($vector, 1 / $magnitude);
}

sub normalise_2d
{
    my $a = shift;
    my $length = distance_2d ([0,0], $a);
    return [1,0] unless $length;
    return [$a->[0] / $length, $a->[1] / $length];
}

sub magnitude_3d
{
    my $vector = shift;
    return distance_3d ([0,0,0], $vector);
}

=item subtract_2d

  $vector_from_a_to_b = subtract_2d ($b, $a);

=cut

sub subtract_2d
{
    my ($a, $b) = @_;
    return [$a->[0] - $b->[0], $a->[1] - $b->[1]];
}

sub add_2d
{
    my ($a, $b) = @_;
    return [$a->[0] + $b->[0], $a->[1] + $b->[1]];
}

sub subtract_3d
{
    my $a = shift;
    my $b = shift;
    return [$a->[0] - $b->[0], $a->[1] - $b->[1], $a->[2] - $b->[2]];
}

sub angle_2d
{
    my ($a, $b) = @_;
    return (PI()/2) if (($a->[0] == $b->[0]) and ($a->[1] < $b->[1]));
    return (PI()/-2) if (($a->[0] == $b->[0]) and ($a->[1] > $b->[1]));
    my $radians = atan (($b->[1] - $a->[1]) / ($b->[0] - $a->[0]));
    $radians += PI() if $b->[0] < $a->[0];

    return $radians;
}

sub angle_vectors_3d
{
    my ($A, $B) = @_;

    my $a_3d = [@{$A}];
    my $b_3d = [@{$B}];

    $a_3d = normalise_3d ($a_3d);
    $b_3d = normalise_3d ($b_3d);

    return acos (($a_3d->[0] * $b_3d->[0]) + ($a_3d->[1] * $b_3d->[1]) + ($a_3d->[2] * $b_3d->[2]));
}

# take three sides of a triangle a_dis, b_dis,c_dis find the positive angle_A
# opposite a_dis

sub angle_triangle
{
    my ($a_dis, $b_dis, $c_dis) = @_;

    my $foo = (2 * $b_dis * $c_dis);

    if ($foo eq 0)
    {
        return 0;
    }
    elsif ($a_dis eq $b_dis + $c_dis)
    {
        return PI();
    }
    else
    {
        my $bar = (($b_dis)**2 + ($c_dis)**2 - ($a_dis)**2) / $foo;
        my $baz = (0 - $bar) * $bar + 1;
        $baz = 0.0000000000001 if $baz <= 0.0;
        return atan ((0 - $bar) / sqrt ( $baz )) + (PI() / 2);
    }
}

sub matrix_3d_to_2d
{
    my ($A, $B, $C) = @_;
    my $N = normalise_3d(x_product_3d(subtract_3d($B, $A), subtract_3d($C, $A)));
    my $U = normalise_3d(subtract_3d($B, $A));
    my $V = x_product_3d($U, $N);
    my $u = add_3d($A, $U);
    my $v = add_3d($A, $V);
    my $n = add_3d($A, $N);

    my $S = [[ $A->[0], $u->[0], $v->[0], $n->[0] ],
             [ $A->[1], $u->[1], $v->[1], $n->[1] ],
             [ $A->[2], $u->[2], $v->[2], $n->[2] ],
             [ 1, 1, 1, 1 ]];

    my $D = [[ 0, 1, 0, 0 ],
             [ 0, 0, 1, 0 ],
             [ 0, 0, 0, 1 ],
             [ 1, 1, 1, 1 ]];

    use Math::MatrixReal;
    my $Smatrix = Math::MatrixReal->new_from_rows($S);
    my $Dmatrix = Math::MatrixReal->new_from_rows($D);
    $Dmatrix * $Smatrix->inverse;
}

sub triangle_normal
{
    my ($a, $b, $c) = biggest_triangle(@_);
    return x_product_3d (subtract_3d ($b, $a), subtract_3d ($c, $a));
}

# returns a ratio between two points that intersects a plane defeined by three
# points http://paulbourke.net/geometry/planeline/

sub line_plane_intersection
{
    my ($p1, $p2, $a, $b, $c) = @_;
    my $N = triangle_normal ($a, $b, $c);
    my $top = dot_product_3d ($N, subtract_3d ($a, $p1));
    my $bottom = dot_product_3d ($N, subtract_3d ($p2, $p1));
    return ($top/$bottom) unless $bottom == 0;
    return 0.5;
}

sub points_to_plane
{
    my ($P1, $P2, $P3) = biggest_triangle(@_);
    my ($x1, $y1, $z1) = @{$P1};
    my ($x2, $y2, $z2) = @{$P2};
    my ($x3, $y3, $z3) = @{$P3};
    my $a1 = $x2 - $x1;
    my $b1 = $y2 - $y1;
    my $c1 = $z2 - $z1;
    my $a2 = $x3 - $x1;
    my $b2 = $y3 - $y1;
    my $c2 = $z3 - $z1;
    my $A = $b1 * $c2 - $b2 * $c1;
    my $B = $a2 * $c1 - $a1 * $c2;
    my $C = $a1 * $b2 - $b1 * $a2;
    my $D = (0 - $A * $x1 - $B * $y1 - $C * $z1);
    return [$A, $B, $C, $D];
}

sub biggest_triangle
{
    my @points = @_;
    return @_ if scalar(@points) < 4;
    my $lookup = {};
    for (-2 .. scalar(@points) -3)
    {
        $lookup->{area_3d($points[$_], $points[$_+1], $points[$_+2])} = $_;
    }
    my @by_area = sort {$b <=> $a} keys %{$lookup};
    my $id = $lookup->{$by_area[0]};
    ($points[$id], $points[$id+1], $points[$id+2]);
}

sub plane_distance
{
    my ($plane, $point) = @_;
    my ($A, $B, $C, $D) = @{$plane};
    my ($x, $y, $z) = @{$point};
    return 9999999 if (($A*$A) + ($B*$B) + ($C*$C)) == 0;
    return (($A*$x) + ($B*$y) + ($C*$z) + $D) / sqrt(($A*$A) + ($B*$B) + ($C*$C));
}

sub point_is_in_triangle
{
    my ($A, $B, $C, $P, $threshold) = @_;
    $threshold = 0.99 unless $threshold;
    return 1 if distance_3d ($A, $P) <= 0.00001;
    return 1 if distance_3d ($B, $P) <= 0.00001;
    return 1 if distance_3d ($C, $P) <= 0.00001;
    my $PA = subtract_3d ($A, $P);
    my $PB = subtract_3d ($B, $P);
    my $PC = subtract_3d ($C, $P);
    my $angle_a = angle_vectors_3d ($PB, $PC);
    my $angle_b = angle_vectors_3d ($PC, $PA);
    my $angle_c = angle_vectors_3d ($PA, $PB);
    my $angle_all = $angle_a + $angle_b + $angle_c;
    return 1 if ($angle_all/(2 * PI()) >= $threshold);
    return 0;
}

sub sign
{
    my ($p1, $p2, $p3) = @_;
    return ($p1->[0] - $p3->[0]) * ($p2->[1] - $p3->[1])
         - ($p2->[0] - $p3->[0]) * ($p1->[1] - $p3->[1]);
}

# https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
sub point_is_in_triangle_2
{
    my ($v1, $v2, $v3, $pt) = @_;

    my $d1 = sign($pt, $v1, $v2);
    my $d2 = sign($pt, $v2, $v3);
    my $d3 = sign($pt, $v3, $v1);

    my $has_neg = ($d1 < 0) || ($d2 < 0) || ($d3 < 0);
    my $has_pos = ($d1 > 0) || ($d2 > 0) || ($d3 > 0);

    return 0 if ($has_neg && $has_pos);
    return 1;
}

=item point_place

Given a point $P that is in the same plane and within a triangle $A, $B, $C,
return the position relative to $A in terms of fractions of the A->B and B->C
vectors:

  my ($a, $b) = point_place ($A, $B, $C, $P);

=cut

# FIXME only works in XY plane, so 3D triangles in vertical planes will fail badly

sub point_place
{
    my ($A, $B, $C, $P) = @_;
    my $ab_line = points_2line ($A, $B);
    my $BC = subtract_2d ($B, $C);
    my $dp_line = points_2line ($P, add_2d ($P, $BC));
    my $D = line_intersection ($ab_line, $dp_line);
    my $ad_distance = distance_2d ($A, $D);
    my $ab_distance = distance_2d ($A, $B);
    my $dp_distance = distance_2d ($D, $P);
    my $bc_distance = distance_2d ($B, $C);
    return ($ad_distance/$ab_distance, $dp_distance/$bc_distance);
}

=item map_point map_point_quad

Given a point $P that is in the same plane and within a triangle $A1, $B1, $C1,
return the 3D position of the point if mapped to triangle $A2, $B2, $C2

  my $P2 = map_point ($A1, $B1, $C1, $A2, $B2, $C2, $P);

..for quads:

  $P2 = map_point_quad ($A1, $B1, $C1, $D1, $A2, $B2, $C2, $D2, $P);

=cut

sub map_point
{
    my ($A1, $B1, $C1, $A2, $B2, $C2, $P) = @_;
    my ($a, $b) = point_place ($A1, $B1, $C1, $P);
    my $a_vec = scale_3d ($a, subtract_3d ($B2, $A2));
    my $b_vec = scale_3d ($b, subtract_3d ($C2, $B2));
    return add_3d ($A2, $a_vec, $b_vec);
}

sub map_point_quad
{
    my ($A1, $B1, $C1, $D1, $A2, $B2, $C2, $D2, $P1) = @_;
    my ($u, $v) = map_point_to_uv($P1,$A1,$B1,$C1,$D1);
    return map_uv_to_point($u,$v,$A2,$B2,$C2,$D2);
}

# given u & v values between 0 and 1, and a 3D quad, get 3d coordinate of point in the quad
sub map_uv_to_point
{
    my ($u, $v, $A, $B, $C, $D) = @_;
    $u = 1-$u; $v = 1-$v;
    my $P = add_3d(scale_3d($u*$v, (subtract_3d(add_3d($A,$C),add_3d($B,$D)))),
                   scale_3d($v, subtract_3d($B,$C)),
                   scale_3d($u, subtract_3d($D,$C)),
                   $C);
    return $P;
}

# given 3d coordinate of point, and a 3D quad, get u & v values (assumes points are co-planar)
# does a crappy linear search to implement reverse mapping of map_uv_to_quad
sub map_point_to_uv
{
    my ($P, $A, $B, $C, $D) = @_;
    my ($u, $v) = (0.5, 0.5);

    my $step = 0.01;

    my ($last_u, $last_v) = ($u, $v);
    for my $i (0 .. 1000)
    {
        $step = 0.001 if $i > 50;
        $step = 0.0001 if $i > 100;
        $step = 0.00001 if $i > 150;
        my $P0 = map_uv_to_point($u,$v,$A,$B,$C,$D);
        my $P1 = map_uv_to_point($u+$step,$v,$A,$B,$C,$D);
        my $P2 = map_uv_to_point($u,$v+$step,$A,$B,$C,$D);

        my $dist_P0 = distance_3d($P,$P0);
        my $dist_P1 = distance_3d($P,$P1);
        my $dist_P2 = distance_3d($P,$P2);

        my ($u_mult, $v_mult) = (-1,-1);
        $u_mult = 1 if $dist_P0 > $dist_P1;
        $v_mult = 1 if $dist_P0 > $dist_P2;

        $u = $u + ($u_mult * $step);
        $v = $v + ($v_mult * $step);
        last if ((abs($u - $last_u) < 0.00001) and (abs($v - $last_v) < 0.00001));
        ($last_u, $last_v) = ($u, $v);
    }
    return ($u, $v);
}

# take an anticlockwise 3d triangle and 2d coordinates for the first two
# points, find the 2d coordinate of the third

sub flatten_triangle
{
    my ($a_3d, $b_3d, $c_3d, $a_2d, $b_2d) = @_;

    # define everything needed for unplaced point

    my $distance_A = distance_3d ($b_3d, $c_3d);
    my $distance_B = distance_3d ($c_3d, $a_3d);
    my $distance_C = distance_3d ($a_3d, $b_3d);

    print STDERR join ' ', 'Distances ABC:', $distance_A, $distance_B, $distance_C, "\n"
        if $ENV{DEBUG};

    # find angle_a from a_dis, b_dis and c_dis
    # this is relative to the base line, not y=constant

    #my $angle_a = _angle ($distance_A, $distance_B, $distance_C);
    my $angle_a = angle_triangle ($distance_A, $distance_B, $distance_C);

    print STDERR join ' ', 'Angle A:', $angle_a, "\n" if $ENV{DEBUG};

    my $adjacent = ($b_2d->[0] - $a_2d->[0]) || 0.00000000001;

    my $base_angle = atan (($b_2d->[1] - $a_2d->[1]) / $adjacent);

    # correct the arctangent

    $base_angle += PI() if ($b_2d->[0] < $a_2d->[0]);

    # find the angle to c relative to y=constant
    my $new_angle = $angle_a + $base_angle;

    my $c_2d = undef;

    $c_2d->[0] = $a_2d->[0] + ($distance_B * cos ($new_angle));
    $c_2d->[1] = $a_2d->[1] + ($distance_B * sin ($new_angle));
    $c_2d->[2] = 0.0;

    return $c_2d;
}

=item points_2line

  $line = points_2line ([1,2], [3,4]);

$line is {a => 1, b => 1};

=cut

sub points_2line
{
    my ($coor_0, $coor_1) = @_;
    my $vector = subtract_2d ($coor_1, $coor_0);
    $vector->[0] = 0.00000000001 unless $vector->[0];
    my $a = $vector->[1] / $vector->[0];
    my $b = $coor_0->[1] - ($coor_0->[0] * $a);
    return {a => $a, b => $b};
}

=item line_intersection

  $coor = line_intersection ($line_a, $line_b);

=back

=cut

sub line_intersection
{
    my ($line_0, $line_1) = @_;
    return if ($line_0->{a} == $line_1->{a});
    my $x = ($line_1->{b} - $line_0->{b}) / ($line_0->{a} - $line_1->{a});
    my $y = ($line_0->{a} * $x) + $line_0->{b};
    return [$x, $y];
}

sub perpendicular_line
{
    my ($line, $coor) = @_;
    $line->{a} = 0.00000000001 unless $line->{a};
    my $a = -1/$line->{a};
    my $b = $coor->[1] - ($coor->[0] * $a);
    return {a => $a, b => $b};
}

sub perpendicular_distance
{
    my ($line, $coor) = @_;
    my $perpendicular_line = perpendicular_line ($line, $coor);
    my $intersection = line_intersection ($line, $perpendicular_line);
    return distance_2d ($coor, $intersection);
}

sub PI
{
    return atan2 (0,-1);
}

sub shortest_link
{
    my ($P1, $P2, $Q1, $Q2) = @_;

    # Direction vectors of the lines
    my $d1 = subtract_3d($P2, $P1);
    my $d2 = subtract_3d($Q2, $Q1);

    # Vector connecting start points
    my $r = subtract_3d($P1, $Q1);

    # Compute the coefficients
    my $a = dot_product_3d($d1, $d1);
    my $b = dot_product_3d($d1, $d2);
    my $c = dot_product_3d($d2, $d2);
    my $d = dot_product_3d($d1, $r);
    my $e = dot_product_3d($d2, $r);

    my $denominator = $a * $c - $b * $b;
    if (abs($denominator) < 1e-8)
    {
        # Lines are parallel
        return (undef, undef);
    }

    # Solve for t and u
    my $t = ($b * $e - $c * $d) / $denominator;
    my $u = ($a * $e - $b * $d) / $denominator;

    if ($t < 0.0 or $t > 1.0 or $u < 0.0 or $u > 1.0)
    {
        # lines don't pass each other
        return (undef, undef);
    }

    # Compute the closest points
    my $P_shortest = add_3d($P1, scale_3d($d1, $t));
    my $Q_shortest = add_3d($Q1, scale_3d($d2, $u));

    return ($P_shortest, $Q_shortest);
}

1;
