package File::DXF::BLOCKS;

use strict;
use warnings;
use File::DXF::Util ('dxf', 'read_DXF');
use File::Spec;

use File::DXF::Section 'next_item';
use base 'File::DXF::Section';

=head1 NAME

File::DXF::BLOCKS - Handle AutoCAD DXF BLOCKS list

=head1 SYNOPSIS

BLOCKS is one section of a DXF file, this class inherits from L<File::DXF::Section>

=head1 DESCRIPTION

Structure is a hash table, with the keys being block names and the values being
DXF fragments equivalent to the contents of the ENTITIES section.

e.g:

   $dxf_in->Process (read_DXF ($path, 'CP1252'));
   my $item = $dxf_in->{BLOCKS}->{'door12'};
   $dxf_out->{ENTITIES}->Process ($item);

=over

=item new

  $foo = File::DXF::BLOCKS->new;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self = {};

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data

  $foo->Process ($data);

=cut

sub Process
{
    my $self = shift;
    my $data = shift;
    my $chunk = [];
    my $state = 'WAITING';
    my $blockname;

    while (@{$data})
    {
        my ($code, $string) = next_item ($data);

        if ($code == 0 and $string eq 'BLOCK')
        {
            $state = 'BLOCK';
            $chunk = [];
        }
        elsif ($state eq 'BLOCK' and $code == 2)
        {
            $blockname = $string;
        }
        elsif ($code == 0 and $string eq 'ENDBLK')
        {
            $self->{$blockname} = $chunk;
            $state = 'WAITING';
            $chunk = [];
        }
        elsif ($state eq 'BLOCK' and $code == 0)
        {
            push @{$chunk}, [$code, $string];
            $state = 'ENTITY';
        }
        elsif ($state eq 'ENTITY')
        {
            push @{$chunk}, [$code, $string];
        }
    }
    return;
}

=item Add_From_Disk

Given paths to DXF file(s), slurps up the content and adds them as separate
items in the BLOCKS table, names are derived from the filename without the
extension.

$blocks->Add_From_Disk ('/path/to/door.dxf', '/path/to/table.dxf');

=cut

sub Add_From_Disk
{
    my $self = shift;

    for my $path (@_)
    {
        my @data;
        my $data = read_DXF ($path);
        my $dxf = File::DXF->new;
        $dxf->Process ($data);
        my $entities = $dxf->{ENTITIES};
        for my $entity (@{$entities})
        {
            next if $entity->{type} =~ /^(INSERT)$/x;
            push @data, [0, $entity->{type}];
            for my $item (@{$entity->{_data}})
            {
                next if $item->[0] == 5;
                next if $item->[0] == 100;
                next if $item->[0] == 330;
                push @data, $item;
            }
        }
        my ($v, $d, $name) = File::Spec->splitpath ($path);
        $name =~ s/\.[[:alnum:]]+$//x;
        $self->{$name} = [@data];
    }
    return 1;
}

=item DXF

Generate DXF formatted data, just for this section:

  $text = $blocks->DXF;

=back

=cut

sub DXF
{
    my $self = shift;
    my $text;
    $text .= dxf (0, 'SECTION');
    $text .= dxf (2, 'BLOCKS');

    for my $name (keys %{$self})
    {
        $text .= dxf (0, 'BLOCK');
        $text .= dxf (8, 0);
        $text .= dxf (2, $name);
        $text .= dxf (70, 0);
        $text .= dxf (10, 0.0);
        $text .= dxf (20, 0.0);
        $text .= dxf (30, 0.0);
        $text .= dxf (3, $name);

        for my $item (@{$self->{$name}})
        {
            next if $item->[0] == 5;
            next if $item->[0] == 100;
            next if $item->[0] == 330;
            $text .= dxf (@{$item});
        }        

        $text .= dxf (0, 'ENDBLK');
        $text .= dxf (8, 0);
    }
    $text .= dxf (0, 'ENDSEC');
    return $text;
}

1;

