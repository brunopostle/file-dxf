package File::DXF::ACDSDATA;

use strict;
use warnings;
use File::DXF::Util 'dxf';

use File::DXF::Section;
use base 'File::DXF::Section';

=head1 NAME

File::DXF::ACDSDATA - Handle AutoCAD DXF ACDSDATA list

=head1 SYNOPSIS

ACDSDATA is one section of a DXF file, this class inherits from L<File::DXF::Section>

=head1 DESCRIPTION

=over

=item DXF

  $dxf = $section->DXF;

=back

=cut

sub DXF
{
    my $self = shift;
    my $text;
    $text .= dxf (0, 'SECTION');
    $text .= dxf (2, 'ACDSDATA');
    $text .= dxf (0, 'ENDSEC');
    return $text;
}

1;

