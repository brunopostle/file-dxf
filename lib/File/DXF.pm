package File::DXF;

=head1 NAME

File::DXF - Handle AutoCAD DXF files

=head1 SYNOPSIS

This is an unfinished DXF parser

=head1 DESCRIPTION

=cut

use strict;
use warnings;
use Carp;
use File::DXF::Util 'dxf';
use File::DXF::HEADER;
use File::DXF::ENTITIES;
use File::DXF::BLOCKS;

our $VERSION=0.02;

=pod

=over

=item new

Create one like so:

  $dxf = File::DXF->new;

=cut

sub new
{
    my $class = shift;
    $class = ref $class || $class;
    my $self;

    $self = {HEADER => File::DXF::HEADER->new, ENTITIES => File::DXF::ENTITIES->new};

    bless $self, $class;
    return $self;
}

=item Process

Try and find some structure in the raw data.  The raw data used as input for
Process() methods always needs to be the two-column table output from
L<File::DXF::Util>::read_DXF(); 

  
  my $data = read_DXF ($path, 'CP1252');
  $dxf->Process ($data);

..or:

  $dxf->Process ([[0, 'SECTION'],
                  [2, 'ENTITIES'],
                     ...
                  [0, 'ENDSEC'],
                  [0, 'EOF']]);

=cut

sub Process
{
    my $self = shift;
    my $data = shift;
    my $section_title;
    my $chunk = [];

    while (@{$data})
    {
        my ($code, $string) = _next ($data);

        if ($code == 0 and $string eq 'SECTION')
        {
            ($code, $string) = _next ($data);

            croak "malformed SECTION title" unless ($code == 2);
            $section_title = $string;
            $section_title =~ s/[^A-Z]//gx;

            $chunk = [];
            next;
        }
        if ($code == 0 and $string eq 'ENDSEC')
        {

            eval "use File::DXF::$section_title";
            if ($@ and $@)
            {
                carp "couldn't load File::DXF::$section_title";
                next;
            }

            my $section;
            $section = eval "File::DXF::$section_title->new()";
            $section->Process ($chunk);
            $self->{$section_title} = $section;
            next;
        }
        if ($code == 0 and $string eq 'EOF')
        {
            return 1;
        }
        push @{$chunk}, [$code, $string];
    }
    return;
}

=item DXF

Generate DXF formatted data:

  $text = $dxf->DXF;

This assembles an entire DXF file consisting of HEADER, BLOCKS, CLASSES,
OBJECTS, TABLES and ENTITIES sections.

=back

=cut

sub DXF
{
    my $self = shift;
    my $text;
    $text .= $self->{HEADER}->DXF if defined $self->{HEADER};
    $text .= $self->{BLOCKS}->DXF if defined $self->{BLOCKS};
    $text .= $self->{CLASSES}->DXF if defined $self->{CLASSES};
    $text .= $self->{OBJECTS}->DXF if defined $self->{OBJECTS};
    $text .= $self->{TABLES}->DXF if defined $self->{TABLES};
    $text .= $self->{ENTITIES}->DXF if defined $self->{ENTITIES};
    $text .= dxf (0, 'EOF');
    return $text;
}

sub _next
{
    my $data = shift;
    my $item = shift @{$data};
    return @$item;
}

=pod

Group Codes in Numerical Order  

The following table gives the group code or group code range accompanied by an
explanation of the group code value. In the table, "fixed" indicates that the
group code always has the same purpose. If a group code isn't fixed, its
purpose depends on the context. For information about abbreviations and
formatting used in this table, see Formatting Conventions in This Reference.
Group codes by number  
 
Group code Description 
 
0 Text string indicating the entity type (fixed) 
 
1 Primary text value for an entity 
 
2 Name (attribute tag, block name, and so on) 
 
3-4 Other text or name values 
 
5 Entity handle; text string of up to 16 hexadecimal digits (fixed) 
 
6 Linetype name (fixed) 
 
7 Text style name (fixed) 
 
8 Layer name (fixed) 
 
9 DXF: variable name identifier (used only in HEADER section of the DXF file) 
 
10 Primary point; this is the start point of a line or text entity, center of a
circle, and so on DXF: X value of the primary point (followed by Y and Z value
codes 20 and 30)
 
11-18 Other points DXF: X value of other points (followed by Y value codes
21-28 and Z value codes 31-38)
 
20, 30 DXF: Y and Z values of the primary point 
 
21-28, 31-37 DXF: Y and Z values of other points 
 
38 DXF: entity's elevation if nonzero 
 
39 Entity's thickness if nonzero (fixed) 
 
40-48 Double-precision floating-point values (text height, scale factors, and
so on) 
 
48 Linetype scale; double precision floating point scalar value; default value
is defined for all entity types 
 
49 Repeated double-precision floating-point value. Multiple 49 groups may
appear in one entity for variable-length tables (such as the dash lengths in
the LTYPE table). A 7x group always appears before the first 49 group to
specify the table length 
 
50-58 Angles (output in degrees to DXF files and radians through AutoLISP and
ObjectARX applications) 
 
60 Entity visibility; integer value; absence or 0 indicates visibility; 1
indicates invisibility 
 
62 Color number (fixed) 
 
66 "Entities follow" flag (fixed) 
 
67 Spacethat is, model or paper space (fixed) 
 
68 APP: identifies whether viewport is on but fully off screen; is not active
or is off 
 
69 APP: viewport identification number 
 
70-78 Integer values, such as repeat counts, flag bits, or modes 
 
90-99 32-bit integer values 
 
100 Subclass data marker (with derived class name as a string). Required for
all objects and entity classes that are derived from another concrete class.
The subclass data marker segregates data defined by different classes in the
inheritance chain for the same object.  This is in addition to the requirement
for DXF names for each distinct concrete class derived from ObjectARX (see
Subclass Markers) 
 
102 Control string, followed by "{<arbitrary name>" or "}". Similar to the
xdata 1002 group code, except that when the string begins with "{", it can be
followed by an arbitrary string whose interpretation is up to the application.
The only other control string allowed is "}" as a group terminator. AutoCAD
does not interpret these strings except during drawing audit operations. They
are for application use 
 
105 Object handle for DIMVAR symbol table entry 
 
110 UCS origin (appears only if code 72 is set to 1) DXF: X value;
 
111 UCS X-axis (appears only if code 72 is set to 1) DXF: X value;
 
112 UCS Y-axis (appears only if code 72 is set to 1) DXF: X value;
 
120-122 DXF: Y value of UCS origin, UCS X-axis, and UCS Y-axis 
 
130-132 DXF: Z value of UCS origin, UCS X-axis, and UCS Y-axis 
 
140-149 Double-precision floating-point values (points, elevation, and DIMSTYLE
settings, for example) 
 
170-179 16-bit integer values, such as flag bits representing DIMSTYLE settings 
 
210 Extrusion direction (fixed) DXF: X value of extrusion direction
 
220, 230 DXF: Y and Z values of the extrusion direction 
 
270-279 16-bit integer values 
 
280-289 16-bit integer values 
 
290-299 Boolean flag value 
 
300-309 Arbitrary text strings 
 
310-319 Arbitrary binary chunks with same representation and limits as 1004
group codes: hexadecimal strings of up to 254 characters represent data chunks
of up to 127 bytes 
 
320-329 Arbitrary object handles; handle values that are taken "as is." They
are not translated during INSERT and XREF operations 
 
330-339 Soft-pointer handle; arbitrary soft pointers to other objects within
same DXF file or drawing. Translated during INSERT and XREF operations 
 
340-349 Hard-pointer handle; arbitrary hard pointers to other objects within
same DXF file or drawing. Translated during INSERT and XREF operations 
 
350-359 Soft-owner handle; arbitrary soft ownership links to other objects
within same DXF file or drawing. Translated during INSERT and XREF operations 
 
360-369 Hard-owner handle; arbitrary hard ownership links to other objects
within same DXF file or drawing. Translated during INSERT and XREF operations 
 
370-379 Lineweight enum value (AcDb::LineWeight). Stored and moved around as a
16-bit integer. Custom non-entity objects may use the full range, but entity
classes only use 371379 DXF group codes in their representation, because
AutoCAD and AutoLISP both always assume a 370 group code is the entity's
lineweight. This allows 370 to behave like other "common" entity fields 
 
380-389 PlotStyleName type enum (AcDb::PlotStyleNameType). Stored and moved
around as a 16-bit integer. Custom non-entity objects may use the full range,
but entity classes only use 381389 DXF group codes in their representation, for
the same reason as the Lineweight range above 
 
390-399 String representing handle value of the PlotStyleName object, basically
a hard pointer, but has a different range to make backward compatibility easier
to deal with. Stored and moved around as an object ID (a handle in DXF files)
and a special type in AutoLISP. Custom non-entity objects may use the full
range, but entity classes only use 391399 DXF group codes in their
representation, for the same reason as the lineweight range above 
 
400-409 16-bit integers 
 
410-419 String 
 
420-427 32-bit integer value. When used with True Color; a 32-bit integer
representing a 24-bit color value. The high-order byte (8 bits) is 0, the
low-order byte an unsigned char holding the Blue value (0-255), then the Green
value, and the next-to-high order byte is the Red Value. Convering this integer
value to hexadecimal yields the following bit mask: 0x00RRGGBB. For example, a
true color with Red==200, Green==100 and Blue==50 is 0x00C86432, and in DXF, in
decimal, 13132850 
 
430-437 String; when used for True Color, a string representing the name of the
color 
 
440-447 32-bit integer value. When used for True Color, the transparency value 
 
450-459 Long 
 
460-469 Double-precision floating-point value 
 
470-479 String 
 
999 DXF: The 999 group code indicates that the line following it is a comment
string. SAVEAS does not include such groups in a DXF output file, but OPEN
honors them and ignores the comments. You can use the 999 group to include
comments in a DXF file that you've edited 
 
1000 ASCII string (up to 255 bytes long) in extended data 
 
1001 Registered application name (ASCII string up to 31 bytes long) for
extended data 
 
1002 Extended data control string ("{" or "}") 
 
1003 Extended data layer name 
 
1004 Chunk of bytes (up to 127 bytes long) in extended data 
 
1005 Entity handle in extended data; text string of up to 16 hexadecimal digits 
 
1010 A point in extended data DXF: X value (followed by 1020 and 1030 groups)
 
1020, 1030 DXF: Y and Z values of a point 
 
1011 A 3D world space position in extended data DXF: X value (followed by 1021
and 1031 groups)
 
1021, 1031 DXF: Y and Z values of a world space position 
 
1012 A 3D world space displacement in extended data DXF: X value (followed by
1022 and 1032 groups)
 
1022, 1032 DXF: Y and Z values of a world space displacement 
 
1013 A 3D world space direction in extended data DXF: X value (followed by 1022
and 1032 groups)
 
1023, 1033 DXF: Y and Z values of a world space direction 
 
1040 Extended data double-precision floating-point value 
 
1041 Extended data distance value 
 
1042 Extended data scale factor 
 
1070 Extended data 16-bit signed integer 
 
1071 Extended data 32-bit signed long 

=head1 COPYING
 
Free use of this software is granted under the terms of the GNU General
Public License version 2 or any later version.

=cut

1;

